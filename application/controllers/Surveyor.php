<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surveyor extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Petugas Surveilans')
		{

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');
			// NOTIF - END

			$bc['kec_surveilans']		= $this->web_app_model->getWhereOneItem($this->session->userdata('kec'),'kec_no','tbl_kec');

			$bc['zonasi_kec']		= $this->web_app_model->get_zonasi_kec();
			$bc['zonasi_desa']		= $this->web_app_model->get_zonasi_desa();
			$bc['data_desa']		= $this->web_app_model->getAllData('tbl_desa');
			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('surveyor/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('surveyor/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('surveyor/bio',$bc,true);	

			$bc['tot_positif_sembuh']		= $this->web_app_model->tot_positif_sembuh();
			$bc['tot_positif_dirawat']		= $this->web_app_model->tot_positif_dirawat();
			$bc['tot_positif_dipantau']		= $this->web_app_model->tot_positif_dipantau();
			$bc['tot_positif_isman']		= $this->web_app_model->tot_positif_isman();
			$bc['tot_positif_meninggal']	= $this->web_app_model->tot_positif_meninggal();

			$bc['tot_suspect_sembuh']		= $this->web_app_model->total_pasien_where('23','1');
			$bc['tot_suspect_dirawat']		= $this->web_app_model->total_pasien_where('1','1');
			$bc['tot_suspect_dipantau']		= $this->web_app_model->total_pasien_where('15','1');
			$bc['tot_suspect_isman']		= $this->web_app_model->total_pasien_where('2','1');
			$bc['tot_suspect_meninggal']	= $this->web_app_model->total_pasien_where('8','1');

			$bc['pos_pasienLuar_dirawat']	= $this->web_app_model->total_pasien_where('30','0');
			$bc['pos_pasienLuar_dipantau']	= $this->web_app_model->total_pasien_where('33','0');
			$bc['pos_pasienLuar_isman']		= $this->web_app_model->total_pasien_where('31','0');

			$bc['pos_pasienLokal_dirawat']	= $this->web_app_model->total_pasien_where('30','1');
			$bc['pos_pasienLokal_dipantau']	= $this->web_app_model->total_pasien_where('33','1');
			$bc['pos_pasienLokal_isman']	= $this->web_app_model->total_pasien_where('31','1');

			$bc['pos_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('98','1');
			$bc['sus_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('8','1');
			$bc['pro_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('27','1');

			$bc['zonasi_desa']				= $this->web_app_model->get_zonasi_desa();

			$bc['grafik1'] 			= $this->load->view('graph/grafik1',$bc,true);
			$bc['grafik2'] 			= $this->load->view('graph/grafik2',$bc,true);
			$bc['grafik3'] 			= $this->load->view('graph/grafik3',$bc,true);
			$bc['grafik4'] 			= $this->load->view('graph/grafik4',$bc,true);
			$bc['grafik5'] 			= $this->load->view('graph/grafik5',$bc,true);

			$this->load->view('general/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	// START - DATA PASIEN TERVERIFIKASI
	public function bg_verified()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Petugas Surveilans')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');
			// NOTIF - END

			$bc['kec_surveilans']		= $this->web_app_model->getWhereOneItem($this->session->userdata('kec'),'kec_no','tbl_kec');

			$status_pasien 				= $this->uri->segment(3);

			$bc['status_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
			$bc['data_verified']		= $this->web_app_model->get5JoinAll2Where('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1','status_pasien',$status_pasien);
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('surveyor/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('surveyor/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('surveyor/bio',$bc,true);
			$bc['modalUpdateStatus']	= $this->load->view('general/modalUpdateStatus',$bc,true);
			//$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_verified',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_verified_luar()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Petugas Surveilans')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');
			// NOTIF - END

			$bc['kec_surveilans']		= $this->web_app_model->getWhereOneItem($this->session->userdata('kec'),'kec_no','tbl_kec');

			$status_pasien 				= $this->uri->segment(3);

			$bc['status_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
			//$bc['data_verified']		= $this->web_app_model->get2JoinAll2Where('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','pasien_verified_dinkes','1','status_pasien',$status_pasien);
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('surveyor/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('surveyor/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('surveyor/bio',$bc,true);
			$bc['modalUpdateStatusPasienLuar']	= $this->load->view('general/modalUpdateStatusPasienLuar',$bc,true);
			//$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_verified_luar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	// END PASIEB TERVERIFIKASI 



	// START LAPORAN TRACERT

	public function bg_lap_tracert()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Petugas Surveilans')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');
			// NOTIF - END

			$bc['lap_tracert']			= $this->web_app_model->get_lap_tracert($this->session->userdata('kec'));
			//$bc['lap_tracert']			= $this->web_app_model->get_riwayat_tracing();
			
			$bc['kec_surveilans']		= $this->web_app_model->getWhereOneItem($this->session->userdata('kec'),'kec_no','tbl_kec');

			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('surveyor/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('surveyor/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('surveyor/bio',$bc,true);

			$this->load->view('general/bg_lap_tracert',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}


	// END LAPORAN TRACERT

	// START DATA ISMAN

	public function bg_isman()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Petugas Surveilans')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');
			// NOTIF - END

			$bc['data_isman']			= $this->web_app_model->get_isman($this->session->userdata('kec'));
			
			$bc['kec_surveilans']		= $this->web_app_model->getWhereOneItem($this->session->userdata('kec'),'kec_no','tbl_kec');

			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('surveyor/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('surveyor/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('surveyor/bio',$bc,true);

			$this->load->view('general/bg_isman',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}


	// END DATA ISMAN


	
}
