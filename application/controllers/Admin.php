<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin')
		{
			$url="https://apicovid.bravo.siat.web.id/api/v_vaksin_on?_where=(nama_kab,eq,KOTA+LANGSA)&_size=23";
			$get_url = file_get_contents($url);
			$data = json_decode($get_url);

			$bc = array(
			'vaksin' => $data
			);
			
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END


			$bc['zonasi_kec']		= $this->web_app_model->get_zonasi_kec();
			$bc['zonasi_desa']		= $this->web_app_model->get_zonasi_desa();
			$bc['data_desa']		= $this->web_app_model->getAllData('tbl_desa');
			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	

			$bc['tot_positif_sembuh']		= $this->web_app_model->tot_positif_sembuh();
			$bc['tot_positif_dirawat']		= $this->web_app_model->tot_positif_dirawat();
			$bc['tot_positif_dipantau']		= $this->web_app_model->tot_positif_dipantau();
			$bc['tot_positif_isman']		= $this->web_app_model->tot_positif_isman();
			$bc['tot_positif_meninggal']	= $this->web_app_model->tot_positif_meninggal();

			$bc['tot_suspect_sembuh']		= $this->web_app_model->total_pasien_where('23','1');
			$bc['tot_suspect_dirawat']		= $this->web_app_model->total_pasien_where('1','1');
			$bc['tot_suspect_dipantau']		= $this->web_app_model->total_pasien_where('15','1');
			$bc['tot_suspect_isman']		= $this->web_app_model->total_pasien_where('2','1');
			$bc['tot_suspect_meninggal']	= $this->web_app_model->total_pasien_where('8','1');

			$bc['pos_pasienLuar_dirawat']	= $this->web_app_model->total_pasien_where('30','0');
			$bc['pos_pasienLuar_dipantau']	= $this->web_app_model->total_pasien_where('33','0');
			$bc['pos_pasienLuar_isman']		= $this->web_app_model->total_pasien_where('31','0');

			$bc['pos_pasienLokal_dirawat']	= $this->web_app_model->total_pasien_where('30','1');
			$bc['pos_pasienLokal_dipantau']	= $this->web_app_model->total_pasien_where('33','1');
			$bc['pos_pasienLokal_isman']	= $this->web_app_model->total_pasien_where('31','1');

			$bc['pos_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('98','1');
			$bc['sus_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('8','1');
			$bc['pro_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('27','1');

			$bc['zonasi_desa']				= $this->web_app_model->get_zonasi_desa();

			$bc['grafik1'] 			= $this->load->view('graph/grafik1',$bc,true);
			$bc['grafik2'] 			= $this->load->view('graph/grafik2',$bc,true);
			$bc['grafik3'] 			= $this->load->view('graph/grafik3',$bc,true);
			$bc['grafik4'] 			= $this->load->view('graph/grafik4',$bc,true);
			$bc['grafik5'] 			= $this->load->view('graph/grafik5',$bc,true);

			$this->load->view('general/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	// START - PENGATURAN GENERAL
	public function bg_pengaturan_general()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin')
		{

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			//$bc['data_ruangan']		= $this->web_app_model->getAllData('tbl_ruang_rawat');
			//$bc['data_stts_pasien']	= $this->web_app_model->getAllData('tbl_status_pasien');
			//$bc['data_prov']			= $this->web_app_model->getAllData('tbl_prov');
			//$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');
			//$bc['data_desa']			= $this->web_app_model->getAllData('tbl_desa');
			//$bc['data_kota']			= $this->web_app_model->getAllData('tbl_kabkota');

			//$bc['data_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');



			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);

			$this->load->view('general/bg_pengaturan_general',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function update_config()
	{
		$config_nama_header			= $this->input->post('config_nama_header');
		$config_nama_footer			= $this->input->post('config_nama_footer');
		$config_durasi_isman		= $this->input->post('config_durasi_isman');
		$config_wa_sender			= $this->input->post('config_wa_sender');

		// get foto
		 $config['upload_path'] 	= './upload/logo';
		 $config['allowed_types'] 	= 'jpg|png|jpeg';
		 $config['max_size'] 		= '5000';  //5MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= 'logo_app';

     	 $this->load->library('upload', $config);

			if(!empty($_FILES['config_logo']['name'])) 
			{
		        if ( $this->upload->do_upload('config_logo') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'config_logo' 				=> $foto['file_name'],
						'config_nama_header' 		=> $config_nama_header,
						'config_nama_footer' 		=> $config_nama_footer,
						'config_durasi_isman' 		=> $config_durasi_isman,
						'config_wa_sender' 			=> $config_wa_sender,

						);

					$where = array(		
						'config_no'				=> 1,

						);

		
					$this->web_app_model->updateDataWhere($where,$data,'tbl_config');
					header('location:'.base_url().'index.php/admin/bg_pengaturan_general/?dt_config=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Logo dan Pengaturan berhasil diupdate!
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Logo dan Pengaturan berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
					$data = array(		
						'config_nama_header' 		=> $config_nama_header,
						'config_nama_footer' 		=> $config_nama_footer,
						'config_durasi_isman' 		=> $config_durasi_isman,
						'config_wa_sender' 			=> $config_wa_sender,

						);

					$where = array(		
						'config_no'				=> 1,

						);
					$this->web_app_model->updateDataWhere($where,$data,'tbl_config');

		          	header('location:'.base_url().'index.php/admin/bg_pengaturan_general?dt_config=1/');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!',
										                text:  'Pengaturan berhasi diupdate tanpa logo (File logo Max: 5mb)',
										                type: 'success',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
				$data = array(		
						'config_nama_header' 		=> $config_nama_header,
						'config_nama_footer' 		=> $config_nama_footer,
						'config_durasi_isman' 		=> $config_durasi_isman,
						'config_wa_sender' 			=> $config_wa_sender,

						);

				$where = array(		
					'config_no'				=> 1,

					);
				$this->web_app_model->updateDataWhere($where,$data,'tbl_config');
	          	header('location:'.base_url().'index.php/admin/bg_pengaturan_general?dt_config=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Success!',
									                text:  'Pengaturan berhasi diupdate tanpa logo!',
									                type: 'success',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}


	// END - PENGATURAN GENERAL

}
