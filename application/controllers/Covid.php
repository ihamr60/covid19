<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Covid extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function _construct()
	{
		session_start();
	}


	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(empty($cek))
		{
			$this->load->view('tampilan_login');
		}
		else
		{
			if($stts == 'rsud')
			{
				header('location:'.base_url().'index.php/rsud/bg_home');
			}
			else if($stts == 'dinkes')
			{
				header('location:'.base_url().'index.php/dinkes/bg_home');
			}
			else if($stts == 'surveyor')
			{
				header('location:'.base_url().'index.php/surveyor/bg_home');
			}
		}
	}

	public function login()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u, $p);
	}
	
	public function logout()
	{
		$cek  = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			header('location:'.base_url().'index.php/web');
		}
		else
		{
			$this->session->sess_destroy(); // memusnahkan sessionnya
			header('location:'.base_url().'index.php/web');
		}
	}



	public function data_covid()
    {
        //get data dari model
       // $data_covid = $this->web_app_model->getJoin3_dataCovid();
    	$data_covid			= $this->web_app_model->getWhereAllItem('1','pasien_verified_dinkes','tbl_pasien');
        //$status_pasien		= $this->web_app_model->getAllData('tbl_status_pasien');

        $status_parent		= $this->web_app_model->getAllData('tbl_status_pasien');
        $status_child		= $this->web_app_model->getAllData('tbl_tindakan_pasien');

        //masukkan data kedalam variabel
        //$data['data_covid'] = $data_covid;
        //$data['data_covid'] = $data_covid;
        
        //deklarasi variabel array
        $response = array();
        $posts = array();
        
        //lopping data dari database
        foreach ($data_covid->result_array() as $hasil)
        {
			$posts[] = array(
				
	            "pasien_no"       		=> $hasil['pasien_no'],
	            "pasien_status_parent"  => $hasil['pasien_status'],
	            "pasien_status_child"	=> $hasil['pasien_stts_tindakan'],
	            
	        );
        }

        foreach ($status_parent->result_array() as $hasil)
        {
			$posts2[] = array(
				
	            "status_parent_name"    => $hasil['sp_status'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }
        
        $posts2[] = array(
                "status_parent_name" => "Meninggal",
            );

        foreach ($status_child->result_array() as $hasil)
        {
			$posts3[] = array(
				
	            "status_child_name"    => $hasil['tindakan_nama'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }

        
        $response['status_parent'] = $posts2;
        $response['status_child'] = $posts3;
        $response['data_covid'] = $posts;
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);

    }


    public function covid_yesterday()
    {
        //get data dari model

        $tgl 					= $this->uri->segment(3);

        if(!empty($tgl))
        {
        	$covid_yesterday 	= $this->web_app_model->getWhereAllItem($tgl,'update_date','tbl_update_harian');
        }
        else
        {
    		$covid_yesterday 		= $this->web_app_model->getAllData('tbl_update_harian');
        }
        
        $status_parent		= $this->web_app_model->getAllData('tbl_status_pasien');
        $status_child		= $this->web_app_model->getAllData('tbl_tindakan_pasien');

        //deklarasi variabel array
        $response = array();
        $posts = array();
        
        //lopping data dari database
        foreach ($covid_yesterday->result_array() as $hasil)
        {

        	$pasien_nik	= $hasil['update_nik'];
			$jmlSensor	= 12;
			$afterVal	= 2;
			 
			// untuk mengambil 4 digit angka di tengah nomor hp yang akan disensor
			$sensor = substr($pasien_nik, $afterVal, $jmlSensor);
			 
			//untuk memecah bagian / kelompok angka pertama dan terakhir
			$pasien_nik2=explode($sensor,$pasien_nik);
			 
			// untuk menggabungkan angka pertama dan terakhir dengan angka tengah yang sudah di sensor
			$newPasienNik=$pasien_nik2[0]."XXXXXXXXXXXX".$pasien_nik2[1];
			 
			// menampilkan hasil data yang disensor
			//echo "<b>Nomor Handphone : </b>".$newPasienNik;


			$posts[] = array(
				
	            "pasien"   				=> $newPasienNik,
	            "update_stts_parent" 	=> $hasil['update_status_parent'],
	            "update_stts_child"		=> $hasil['update_status_child'],	
	            "updated_at"       		=> $hasil['update_date'],
	        );
        }

        foreach ($status_parent->result_array() as $hasil)
        {
			$posts2[] = array(
				
	            "status_parent_name"    => $hasil['sp_status'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }

        foreach ($status_child->result_array() as $hasil)
        {
			$posts3[] = array(
				
	            "status_child_name"    => $hasil['tindakan_nama'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }
       
        
        $response['status_parent'] 		= $posts2;
        $response['status_child'] 		= $posts3;
        $response['covid_yesterday'] 	= $posts;
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);

    }
    
    public function apiCovid()
    {
    	$data_covid			= $this->web_app_model->getWhereAllItem('1','pasien_verified_dinkes','tbl_pasien');

        $status_parent		= $this->web_app_model->getAllData('tbl_status_pasien');
        $status_child		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
        
        $count = 0;
        // $temp = array();
        
        // foreach($status_parent->result_array() as $item){
            
	       //foreach($data_covid->result_array() as $item2){
	       //    if ($item2['pasien_status'] == $item['sp_status']){
	       //        $count+=1;
	       //    }
	       //}
	       
        // }
        // var_dump($count);
        
        //deklarasi variabel array
        $response = array();
        $posts = array();
        
        
        //lopping data dari database
        foreach ($data_covid->result_array() as $hasil)
        {
			$posts[] = array(
				
	            "pasien_no"       		=> $hasil['pasien_no'],
	            "pasien_status_parent"  => $hasil['pasien_status'],
	            "pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }
        
        

        foreach ($status_parent->result_array() as $hasil)
        {
            $count = 0;
            foreach ($data_covid->result_array() as $item)
            {
    			if ($item['pasien_status'] == $hasil['sp_status']){
    			    $count+=1;
    			}
            }
            
			$posts2[] = array(
				
	            "status_parent_name"    => $hasil['sp_status'],
	            "total" => $count,
	        );
        }
        
        $count = 0;
        foreach ($data_covid->result_array() as $item)
        {
			if ($item['pasien_status'] == "Meninggal"){
			    $count+=1;
			}
        }
        
        
        // $posts2[] = array(
            
        //         "status_parent_name" => "Meninggal",
        //         "total" => $count,
        //     );
        
        $posts3 = array();
        
        foreach ($status_child->result_array() as $hasil)
        {
			$posts3[] = array(
				
	            "status_child_name"    => $hasil['tindakan_nama'],
	            
	        );
        }
        
        

        $posts4 = array();
        // $temp = array();
        $temp = [];
        
        foreach ($status_parent->result_array() as $item)
        {
			foreach ($status_child->result_array() as $item2)
			{
			    
			    $count = 0;
    		    foreach($data_covid->result_array() as $item3)
    		    {
    		        
    		        if ($item3['pasien_status'] == $item['sp_status'])
    		        {
    		            if($item3['pasien_stts_tindakan'] == $item2['tindakan_nama'])
    		            {
    		                
    		                $posts4[] = array(
    		                    "status_parent_name" => $item['sp_status'],
                	            "status_child_name"    => $item2['tindakan_nama'],
                	            "total" => $count,
                	        );
                	   //     $count+=1;
    		            }
    		        }
    		    }
			}
        }
        
        // var_dump($temp[0][0]);
        // print_r(array_unique($a));
        
        // $result = array_unique($temp);
        // var_dump($result);
        // var_dump($temp);
        
        
        
    
        // $response['test'] = $posts4;
        $response['status_parent'] = $posts2;
        $response['status_child'] = $posts3;
        $response['data_covid'] = $posts;
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);
    }

    public function csv()
    {
		$config			= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
		$this->db->query("TRUNCATE TABLE tbl_vaksinasi");
		if(($handle		=	fopen("upload/vaksin/".$config['config_csv_vaksin']."", "r")) !== FALSE)
		{
			while(($row	=	fgetcsv($handle)) !== FALSE)
			{
				$data = [
					'tanggal' 			=> $row[0],
					'provinsi' 			=> $row[1],
					'kabupaten' 		=> $row[2],
					'kecamatan' 		=> $row[3],
					'faskes' 			=> $row[4],
					'nik' 				=> $row[5],
					'nama' 				=> $row[6],
					'jenis_kelamin' 	=> $row[7],
					'kelompok_usia' 	=> $row[8],
					'kategori' 			=> $row[9],
					'sub_kategori' 		=> $row[10],
					'vaksinasi' 		=> $row[11],
					'tiket_vaksinasi' 	=> $row[12],
					'jenis_vaksin' 		=> $row[13],
				];
				$this->web_app_model->insertData($data,'tbl_vaksinasi');
			}
			fclose($handle);
		}
		$this->db->query("DELETE FROM tbl_vaksinasi WHERE provinsi='Provinsi'");
    }
}
