<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function wa_blast()
	{
		$data = [
		    'api_key' => 'ffec0288a1951c00c9114eb448b99aa93ee088fd',
		    'sender'  => '6285358868256',
		    'number'  => '085361885100',
		    'message' => 'Testing by API 1'
		];

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://wa.langsakota.go.id/api/send-message.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($data))
		);

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;


		$data = [
		    'api_key' => 'ffec0288a1951c00c9114eb448b99aa93ee088fd',
		    'sender'  => '628116721484',
		    'number'  => '085361885100',
		    'message' => 'Testing by API 2'
		];

		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://wa.langsakota.go.id/api/send-message.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($data))
		);

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
			}
}
