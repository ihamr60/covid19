<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rsud extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			$url="https://apicovid.bravo.siat.web.id/api/v_vaksin_on?_where=(nama_kab,eq,KOTA+LANGSA)&_size=23";
			$get_url = file_get_contents($url);
			$data = json_decode($get_url);

			$bc = array(
			'vaksin' => $data
			);

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END


			$bc['zonasi_kec']		= $this->web_app_model->get_zonasi_kec();
			$bc['zonasi_desa']		= $this->web_app_model->get_zonasi_desa();
			$bc['data_desa']		= $this->web_app_model->getAllData('tbl_desa');
			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);	

			$bc['tot_positif_sembuh']		= $this->web_app_model->tot_positif_sembuh();
			$bc['tot_positif_dirawat']		= $this->web_app_model->tot_positif_dirawat();
			$bc['tot_positif_dipantau']		= $this->web_app_model->tot_positif_dipantau();
			$bc['tot_positif_isman']		= $this->web_app_model->tot_positif_isman();
			$bc['tot_positif_meninggal']	= $this->web_app_model->tot_positif_meninggal();

			$bc['tot_suspect_sembuh']		= $this->web_app_model->total_pasien_where('23','1');
			$bc['tot_suspect_dirawat']		= $this->web_app_model->total_pasien_where('1','1');
			$bc['tot_suspect_dipantau']		= $this->web_app_model->total_pasien_where('15','1');
			$bc['tot_suspect_isman']		= $this->web_app_model->total_pasien_where('2','1');
			$bc['tot_suspect_meninggal']	= $this->web_app_model->total_pasien_where('8','1');

			$bc['pos_pasienLuar_dirawat']	= $this->web_app_model->total_pasien_where('30','0');
			$bc['pos_pasienLuar_dipantau']	= $this->web_app_model->total_pasien_where('33','0');
			$bc['pos_pasienLuar_isman']		= $this->web_app_model->total_pasien_where('31','0');

			$bc['pos_pasienLokal_dirawat']	= $this->web_app_model->total_pasien_where('30','1');
			$bc['pos_pasienLokal_dipantau']	= $this->web_app_model->total_pasien_where('33','1');
			$bc['pos_pasienLokal_isman']	= $this->web_app_model->total_pasien_where('31','1');

			$bc['pos_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('98','1');
			$bc['sus_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('8','1');
			$bc['pro_pasienLokal_meninggal']= $this->web_app_model->total_pasien_where('27','1');

			$bc['zonasi_desa']				= $this->web_app_model->get_zonasi_desa();

			$bc['grafik1'] 			= $this->load->view('graph/grafik1',$bc,true);
			$bc['grafik2'] 			= $this->load->view('graph/grafik2',$bc,true);
			$bc['grafik3'] 			= $this->load->view('graph/grafik3',$bc,true);
			$bc['grafik4'] 			= $this->load->view('graph/grafik4',$bc,true);
			$bc['grafik5'] 			= $this->load->view('graph/grafik5',$bc,true);

			$this->load->view('general/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	// START - FORM INPUT PASIEN
	public function bg_tambah_pasien()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_ruangan']		= $this->web_app_model->getAllData('tbl_ruang_rawat');
			$bc['data_stts_pasien']	= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['data_prov']		= $this->web_app_model->getAllData('tbl_prov');
			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');
			$bc['data_desa']		= $this->web_app_model->getAllData('tbl_desa');
			$bc['data_kota']		= $this->web_app_model->getAllData('tbl_kabkota');

			$bc['data_tindakan']	= $this->web_app_model->getAllData('tbl_tindakan_pasien');

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);

			$this->load->view('general/bg_tambah_pasien',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_tambah_pasien_luar()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_ruangan']		= $this->web_app_model->getAllData('tbl_ruang_rawat');
			$bc['data_stts_pasien']	= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['data_prov']		= $this->web_app_model->getAllData('tbl_prov');
			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');
			$bc['data_desa']		= $this->web_app_model->getAllData('tbl_desa');
			$bc['data_kota']		= $this->web_app_model->getAllData('tbl_kabkota');

			$bc['data_tindakan']	= $this->web_app_model->getAllData('tbl_tindakan_pasien');

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);

			$this->load->view('general/bg_tambah_pasien_luar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	// END - MENU FORM INPUT PASIEN

	// START - DATA MASTER RS
	public function bg_rs()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_rs']			= $this->web_app_model->getAllData('tbl_rs');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahRs'] 	= $this->load->view('general/modalTambahRs',$bc,true);
			$bc['modalEditRs'] 		= $this->load->view('general/modalEditRs',$bc,true);	

			$this->load->view('general/bg_data_rs',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahRs()
	{
		$rs_nama 			= $this->input->post('rs_nama');

		$data = array(		
			'rs_nama' 		=> $rs_nama,
			);

		$this->web_app_model->insertData($data,'tbl_rs');
		header('location:'.base_url().'index.php/rsud/bg_rs?dt_rs=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Rumah Sakit berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data RS berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editRs()
	{
		$rs_nama 			= $this->input->post('rs_nama');
		$rs_no 				= $this->input->post('rs_no');

		$data = array(		
			'rs_nama' 		=> $rs_nama,
			);

		$where = array(
			'rs_no'			=> $rs_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_rs');
		header('location:'.base_url().'index.php/rsud/bg_rs?dt_rs=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Rumah Sakit berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data RS berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_rs()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('rs_no'=>$id);


		$this->web_app_model->deleteData('tbl_rs',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_rs?dt_rs=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data RS berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data RS berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	// END - DATA MASTER RS



	// START - DATA MASTER STATUS PASIEN
	public function bg_status()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_status']		= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahStatus']= $this->load->view('general/modalTambahStatus',$bc,true);
			$bc['modalEditStatus'] 	= $this->load->view('general/modalEditStatus',$bc,true);	

			$this->load->view('general/bg_status',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahStatus()
	{
		$sp_status 			= $this->input->post('sp_status');

		$data = array(		
			'sp_status' 	=> $sp_status,
			);

		$this->web_app_model->insertData($data,'tbl_status_pasien');
		header('location:'.base_url().'index.php/rsud/bg_status?dt_status=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Status Pasien berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Status Pasien berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editStatus()
	{
		$sp_status 			= $this->input->post('sp_status');
		$sp_no 				= $this->input->post('sp_no');

		$data = array(		
			'sp_status' 		=> $sp_status,
			);

		$where = array(
			'sp_no'			=> $sp_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_status_pasien');
		header('location:'.base_url().'index.php/rsud/bg_status?dt_status=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Status Pasien berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Status Pasien berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_status()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('sp_no'=>$id);


		$this->web_app_model->deleteData('tbl_status_pasien',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_status?dt_status=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Status Pasien berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Status Pasien berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	// END - DATA MASTER STATUS PASIEN



	// START - DATA MASTER DESA
	public function bg_data_desa()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');
			$bc['data_desa']		= $this->web_app_model->getAll2Join('desa_kec','kec_no','tbl_desa','tbl_kec');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahDesa'] 	= $this->load->view('general/modalTambahDesa',$bc,true);
			$bc['modalEditDesa'] 	= $this->load->view('general/modalEditDesa',$bc,true);	

			$this->load->view('general/bg_data_desa',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahDesa()
	{
		$desa_nama 			= $this->input->post('desa_nama');

		$data = array(		
			'desa_nama' 		=> $desa_nama,
			);

		$this->web_app_model->insertData($data,'tbl_desa');
		header('location:'.base_url().'index.php/rsud/bg_data_desa?dt_desa=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Desa berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Desa berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editDesa()
	{
		$desa_nama 			= $this->input->post('desa_nama');
		$desa_no 			= $this->input->post('desa_no');

		$data = array(		
			'desa_nama' 	=> $desa_nama,
			);

		$where = array(
			'desa_no'		=> $desa_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_desa');
		header('location:'.base_url().'index.php/rsud/bg_data_desa?dt_desa=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Desa berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Desa berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_desa()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('desa_no'=>$id);


		$this->web_app_model->deleteData('tbl_desa',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_data_desa?dt_desa=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Desa berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Desa berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER DESA 



	// START - DATA MASTER KECAMATAN
	public function bg_data_kec()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_kabkota']		= $this->web_app_model->getAllData('tbl_kabkota');
			$bc['data_kec']			= $this->web_app_model->getAll2Join('kec_kabkota','kabkota_no','tbl_kec','tbl_kabkota');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahKec'] 	= $this->load->view('general/modalTambahKec',$bc,true);
			$bc['modalEditKec'] 	= $this->load->view('general/modalEditKec',$bc,true);	

			$this->load->view('general/bg_data_kec',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahKec()
	{
		$kec_nama 			= $this->input->post('kec_nama');

		$data = array(		
			'kec_nama' 		=> $kec_nama,
			);

		$this->web_app_model->insertData($data,'tbl_kec');
		header('location:'.base_url().'index.php/rsud/bg_data_kec?dt_kec=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kecamatan berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kecamatan berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editKec()
	{
		$kec_nama 			= $this->input->post('kec_nama');
		$kec_no 			= $this->input->post('kec_no');

		$data = array(		
			'kec_nama' 		=> $kec_nama,
			);

		$where = array(
			'kec_no'		=> $kec_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_kec');
		header('location:'.base_url().'index.php/rsud/bg_data_kec?dt_kec=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kecamatan berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kecamatan berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_kec()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kec_no'=>$id);


		$this->web_app_model->deleteData('tbl_kec',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_data_kec?dt_kec=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kecamatan berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kecamatan berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER KECAMATAN



	// START - DATA MASTER KABKOTA
	public function bg_kabkota()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_prov']		= $this->web_app_model->getAllData('tbl_prov');
			$bc['data_kabkota']		= $this->web_app_model->getAll2Join('kabkota_prov','prov_no','tbl_kabkota','tbl_prov');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahKabKota'] 	= $this->load->view('general/modalTambahKabKota',$bc,true);
			$bc['modalEditKabKota'] 	= $this->load->view('general/modalEditKabKota',$bc,true);	

			$this->load->view('general/bg_kabkota',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahKabKota()
	{
		$kabkota_nama 			= $this->input->post('kabkota_nama');

		$data = array(		
			'kabkota_nama' 		=> $kabkota_nama,
			);

		$this->web_app_model->insertData($data,'tbl_kabkota');
		header('location:'.base_url().'index.php/rsud/bg_kabkota?dt_kabkota=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kapupaten / Kota berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kapupaten / Kota berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editKabKota()
	{
		$kabkota_nama 			= $this->input->post('kabkota_nama');
		$kabkota_no 			= $this->input->post('kabkota_no');

		$data = array(		
			'kabkota_nama' 		=> $kabkota_nama,
			);

		$where = array(
			'kabkota_no'		=> $kabkota_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_kabkota');
		header('location:'.base_url().'index.php/rsud/bg_kabkota?dt_kabkota=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kabupaten / Kota berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kabupaten / Kota berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_kabkota()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('kabkota_no'=>$id);


		$this->web_app_model->deleteData('tbl_kabkota',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_kabkota?dt_kabkota=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kabupaten / Kota berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kabupaten / Kota berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER KAB/KOTA


		// START - DATA MASTER PROVINSI
	public function bg_prov()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END


			$bc['data_negara']		= $this->web_app_model->getAllData('tbl_negara');
			$bc['data_prov']		= $this->web_app_model->getAll2Join('prov_negara','negara_no','tbl_prov','tbl_negara');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahProv'] 	= $this->load->view('general/modalTambahProv',$bc,true);
			$bc['modalEditProv'] 	= $this->load->view('general/modalEditProv',$bc,true);	

			$this->load->view('general/bg_prov',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahProv()
	{
		$prov_nama 			= $this->input->post('prov_nama');

		$data = array(		
			'prov_nama' 	=> $prov_nama,
			);

		$this->web_app_model->insertData($data,'tbl_prov');
		header('location:'.base_url().'index.php/rsud/bg_prov?dt_prov=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Provinsi berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Provinsi berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editProv()
	{
		$prov_nama 			= $this->input->post('prov_nama');
		$prov_no 			= $this->input->post('prov_no');

		$data = array(		
			'prov_nama' 		=> $prov_nama,
			);

		$where = array(
			'prov_no'		=> $prov_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_prov');
		header('location:'.base_url().'index.php/rsud/bg_prov?dt_prov=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Provinsi berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Provinsi berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_prov()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('prov_no'=>$id);


		$this->web_app_model->deleteData('tbl_prov',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_prov?dt_prov=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Provinsi berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Provinsi berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER PROVINSI


	// START - DATA MASTER NEGARA
	public function bg_negara()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_negara']		= $this->web_app_model->getAllData('tbl_negara');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahNegara']= $this->load->view('general/modalTambahNegara',$bc,true);
			$bc['modalEditNegara'] 	= $this->load->view('general/modalEditNegara',$bc,true);	

			$this->load->view('general/bg_negara',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahNegara()
	{
		$negara_nama 			= $this->input->post('negara_nama');

		$data = array(		
			'negara_nama' 		=> $negara_nama,
			);

		$this->web_app_model->insertData($data,'tbl_negara');
		header('location:'.base_url().'index.php/rsud/bg_negara?dt_negara=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Negara berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Negara berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editNegara()
	{
		$negara_nama 			= $this->input->post('negara_nama');
		$negara_no 				= $this->input->post('negara_no');

		$data = array(		
			'negara_nama' 		=> $negara_nama,
			);

		$where = array(
			'negara_no'			=> $negara_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_negara');
		header('location:'.base_url().'index.php/rsud/bg_negara?dt_negara=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Negara berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Negara berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_negara()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('negara_no'=>$id);


		$this->web_app_model->deleteData('tbl_negara',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_negara?dt_negara=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Negara berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Negara berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER NEGARA

	// START - DATA MASTER LAB
	public function bg_lab()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_lab']			= $this->web_app_model->getAllData('tbl_lab');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahLab']= $this->load->view('general/modalTambahLab',$bc,true);
			$bc['modalEditLab'] 	= $this->load->view('general/modalEditLab',$bc,true);	

			$this->load->view('general/bg_lab',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahLab()
	{
		$lab_nama 			= $this->input->post('lab_nama');

		$data = array(		
			'lab_nama' 		=> $lab_nama,
			);

		$this->web_app_model->insertData($data,'tbl_lab');
		header('location:'.base_url().'index.php/rsud/bg_lab?dt_lab=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Laboratorium berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Laboratorium berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editLab()
	{
		$lab_nama 				= $this->input->post('lab_nama');
		$lab_no 				= $this->input->post('lab_no');

		$data = array(		
			'lab_nama' 		=> $lab_nama,
			);

		$where = array(
			'lab_no'			=> $lab_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_lab');
		header('location:'.base_url().'index.php/rsud/bg_lab?dt_lab=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Laboratorium berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Laboratorium berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_lab()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('lab_no'=>$id);


		$this->web_app_model->deleteData('tbl_lab',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_lab?dt_lab=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Laboratorium berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Laboratorium berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER LAB


	// START - DATA MASTER PROFESI
	public function bg_profesi()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_profesi']		= $this->web_app_model->getAllData('tbl_pekerjaan');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahProfesi']	= $this->load->view('general/modalTambahProfesi',$bc,true);
			$bc['modalEditProfesi'] 	= $this->load->view('general/modalEditProfesi',$bc,true);	

			$this->load->view('general/bg_profesi',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahProfesi()
	{
		$pekerjaan_nama 			= $this->input->post('pekerjaan_nama');

		$data = array(		
			'pekerjaan_nama' 		=> $pekerjaan_nama,
			);

		$this->web_app_model->insertData($data,'tbl_pekerjaan');
		header('location:'.base_url().'index.php/rsud/bg_profesi?dt_profesi=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Profesi berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Profesi berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editProfesi()
	{
		$pekerjaan_nama 			= $this->input->post('pekerjaan_nama');
		$pekerjaan_no 				= $this->input->post('pekerjaan_no');

		$data = array(		
			'pekerjaan_nama' 		=> $pekerjaan_nama,
			);

		$where = array(
			'pekerjaan_no'			=> $pekerjaan_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_pekerjaan');
		header('location:'.base_url().'index.php/rsud/bg_profesi?dt_profesi=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Profesi berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Profesi berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_profesi()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('pekerjaan_no'=>$id);


		$this->web_app_model->deleteData('tbl_pekerjaan',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_profesi?dt_profesi=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Profesi berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Profesi berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER PROFESI




	// START - DATA MASTER SPESIMEN
	public function bg_spesimen()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_spesimen']	= $this->web_app_model->getAllData('tbl_jenis_spesimen');
			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahSpesimen']	= $this->load->view('general/modalTambahSpesimen',$bc,true);
			$bc['modalEditSpesimen'] 	= $this->load->view('general/modalEditSpesimen',$bc,true);	

			$this->load->view('general/bg_spesimen',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahSpesimen()
	{
		$jenis_spesi_nama 			= $this->input->post('jenis_spesi_nama');

		$data = array(		
			'jenis_spesi_nama' 		=> $jenis_spesi_nama,
			);

		$this->web_app_model->insertData($data,'tbl_jenis_spesimen');
		header('location:'.base_url().'index.php/rsud/bg_spesimen?dt_spesimen=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Spesimen berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Spesimen berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editSpesimen()
	{
		$jenis_spesi_nama 			= $this->input->post('jenis_spesi_nama');
		$jenis_spesi_no 			= $this->input->post('jenis_spesi_no');

		$data = array(		
			'jenis_spesi_nama' 		=> $jenis_spesi_nama,
			);

		$where = array(
			'jenis_spesi_no'		=> $jenis_spesi_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_jenis_spesimen');
		header('location:'.base_url().'index.php/rsud/bg_spesimen?dt_spesimen=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Spesimen berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Spesimen berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_spesimen()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('jenis_spesi_no'=>$id);


		$this->web_app_model->deleteData('tbl_jenis_spesimen',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_spesimen?dt_spesimen=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Spesimen berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Spesimen berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER SPESIMEN



	// START - DATA MASTER KATEG HASIL SPESIMEN
	public function bg_kategHasilSpesimen()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_hsl_spesimen']	= $this->web_app_model->getAllData('tbl_kateg_hasil_spesimen');
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahKategHasilSpesimen']	= $this->load->view('general/modalTambahKategHasilSpesimen',$bc,true);
			$bc['modalEditKategHasilSpesimen'] 		= $this->load->view('general/modalEditKategHasilSpesimen',$bc,true);	

			$this->load->view('general/bg_kategHasilSpesimen',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahKategHasilSpesimen()
	{
		$spesimen_nama 			= $this->input->post('spesimen_nama');

		$data = array(		
			'spesimen_nama' 	=> $spesimen_nama,
			);

		$this->web_app_model->insertData($data,'tbl_kateg_hasil_spesimen');
		header('location:'.base_url().'index.php/rsud/bg_kategHasilSpesimen?dt_hsl_spesimen=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kategori Hasil Spesimen berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kategori Hasil Spesimen berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editKategHasilSpesimen()
	{
		$spesimen_nama 			= $this->input->post('spesimen_nama');
		$spesimen_no 			= $this->input->post('spesimen_no');

		$data = array(		
			'spesimen_nama' 	=> $spesimen_nama,
			);

		$where = array(
			'spesimen_no'		=> $spesimen_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_kateg_hasil_spesimen');
		header('location:'.base_url().'index.php/rsud/bg_kategHasilSpesimen?dt_hsl_spesimen=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kategori Hasil Spesimen berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kategori Hasil Spesimen berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_kategHasilSpesimen()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('spesimen_no'=>$id);


		$this->web_app_model->deleteData('tbl_kateg_hasil_spesimen',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_kategHasilSpesimen?dt_hsl_spesimen=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kategori Hasil Spesimen berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kategori Hasil Spesimen berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER KATEG HASIL SPESIMEN


	// START - DATA MASTER KATEG HUB KONTAK
	public function bg_kategHub()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_hubKontak']		= $this->web_app_model->getAllData('tbl_kateg_hubungan_kontak');
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_kategHubKontak',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahKategHubKontak()
	{
		$tkh_hubungan 			= $this->input->post('tkh_hubungan');

		$data = array(		
			'tkh_hubungan' 	=> $tkh_hubungan,
			);

		$this->web_app_model->insertData($data,'tbl_kateg_hubungan_kontak');
		header('location:'.base_url().'index.php/rsud/bg_kategHub?dt_kateghub=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kategori Hubungan Kontak berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kategori Hubungan Kontak berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editKategHubKontak()
	{
		$tkh_hubungan 			= $this->input->post('tkh_hubungan');
		$tkh_no 				= $this->input->post('tkh_no');

		$data = array(		
			'tkh_hubungan' 		=> $tkh_hubungan,
			);

		$where = array(
			'tkh_no'			=> $tkh_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_kateg_hubungan_kontak');
		header('location:'.base_url().'index.php/rsud/bg_kategHub?dt_kateghub=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Kategori Hubungan Kontak berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Kategori Hubungan Kontak berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_kategHubKontak()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('tkh_no'=>$id);


		$this->web_app_model->deleteData('tbl_kateg_hubungan_kontak',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_kategHub?dt_kateghub=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Kategori Hubungan Kontak berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Kategori Hubungan Kontak berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER KATEG HUB KONTAK



	// START - DATA MASTER FASKES
	public function bg_faskes()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_faskes']			= $this->web_app_model->getAllData('tbl_faskes');
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahFaskes']	= $this->load->view('general/modalTambahFaskes',$bc,true);
			$bc['modalEditFaskes'] 		= $this->load->view('general/modalEditFaskes',$bc,true);	

			$this->load->view('general/bg_faskes',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahFaskes()
	{
		$faskes_nama 			= $this->input->post('faskes_nama');

		$data = array(		
			'faskes_nama' 		=> $faskes_nama,
			);

		$this->web_app_model->insertData($data,'tbl_faskes');
		header('location:'.base_url().'index.php/rsud/bg_faskes?dt_faskes=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Fasilitas Kesehatan berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Fasilitas Kesehatan berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editFaskes()
	{
		$faskes_nama 			= $this->input->post('faskes_nama');
		$faskes_no 				= $this->input->post('faskes_no');

		$data = array(		
			'faskes_nama' 		=> $faskes_nama,
			);

		$where = array(
			'faskes_no'			=> $faskes_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_faskes');
		header('location:'.base_url().'index.php/rsud/bg_faskes?dt_faskes=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Fasilitas Kesehatan berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Fasilitas Kesehatan berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_faskes()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('faskes_no'=>$id);


		$this->web_app_model->deleteData('tbl_faskes',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_faskes?dt_faskes=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Fasilitas Kesehatan berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Fasilitas Kesehatan berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER FASKES



	// START - DATA MASTER TINDAKAN PASIEN
	public function bg_tindakan()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['status_pasien']		= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['data_tindakan']		= $this->web_app_model->getAll2Join('status_pasien','sp_no','tbl_tindakan_pasien','tbl_status_pasien');
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			$bc['modalTambahTindakan']	= $this->load->view('general/modalTambahTindakan',$bc,true);
			$bc['modalEditTindakan'] 	= $this->load->view('general/modalEditTindakan',$bc,true);	

			$this->load->view('general/bg_tindakan',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function tambahTindakan()
	{
		$tindakan_nama 			= $this->input->post('tindakan_nama');

		$data = array(		
			'tindakan_nama' 	=> $tindakan_nama,
			);

		$this->web_app_model->insertData($data,'tbl_tindakan_pasien');
		header('location:'.base_url().'index.php/rsud/bg_tindakan?dt_tindakan=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Jenis Tindakan Pasien berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Jenis Tindakan Pasien berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function editTindakan()
	{
		$tindakan_nama 			= $this->input->post('tindakan_nama');
		$tindakan_no 			= $this->input->post('tindakan_no');

		$data = array(		
			'tindakan_nama' 	=> $tindakan_nama,
			);

		$where = array(
			'tindakan_no'		=> $tindakan_no,
			);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_tindakan_pasien');
		header('location:'.base_url().'index.php/rsud/bg_tindakan?dt_tindakan=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Jenis Tindakan Pasien berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Jenis Tindakan Pasien berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function hapus_tindakan()
	{

		$id 			= $this->uri->segment(3);
		$hapus 			= array('tindakan_no'=>$id);


		$this->web_app_model->deleteData('tbl_tindakan_pasien',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_tindakan?dt_tindakan=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Jenis Tindakan Pasien berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Jenis Tindakan Pasien berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}
	// END - DATA MASTER TINDAKAN PASIEN




	// START - CRUB DATA PASIEN 

	public function tambahPasien()
	{
		date_default_timezone_set('Asia/Jakarta');

		$pasien_nik 		= $this->input->post('pasien_nik');
		$pasien_no_rm 		= $this->input->post('pasien_no_rm');
		$pasien_nama 		= $this->input->post('pasien_nama');
		$pasien_tlp 		= $this->input->post('pasien_tlp');
		$pasien_tgl_lhr 	= $this->input->post('pasien_tgl_lhr');
		$pasien_kelamin 	= $this->input->post('pasien_kelamin');

		$pasien_alamat 		= $this->input->post('pasien_alamat');
		$pasien_desa 		= $this->input->post('pasien_desa');
		//$pasien_kabkota 	= $this->input->post('pasien_kabkota');
		//$pasien_kec 		= $this->input->post('pasien_kec');
		//$pasien_provinsi 	= $this->input->post('pasien_provinsi');
		$pasien_tgl_masuk 	= $this->input->post('pasien_tgl_masuk');


		$pasien_tgl_keluar 	= $this->input->post('pasien_tgl_keluar');
		//$pasien_status 		= $this->input->post('pasien_status');

		$pasien_no_swabpcr 		= $this->input->post('pasien_no_swabpcr');
		$pasien_tgl_pengambilan_swab 	= $this->input->post('pasien_tgl_pengambilan_swab');
		$pasien_tgl_hasil_lab 	= $this->input->post('pasien_tgl_hasil_lab');
		$pasien_ruang_rawat 	= $this->input->post('pasien_ruang_rawat');
		$pasien_ket 			= $this->input->post('pasien_ket');

		$pasien_penginput 		= $this->session->userdata('nama');
		$pasien_stts_tindakan	= $this->input->post('pasien_stts_tindakan');
		$pasien_verified_dinkes	= '0';
		$pasien_lokal			= '1';

		$pasien_tgl_lapor		= date('Y-m-d');//$this->input->post('pasien_tgl_lapor');

		// =====
		$kd_kec_pasien			= $this->web_app_model->getWhereOneItem($pasien_desa,'desa_no','tbl_desa');
		$dt_tracert 		= $this->web_app_model->getWhereOneItem($kd_kec_pasien['desa_kec'],'tracert_kd_kec','tbl_tracert');

		if($pasien_stts_tindakan == 31 OR $pasien_stts_tindakan == 26 OR $pasien_stts_tindakan == 2 OR $pasien_stts_tindakan == 15 OR $pasien_stts_tindakan == 28 OR $pasien_stts_tindakan == 33)
		{
			$pasien_tracert = $dt_tracert['tracert_username'];
			$pasien_tracing = '1';
		}
		else
		{
			$pasien_tracert = "";
			$pasien_tracing = "";
		}
		//=====

		$data = array(		
			'pasien_nik' 	=> $pasien_nik,
			'pasien_no_rm' 	=> $pasien_no_rm,
			'pasien_nama' 	=> $pasien_nama,
			'pasien_tlp' 	=> $pasien_tlp,
			'pasien_tgl_lhr'=> $pasien_tgl_lhr,
			'pasien_kelamin'=> $pasien_kelamin,

			'pasien_alamat' 	=> $pasien_alamat,
			'pasien_desa' 		=> $pasien_desa,
			//'pasien_kabkota'	=> $pasien_kabkota,
			//'pasien_kec' 		=> $pasien_kec,
			//'pasien_provinsi' 	=> $pasien_provinsi,
			'pasien_tgl_masuk' 	=> $pasien_tgl_masuk,

			'pasien_tgl_keluar' 			=> $pasien_tgl_keluar,
			//'pasien_status' 				=> $pasien_status,
			'pasien_no_swabpcr' 			=> $pasien_no_swabpcr,
			'pasien_tgl_pengambilan_swab' 	=> $pasien_tgl_pengambilan_swab,
			'pasien_tgl_hasil_lab' 			=> $pasien_tgl_hasil_lab,
			'pasien_ruang_rawat' 			=> $pasien_ruang_rawat,
			'pasien_ket' 					=> $pasien_ket,

			'pasien_penginput' 				=> $pasien_penginput,
			'pasien_verified_dinkes' 		=> $pasien_verified_dinkes,
			'pasien_stts_tindakan' 			=> $pasien_stts_tindakan,
			'pasien_lokal' 					=> $pasien_lokal,

			'pasien_tgl_lapor' 				=> $pasien_tgl_lapor,
			'pasien_tracert' 				=> $pasien_tracert,
			'pasien_tracing' 				=> $pasien_tracing,
			);

		$this->web_app_model->insertData($data,'tbl_pasien');
		header('location:'.base_url().'index.php/rsud/bg_tambah_info_pasien/'.$pasien_nik.'?dt_tmbhpasien=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pasien berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Pasien berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function tambahPasienLuar()
	{
		date_default_timezone_set('Asia/Jakarta');

		$pasien_nik 		= $this->input->post('pasien_nik');
		$pasien_no_rm 		= $this->input->post('pasien_no_rm');
		$pasien_nama 		= $this->input->post('pasien_nama');
		$pasien_tlp 		= $this->input->post('pasien_tlp');
		$pasien_tgl_lhr 	= $this->input->post('pasien_tgl_lhr');
		$pasien_kelamin 	= $this->input->post('pasien_kelamin');

		$pasien_alamat 		= $this->input->post('pasien_alamat');
		$pasien_desa 		= $this->input->post('pasien_desa');
		$pasien_kabkota 	= $this->input->post('pasien_kabkota');
		$pasien_kec 		= $this->input->post('pasien_kec');
		$pasien_provinsi 	= $this->input->post('pasien_provinsi');
		$pasien_tgl_masuk 	= $this->input->post('pasien_tgl_masuk');


		$pasien_tgl_keluar 	= $this->input->post('pasien_tgl_keluar');
		//$pasien_status 		= $this->input->post('pasien_status');

		$pasien_no_swabpcr 		= $this->input->post('pasien_no_swabpcr');
		$pasien_tgl_pengambilan_swab 	= $this->input->post('pasien_tgl_pengambilan_swab');
		$pasien_tgl_hasil_lab 	= $this->input->post('pasien_tgl_hasil_lab');
		$pasien_ruang_rawat 	= $this->input->post('pasien_ruang_rawat');
		$pasien_ket 			= $this->input->post('pasien_ket');

		$pasien_penginput 		= $this->session->userdata('nama');
		$pasien_stts_tindakan	= $this->input->post('pasien_stts_tindakan');
		$pasien_verified_dinkes	= '0';
		$pasien_lokal			= '0';

		$pasien_tgl_lapor		= date('Y-m-d');//$this->input->post('pasien_tgl_lapor');


		$data = array(		
			'pasien_nik' 	=> $pasien_nik,
			'pasien_no_rm' 	=> $pasien_no_rm,
			'pasien_nama' 	=> $pasien_nama,
			'pasien_tlp' 	=> $pasien_tlp,
			'pasien_tgl_lhr'=> $pasien_tgl_lhr,
			'pasien_kelamin'=> $pasien_kelamin,

			'pasien_alamat' 	=> $pasien_alamat,
			'pasien_desa' 		=> $pasien_desa,
			'pasien_kabkota'	=> $pasien_kabkota,
			'pasien_kec' 		=> $pasien_kec,
			'pasien_provinsi' 	=> $pasien_provinsi,
			'pasien_tgl_masuk' 	=> $pasien_tgl_masuk,

			'pasien_tgl_keluar' 			=> $pasien_tgl_keluar,
			//'pasien_status' 				=> $pasien_status,
			'pasien_no_swabpcr' 			=> $pasien_no_swabpcr,
			'pasien_tgl_pengambilan_swab' 	=> $pasien_tgl_pengambilan_swab,
			'pasien_tgl_hasil_lab' 			=> $pasien_tgl_hasil_lab,
			'pasien_ruang_rawat' 			=> $pasien_ruang_rawat,
			'pasien_ket' 					=> $pasien_ket,

			'pasien_penginput' 				=> $pasien_penginput,
			'pasien_verified_dinkes' 		=> $pasien_verified_dinkes,
			'pasien_stts_tindakan' 			=> $pasien_stts_tindakan,
			'pasien_lokal' 					=> $pasien_lokal,

			'pasien_tgl_lapor' 				=> $pasien_tgl_lapor,
			);

		$this->web_app_model->insertData($data,'tbl_pasien');
		header('location:'.base_url().'index.php/rsud/bg_tambah_info_pasien/'.$pasien_nik.'?dt_tmbhpasien=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pasien berhasil ditambahkan!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Pasien berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function bg_tambah_info_pasien()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$nik_pasien					= $this->uri->segment(3);

			$bc['data_pekerjaan']		= $this->web_app_model->getAllData('tbl_pekerjaan');
			$bc['data_negara']			= $this->web_app_model->getAllData('tbl_negara');
			$bc['data_faskes']			= $this->web_app_model->getAllData('tbl_faskes');
			$bc['data_kabkota']			= $this->web_app_model->getAllData('tbl_kabkota');
			$bc['data_prov']			= $this->web_app_model->getAllData('tbl_prov');

			$bc['data_pasien']			= $this->web_app_model->getWhereOneItem($nik_pasien,'pasien_nik','tbl_pasien');
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			//$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_tambah_info_pasien',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function updateFoto()
	{
		$pasien_nik					= $this->input->post('pasien_nik');

		// get foto
		 $config['upload_path'] 	= './upload/pasien_foto';
		 $config['allowed_types'] 	= 'jpg|png|jpeg';
		 $config['max_size'] 		= '5000';  //5MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $pasien_nik;

     	 $this->load->library('upload', $config);

			if(!empty($_FILES['pasien_foto']['name'])) 
			{
		        if ( $this->upload->do_upload('pasien_foto') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'pasien_foto' 				=> $foto['file_name'],

						);

					$where = array(		
						'pasien_nik'				=> $pasien_nik,

						);

		
					$this->web_app_model->updateDataWhere($where,$data,'tbl_pasien');
					header('location:'.base_url().'index.php/rsud/bg_tambah_info_pasien/'.$pasien_nik.'?dt_tmbhpasien=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Foto Pasien berhasil diupdate!
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Foto Pasien berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/rsud/bg_tambah_info_pasien/'.$pasien_nik.'?dt_tmbhpasien=1/');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Foto gagal terupdate, pastikan File max 5 Mb atau hubungi IT Satgas Covid-19',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/rsud/bg_tambah_info_pasien/'.$pasien_nik.'?dt_tmbhpasien=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'Photo kosong!',
									                text:  'Mohon lampirkan Photo',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	public function tambah_info_pasien()
	{
		$pasien_nik					= $this->uri->segment(3);
		
		$pasien_nama_ortu			= $this->input->post('pasien_nama_ortu');
		$pasien_pekerjaan			= $this->input->post('pasien_pekerjaan');
		$pasien_usia				= $this->input->post('pasien_usia');
		$pasien_kewarganegaraan		= $this->input->post('pasien_kewarganegaraan');
		$pasien_tgl_lapor			= date('Y-m-d');//$this->input->post('pasien_tgl_lapor');
		$pasien_faskes_asal			= $this->input->post('pasien_faskes_asal');
		$pasien_kabkota_faskes_asal	= $this->input->post('pasien_kabkota_faskes_asal');
		$pasien_prov_faskes_asal	= $this->input->post('pasien_prov_faskes_asal');
		

		// get foto
		 $config['upload_path'] 	= './upload/pasien_berkas';
		 $config['allowed_types'] 	= 'pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $pasien_nik;

     	 $this->load->library('upload', $config);

			
		        $this->upload->do_upload('pasien_lampiran');

		            $foto = $this->upload->data();		

					$data = array(		
						'pasien_lampiran' 			=> $foto['file_name'],
						'pasien_nama_ortu' 			=> $pasien_nama_ortu,
						'pasien_pekerjaan' 			=> $pasien_pekerjaan,
						'pasien_usia' 				=> $pasien_usia,
						'pasien_kewarganegaraan'	=> $pasien_kewarganegaraan,
						'pasien_tgl_lapor' 			=> $pasien_tgl_lapor,
						'pasien_faskes_asal' 		=> $pasien_faskes_asal,
						'pasien_kabkota_faskes_asal'=> $pasien_kabkota_faskes_asal,
						'pasien_prov_faskes_asal' 	=> $pasien_prov_faskes_asal,
						);

					$where = array(		
						'pasien_nik'				=> $pasien_nik,

						);

		
					$this->web_app_model->updateDataWhere($where,$data,'tbl_pasien');
					header('location:'.base_url().'index.php/rsud/bg_pasien_pending?dt_pending=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Informasi Pasien berhasil diupdate!
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Informasi Pasien berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
	}

	public function hapus_pasien()
	{
		
		$pasien_nik	 			= $this->uri->segment(3);
		$pasien_foto		 	= $this->uri->segment(4);
		$pasien_lampiran 		= $this->uri->segment(5);
		$hapus 					= array('pasien_nik'=>$pasien_nik);
		$path 					= './upload/pasien_foto/';
		$path2 					= './upload/pasien_berkas/';
  		@unlink($path.$pasien_foto);
  		@unlink($path2.$pasien_lampiran);
		
		$this->web_app_model->deleteData('tbl_pasien',$hapus);
		header('location:'.base_url().'index.php/rsud/bg_pasien_pending?dt_pending=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Data Pasien Pending berhasil dihapus...!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Data Pasien Pending berhasil dihapus!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	// END - CRUD DATA PASIEN



	// START - DATA PASIEN PENDING
	public function bg_pasien_pending()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['status_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			//$bc['modalUpdateStatus']	= $this->load->view('general/modalUpdateStatus',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_pasien_pending',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_pasien_pending_luar()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			

			$bc['status_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			//$bc['modalUpdateStatus']	= $this->load->view('general/modalUpdateStatus',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_pasien_pending_luar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}
	// END - DATA PASIEN PENDING



	// START - DATA PASIEN TERVERIFIKASI
	public function bg_verified()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$status_pasien 				= $this->uri->segment(3);

			$bc['status_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
			$bc['data_verified']		= $this->web_app_model->get5JoinAll2Where('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1','status_pasien',$status_pasien);
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			$bc['modalUpdateStatus']	= $this->load->view('general/modalUpdateStatus',$bc,true);
			//$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_verified_ajax',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	function data_verified_lokal(){
		$status_pasien 				= $this->uri->segment(3);

        $data=$this->web_app_model->get5JoinAll2Where_ajax('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1','status_pasien',$status_pasien);
        echo json_encode($data);
    }

    public function bg_updateStatus()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['status_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
			$bc['data_pasien']			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'pasien_nik','tbl_pasien');
			//$bc['data_stts_pasien']	= $this->web_app_model->getAllData('tbl_status_pasien');

			//$bc['data_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');



			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);

			$this->load->view('general/bg_updateStatus',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_verified_luar()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$status_pasien 				= $this->uri->segment(3);

			$bc['status_tindakan']		= $this->web_app_model->getAllData('tbl_tindakan_pasien');
			//$bc['data_verified']		= $this->web_app_model->get2JoinAll2Where('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','pasien_verified_dinkes','1','status_pasien',$status_pasien);
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			$bc['modalUpdateStatusPasienLuar']	= $this->load->view('general/modalUpdateStatusPasienLuar',$bc,true);
			//$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_verified_luar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function updateStatus()
	{
		date_default_timezone_set('Asia/Jakarta');

		$pasien_nik 					= $this->input->post('pasien_nik');
		$pasien_stts_tindakan 			= $this->input->post('pasien_stts_tindakan');
		$pasien_stts_tindakan_before 	= $this->input->post('pasien_stts_tindakan_before');
		$status_pasien					= $this->input->post('status_pasien');
		$pasien_tgl_lapor				= date('Y-m-d');

		$cek_update_harian				= $this->web_app_model->get2WhereAllItem($pasien_nik,'update_nik',date('Y-m-d'),'update_date','tbl_update_harian');


		$data = array(		
			'pasien_stts_tindakan' 			=> $pasien_stts_tindakan,
			'pasien_stts_tindakan_before' 	=> $pasien_stts_tindakan_before,
			'pasien_verified_dinkes' 		=> "0",
			'pasien_tgl_lapor' 				=> $pasien_tgl_lapor,
			);

		$where = array(
			'pasien_nik'				=> $pasien_nik,
			);
	
/*
		$data2 = array(		
			'update_nik' 				=> $pasien_nik,
			'update_status_parent' 		=> $status_pasien,
			'update_status_child' 		=> $pasien_stts_tindakan,
			'update_date' 				=> date('Y-m-d'),
			);

		
		$where2 = array(
			'update_nik'				=> $pasien_nik,
			'update_date'				=> date('Y-m-d'),
			);
		


		// INSERT TO TBL UPDATE HARIAN
		if($cek_update_harian->num_rows() >= 1)
		{
			$this->web_app_model->updateDataWhere($where2, $data2,'tbl_update_harian');
		}
		else if($cek_update_harian->num_rows() <= 0)
		{
			$this->web_app_model->insertData($data2,'tbl_update_harian');
		}
		// END INSERT TABEL UPDATE HARIAN

*/
		
		$this->web_app_model->updateDataWhere($where, $data,'tbl_pasien');

		
		header('location:'.base_url().'index.php/rsud/bg_verified/'.$status_pasien.'?dt_pasien=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Status Pasien berhasil diupdate!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Status Pasien berhasil diupdate!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
		
	}

	// END - DATA PASIEN TERVERIFIKASI



	// START - FORM EDIT PASIEN
	public function bg_edit_pasien()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_ruangan']		= $this->web_app_model->getAllData('tbl_ruang_rawat');
			$bc['data_stts_pasien']	= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['data_prov']		= $this->web_app_model->getAllData('tbl_prov');
			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');
			$bc['data_desa']		= $this->web_app_model->getAllData('tbl_desa');
			$bc['data_kota']		= $this->web_app_model->getAllData('tbl_kabkota');

			$bc['data_tindakan']	= $this->web_app_model->getAllData('tbl_tindakan_pasien');

			$bc['data_pasien']		= $this->web_app_model->get5JoinOneWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_nik',$this->uri->segment(3));

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);

			$this->load->view('general/bg_edit_pasien',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function editPasien()
	{
		$pasien_nik 		= $this->input->post('pasien_nik');
		$pasien_no_rm 		= $this->input->post('pasien_no_rm');
		$pasien_nama 		= $this->input->post('pasien_nama');
		$pasien_tlp 		= $this->input->post('pasien_tlp');
		$pasien_tgl_lhr 	= $this->input->post('pasien_tgl_lhr');
		$pasien_kelamin 	= $this->input->post('pasien_kelamin');

		$pasien_alamat 		= $this->input->post('pasien_alamat');
		$pasien_desa 		= $this->input->post('pasien_desa');
		$pasien_kabkota 	= $this->input->post('pasien_kabkota');
		$pasien_kec 		= $this->input->post('pasien_kec');
		$pasien_provinsi 	= $this->input->post('pasien_provinsi');
		$pasien_tgl_masuk 	= $this->input->post('pasien_tgl_masuk');


		$pasien_tgl_keluar 	= $this->input->post('pasien_tgl_keluar');
		$pasien_status 		= $this->input->post('pasien_status');

		$pasien_tgl_pengambilan_swab 	= $this->input->post('pasien_tgl_pengambilan_swab');
		$pasien_tgl_hasil_lab 	= $this->input->post('pasien_tgl_hasil_lab');
		$pasien_ruang_rawat 	= $this->input->post('pasien_ruang_rawat');
		$pasien_ket 			= $this->input->post('pasien_ket');

		$pasien_penginput 		= $this->session->userdata('nama');
		$pasien_stts_tindakan	= $this->input->post('pasien_stts_tindakan');
		//$pasien_verified_dinkes	= '0';
		//$pasien_lokal			= '1';


		$data = array(		
			'pasien_nik' 	=> $pasien_nik,
			'pasien_no_rm' 	=> $pasien_no_rm,
			'pasien_nama' 	=> $pasien_nama,
			'pasien_tlp' 	=> $pasien_tlp,
			'pasien_tgl_lhr'=> $pasien_tgl_lhr,
			'pasien_kelamin'=> $pasien_kelamin,

			'pasien_alamat' 	=> $pasien_alamat,
			'pasien_desa' 		=> $pasien_desa,
			'pasien_kabkota'	=> $pasien_kabkota,
			'pasien_kec' 		=> $pasien_kec,
			'pasien_provinsi' 	=> $pasien_provinsi,
			'pasien_tgl_masuk' 	=> $pasien_tgl_masuk,

			'pasien_tgl_keluar' 			=> $pasien_tgl_keluar,
			'pasien_status' 				=> $pasien_status,
			'pasien_tgl_pengambilan_swab' 	=> $pasien_tgl_pengambilan_swab,
			'pasien_tgl_hasil_lab' 			=> $pasien_tgl_hasil_lab,
			'pasien_ruang_rawat' 			=> $pasien_ruang_rawat,
			'pasien_ket' 					=> $pasien_ket,

			'pasien_penginput' 				=> $pasien_penginput,
			//'pasien_verified_dinkes' 		=> $pasien_verified_dinkes,
			'pasien_stts_tindakan' 			=> $pasien_stts_tindakan,
			//'pasien_lokal' 					=> $pasien_lokal,
			);

		$where = array(
			'pasien_nik' 	=> $pasien_nik,
		);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_pasien');
		header('location:'.base_url().'index.php/rsud/bg_edit_info_pasien/'.$pasien_nik.'?dt_tmbhpasien=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pasien berhasil diedit!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Pasien berhasil diedit!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}	

	public function bg_edit_info_pasien()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$nik_pasien					= $this->uri->segment(3);

			$bc['data_pekerjaan']		= $this->web_app_model->getAllData('tbl_pekerjaan');
			$bc['data_negara']			= $this->web_app_model->getAllData('tbl_negara');
			$bc['data_faskes']			= $this->web_app_model->getAllData('tbl_faskes');
			$bc['data_kabkota']			= $this->web_app_model->getAllData('tbl_kabkota');
			$bc['data_prov']			= $this->web_app_model->getAllData('tbl_prov');

			$bc['data_pasien']			= $this->web_app_model->get5JoinOneWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_nik',$this->uri->segment(3));
			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			//$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_edit_info_pasien',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function bg_edit_pasien_luar()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{

			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$bc['data_ruangan']		= $this->web_app_model->getAllData('tbl_ruang_rawat');
			$bc['data_stts_pasien']	= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['data_prov']		= $this->web_app_model->getAllData('tbl_prov');
			$bc['data_kec']			= $this->web_app_model->getAllData('tbl_kec');
			$bc['data_desa']		= $this->web_app_model->getAllData('tbl_desa');
			$bc['data_kota']		= $this->web_app_model->getAllData('tbl_kabkota');

			$bc['data_tindakan']	= $this->web_app_model->getAllData('tbl_tindakan_pasien');

			$bc['data_pasien']		= $this->web_app_model->getJoinOneWhere('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_nik',$this->uri->segment(3));

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['kontroller'] 		= $this->session->userdata('kontroller');
			
			$bc['atas'] 			= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('rsud/bio',$bc,true);

			$this->load->view('general/bg_edit_pasien_luar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	public function editPasienLuar()
	{
		$pasien_nik 		= $this->input->post('pasien_nik');
		$pasien_no_rm 		= $this->input->post('pasien_no_rm');
		$pasien_nama 		= $this->input->post('pasien_nama');
		$pasien_tlp 		= $this->input->post('pasien_tlp');
		$pasien_tgl_lhr 	= $this->input->post('pasien_tgl_lhr');
		$pasien_kelamin 	= $this->input->post('pasien_kelamin');

		$pasien_alamat 		= $this->input->post('pasien_alamat');
		$pasien_desa 		= $this->input->post('pasien_desa');
		$pasien_kabkota 	= $this->input->post('pasien_kabkota');
		$pasien_kec 		= $this->input->post('pasien_kec');
		$pasien_provinsi 	= $this->input->post('pasien_provinsi');
		$pasien_tgl_masuk 	= $this->input->post('pasien_tgl_masuk');


		$pasien_tgl_keluar 	= $this->input->post('pasien_tgl_keluar');
		$pasien_status 		= $this->input->post('pasien_status');

		$pasien_tgl_pengambilan_swab 	= $this->input->post('pasien_tgl_pengambilan_swab');
		$pasien_tgl_hasil_lab 	= $this->input->post('pasien_tgl_hasil_lab');
		$pasien_ruang_rawat 	= $this->input->post('pasien_ruang_rawat');
		$pasien_ket 			= $this->input->post('pasien_ket');

		$pasien_penginput 		= $this->session->userdata('nama');
		$pasien_stts_tindakan	= $this->input->post('pasien_stts_tindakan');
		//$pasien_verified_dinkes	= '0';
		//$pasien_lokal			= '1';


		$data = array(		
			'pasien_nik' 	=> $pasien_nik,
			'pasien_no_rm' 	=> $pasien_no_rm,
			'pasien_nama' 	=> $pasien_nama,
			'pasien_tlp' 	=> $pasien_tlp,
			'pasien_tgl_lhr'=> $pasien_tgl_lhr,
			'pasien_kelamin'=> $pasien_kelamin,

			'pasien_alamat' 	=> $pasien_alamat,
			'pasien_desa' 		=> $pasien_desa,
			'pasien_kabkota'	=> $pasien_kabkota,
			'pasien_kec' 		=> $pasien_kec,
			'pasien_provinsi' 	=> $pasien_provinsi,
			'pasien_tgl_masuk' 	=> $pasien_tgl_masuk,

			'pasien_tgl_keluar' 			=> $pasien_tgl_keluar,
			'pasien_status' 				=> $pasien_status,
			'pasien_tgl_pengambilan_swab' 	=> $pasien_tgl_pengambilan_swab,
			'pasien_tgl_hasil_lab' 			=> $pasien_tgl_hasil_lab,
			'pasien_ruang_rawat' 			=> $pasien_ruang_rawat,
			'pasien_ket' 					=> $pasien_ket,

			'pasien_penginput' 				=> $pasien_penginput,
			//'pasien_verified_dinkes' 		=> $pasien_verified_dinkes,
			'pasien_stts_tindakan' 			=> $pasien_stts_tindakan,
			//'pasien_lokal' 					=> $pasien_lokal,
			);

		$where = array(
			'pasien_nik' 	=> $pasien_nik,
		);

		$this->web_app_model->updateDataWhere($where, $data,'tbl_pasien');
		header('location:'.base_url().'index.php/rsud/bg_edit_info_pasien_luar/'.$pasien_nik.'?dt_pending=1/');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data Pasien Luar berhasil diedit!
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data Pasien Luar berhasil diedit!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function bg_edit_info_pasien_luar()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Pengguna')
		{
			// NOTIF - START
			$bc['data_pasien_pending']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','0');
			$bc['data_pasien_verified']	= $this->web_app_model->get5JoinAllWhere('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','kec_no','desa_kec','kabkota_no','kec_kabkota','prov_no','kabkota_prov','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','tbl_kec','tbl_desa','tbl_kabkota','tbl_kec','tbl_prov','tbl_kabkota','pasien_verified_dinkes','1');
			$bc['data_status']			= $this->web_app_model->getAllData('tbl_status_pasien');
			$bc['pasien_pending_luar']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','0');
			$bc['pasien_pending_luar_verified']	= $this->web_app_model->getJoinAll2Where('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_lokal','0','pasien_verified_dinkes','1');

			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','config_no','tbl_config');
			// NOTIF - END

			$nik_pasien					= $this->uri->segment(3);

			$bc['data_pekerjaan']		= $this->web_app_model->getAllData('tbl_pekerjaan');
			$bc['data_negara']			= $this->web_app_model->getAllData('tbl_negara');
			$bc['data_faskes']			= $this->web_app_model->getAllData('tbl_faskes');
			$bc['data_kabkota']			= $this->web_app_model->getAllData('tbl_kabkota');
			$bc['data_prov']			= $this->web_app_model->getAllData('tbl_prov');

			$bc['data_pasien']			= $this->web_app_model->getJoinOneWhere('pasien_stts_tindakan','tindakan_no','tbl_pasien','tbl_tindakan_pasien','pasien_nik',$this->uri->segment(3));

			$bc['username'] 			= $this->session->userdata('username');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['kontroller'] 			= $this->session->userdata('kontroller');
			
			$bc['atas'] 				= $this->load->view('rsud/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('rsud/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('rsud/bio',$bc,true);
			//$bc['modalTambahKategHubKontak']	= $this->load->view('general/modalTambahKategHubKontak',$bc,true);
			//$bc['modalEditKategHubKontak'] 		= $this->load->view('general/modalEditKategHubKontak',$bc,true);	

			$this->load->view('general/bg_edit_info_pasien_luar',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}

	// END - MENU FORM EDIT PASIEN
}
