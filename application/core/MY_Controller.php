<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public function get_id()
	{
		/* FOR SQL SERVER ========
		$result = $this->db->query("DECLARE @myid uniqueidentifier = NEWID();  
SELECT CONVERT(char(40), @myid) AS 'Id';");
		return ($result->num_rows() == 1) ? $result->result()[0]->Id : FALSE;
		*/

		// FOR MYSQL
		$result = $this->db->query("Select UPPER(REPLACE(UUID(), '-', '')) as Id");
		return ($result->num_rows() == 1) ? $result->result()[0]->Id : FALSE;
	}

	//  ID GRUP TELEGRAM AKUN ROBOT
	public function get_id_group_telegram()
	{
		 return "-412702261"; // GROUP ID BOT TELEGRAM 
	}

	// TOKEN TELEGRAM BOT
	public function get_token_bot_telegram()
	{
		 return "1485300398:AAFKGFg-LzaROk0Im6hO1sXfbazdw7RMM9U";
		 //UNSAM
	}

	public function data_master()
	{
		 return 
		 $bc['data_pasien_pending']	= $this->web_app_model->getWhereAllItem('0','pasien_verified_dinkes','tbl_pasien');
		 
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */