<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login Sipecrok Langsa</title>
	<link rel="shortcut icon" href="<?php echo base_url() ?>vendor/assets/images/web/unsam.ico" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url() ?><?php echo base_url() ?>vendor/login/vendor/login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendor/login/css/main.css">
<!--===============================================================================================-->

<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
  
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
				<form class="validate-form" method="post" action="<?php echo base_url();?>index.php/web/login">
					<br>
					<span class="login100-form-title p-b-34">
						<img src="<?php echo base_url() ?>vendor/login/images/logo-satgas.png" width="150px" height="150px">
					</span>
					<span class="login100-form-title p-b-34">
						<b>LOGIN SIPECROK LANGSA</b><br><br>
						SISTEM INFORMASI PANDEMI CORONA VIRUS 2019 KOTA LANGSA
						<p><br>Kota Langsa, Provinsi Aceh, Indonesia</p>
					</span>
					<?php echo $this->session->flashdata('info'); ?>
					<div class="alert alert-info">
                        <button data-dismiss="alert" class="close">
                            &times;
                        </button>
                        Akses anda akan diblokir jika upaya login gagal lebih dari 3 kali.
                    </div>
                    <div class="alert alert-warning">
                        <button data-dismiss="alert" class="close">
                            &times;
                        </button>
                        Pastikan menginput otentikasi dengan benar, saat ini anda sudah<br>
                        <?php 
						//ini script untuk menampilkan brapa kali user mencoba login
						if(isset($_SESSION['auth'])){
						    echo $_SESSION['auth'];
						}
					?> kali gagal login, silahkan hubungi pihak terkait untuk penanganan lebih lanjut
                    </div>	

					
					<div class="wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="first-name" class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>			
						
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Sign in
						</button>
					</div>
					<br>
					
						<br>
					<p align="center">
						Developed by <b>TIM IT Satgas Covid-19 Kota Langsa<br>Powered by Diskominfo Langsa<?php //	$this->load->view('versi'); ?>
					</p>
					<!--<div class="w-full text-center p-t-27 p-b-239">
						<span class="txt1">
							Forgot
						</span>

						<a href="#" class="txt2">
							User name / password?
						</a>
					</div> -->

					<!--<div class="w-full text-center">
						<a href="#" class="txt3">
							Sign Up
						</a>
					</div>-->
				</form>
				<br><br><br><br>
				<!--<h6>TABEL UJICOBA PASIEN ISOLASI MANDIRI DALAM MASA INKUBASI: (ON DEVELOPMENT)</h6><br>
				<?php
					

					foreach($data_pasien_isman->result_array() as $e)
					{
						$tgl1 = new DateTime($e['pasien_tgl_lapor']);
						$tgl2 = new DateTime(date('Y-m-d'));
						$d = $tgl2->diff($tgl1)->days + 1;
						//echo $d." hari <br>";
	

						if(($e['pasien_stts_tindakan'] == 2 || $e['pasien_stts_tindakan'] == 15) && $d >= $data_config['config_durasi_isman'])
						{
							$data = array(		
								'pasien_stts_tindakan' 			=> 23,
							);

							$where = array(		
								'pasien_nik' 			=> $e['pasien_nik'],
							);

							$this->web_app_model->updateDataWhere($where,$data,'tbl_pasien');
						}
						else if(($e['pasien_stts_tindakan'] == 26 || $e['pasien_stts_tindakan'] == 28) && $d >= $data_config['config_durasi_isman'])
						{
							$data = array(		
								'pasien_stts_tindakan' 			=> 29,
							);

							$where = array(		
								'pasien_nik' 			=> $e['pasien_nik'],
							);

							$this->web_app_model->updateDataWhere($where,$data,'tbl_pasien');
						}
						else if(($e['pasien_stts_tindakan'] == 31 || $e['pasien_stts_tindakan'] == 33) && $d >= $data_config['config_durasi_isman'])
						{
							$data = array(		
								'pasien_stts_tindakan' 			=> 99,
							);

							$where = array(		
								'pasien_nik' 			=> $e['pasien_nik'],
							);

							$this->web_app_model->updateDataWhere($where,$data,'tbl_pasien');
						}

						echo 'Tanggal Lapor: '.$e['pasien_tgl_lapor'].' | Nama: ******** | Status: '.$e['tindakan_nama'].' = '.$d.' Hari yang lalu<br>';
					}
				?>-->


				
		</div> 
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>vendor/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>vendor/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>vendor/login/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url() ?>vendor/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>vendor/login/vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>vendor/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url() ?>vendor/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>vendor/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

	<?php echo $this->session->flashdata('info2'); ?>

</body>
</html>