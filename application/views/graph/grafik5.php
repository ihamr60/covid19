<script type="text/javascript">
	Highcharts.chart('grafik5', {
	  chart: {
	    type: 'column'
	  },
	  title: {
	    text: 'BAR CHART KASUS AKTIF PER GAMPONG DI KOTA LANGSA'
	  },
	  subtitle: {
	    text: 'Source: Data Harian Terkini (RealTime)'
	  },
	  xAxis: {
	    type: 'category',
	    labels: {
	      rotation: -45,
	      style: {
	        fontSize: '13px',
	        fontFamily: 'Verdana, sans-serif'
	      }
	    }
	  },
	  yAxis: {
	    min: 0,
	    title: {
	      text: 'Jumlah Kasus Aktif'
	    }
	  },
	  legend: {
	    enabled: false
	  },
	  tooltip: {
	    pointFormat: 'Jumlah Kasus: <b>{point.y} Orang</b>'
	  },
	  series: [{
	    name: 'Population',
	    data: [
	    <?php 
	    	foreach($zonasi_desa->result_array() as $d)
	    	{
	    ?>
	      ['<?php echo $d['desa_nama'] ?>', <?php echo $d['total'] ?>],
	    <?php } ?>
	    ],
	    dataLabels: {
	      enabled: true,
	      rotation: -90,
	      color: '#FFFFFF',
	      align: 'right',
	      format: '{point.y} Org', // one decimal
	      y: 10, // 10 pixels down from the top
	      style: {
	        fontSize: '13px',
	        fontFamily: 'Verdana, sans-serif'
	      }
	    }
	  }]
	});
</script>