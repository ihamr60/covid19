<script type="text/javascript">
        var clrs = Highcharts.getOptions().colors;
          var pieColors = [clrs[2], clrs[0], clrs[3], clrs[1], clrs[4]];

          // Get a default pattern, but using the pieColors above.
          // The i-argument refers to which default pattern to use
          function getPattern(i) {
            return {
              pattern: Highcharts.merge(Highcharts.patterns[i], {
                color: pieColors[i]
              })
            };
          }

          // Get 5 patterns
          var patterns = [0, 1, 2, 3, 4].map(getPattern);

          var chart = Highcharts.chart('grafik2', {
            chart: {
              type: 'pie'
            },

            title: {
              text: 'Visualisasi Kasus Suspect di Kota Langsa'
            },

            subtitle: {
              text: 'Source: Data Real Time'
            },

            colors: patterns,

            tooltip: {
              valueSuffix: '%',
              borderColor: '#8ae'
            },

            plotOptions: {
              series: {
                dataLabels: {
                  enabled: true,
                  connectorColor: '#777',
                  format: '<b>{point.name}</b>: {point.y} Orang'
                },
                point: {
                  events: {
                    click: function () {
                      window.location.href = this.website;
                    }
                  }
                },
                cursor: 'pointer',
                borderWidth: 3
              }
            },

            series: [{
              name: 'Screen reader usage',
              data: [{
                name: 'SUSPECT (DIPANTAU)',
                y: <?php echo $tot_suspect_dipantau['total']; ?>,
                website: 'https://covid19.langsakota.go.id',
              }, {
                name: 'SUSPECT (DIRAWAT)',
                y: <?php echo $tot_suspect_dirawat['total']; ?>,
                website: 'https://covid19.langsakota.go.id',
              }, {
                name: 'SUSPECT (MENINGGAL)',
                y: <?php echo $tot_suspect_meninggal['total']; ?>,
                website: 'https://covid19.langsakota.go.id',
              }, {
                name: 'SUSPECT (ISOLASI MANDIRI)',
                y: <?php echo $tot_suspect_isman['total']; ?>,
                website: 'https://covid19.langsakota.go.id',
              }, {
                name: 'SUSPECT (SEMBUH)',
                y: <?php echo $tot_suspect_sembuh['total']; ?>,
                website: 'https://covid19.langsakota.go.id',
              }]
            }],

            responsive: {
              rules: [{
                condition: {
                  maxWidth: 500
                },
                chartOptions: {
                  plotOptions: {
                    series: {
                      dataLabels: {
                        format: '<b>{point.name}</b>'
                      }
                    }
                  }
                }
              }]
            }
          });

          // Toggle patterns enabled
          document.getElementById('patterns-enabled').onclick = function () {
            chart.update({
              colors: this.checked ? patterns : pieColors
            });
          };
    </script>