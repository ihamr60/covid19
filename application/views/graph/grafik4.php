<script type="text/javascript">
  Highcharts.chart('grafik4', {

    chart: {
      type: 'item'
    },

    title: {
      text: 'DATA VISUAL PASIEN MENINGGAL DI KOTA LANGSA'
    },

    subtitle: {
      text: 'Data Harian'
    },

    legend: {
      labelFormat: '{name} <span style="opacity: 0.4">{y} Orang</span>'
    },

    series: [{
      name: 'Total',
      keys: ['name', 'y', 'color', 'label'],
      data: [
        ['Meninggal (Positif)', <?php echo $pos_pasienLokal_meninggal['total']; ?>, '#BE3075', 'POSITIF'],
        ['Meninggal (Probable)', <?php echo $pro_pasienLokal_meninggal['total']; ?>, '#EB001F', 'PROBABLE'],
        ['Meninggal (Suspect)', <?php echo $sus_pasienLokal_meninggal['total']; ?>, '#64A12D', 'SUSPECT']
      ],
      dataLabels: {
        enabled: true,
        format: '{point.label}'
      },

      // Circular options
      center: ['50%', '88%'],
      size: '170%',
      startAngle: -100,
      endAngle: 100
    }]
  });
</script>