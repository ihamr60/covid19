<script type="text/javascript">
        Highcharts.chart('vaksin1', {

          title: {
            text: 'Jumlah Pasien Vaksinasi Harian per Klasifikasi Usia'
          },

          subtitle: {
            text: 'Source: Operator Dinkes Langsa'
          },

          yAxis: {
            title: {
              text: 'Total Pasien Melakukan Vaksin'
            }
          },

          xAxis: {
            accessibility: {
              rangeDescription: 'Vaksinasi'
            }
          },

          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
          },

          plotOptions: {
            series: {
              label: {
                connectorAllowed: false
              },
              pointStart: 1
            }
          },

          series: [{
            name: 'Usia Tidak Diketahui',
            data: [
            <?php
              foreach($vaksin_null->result_array () as $d)
              {
                echo "['".$d['tanggal']."',".$d['total']."],";
              }
            ?>
        ]
          },{
            name: 'Usia 18-30 Tahun',
            data: [
            <?php
              foreach($vaksin_18_30->result_array () as $d)
              {
                echo "['".$d['tanggal']."',".$d['total']."],";
              }
            ?>
        ]
          },
          {
            name: 'Usia 31-45 Tahun',
            data: [
            <?php
              foreach($vaksin_31_45->result_array () as $d)
              {
                echo "['".$d['tanggal']."',".$d['total']."],";
              }
            ?>
        ]
          },
          {
            name: 'Usia 46-59 Tahun',
            data: [
            <?php
              foreach($vaksin_46_59->result_array () as $d)
              {
                echo "['".$d['tanggal']."',".$d['total']."],";
              }
            ?>
        ]
          },
          {
            name: 'Usia < 18 Tahun',
            data: [
            <?php
              foreach($vaksin_18_kurang->result_array () as $d)
              {
                echo "['".$d['tanggal']."',".$d['total']."],";
              }
            ?>
        ]
          },
          {
            name: 'Usia > 60 Tahun',
            data: [
            <?php
              foreach($vaksin_60_lebih->result_array () as $d)
              {
                echo "['".$d['tanggal']."',".$d['total']."],";
              }
            ?>
        ]
          }],

          responsive: {
            rules: [{
              condition: {
                maxWidth: 500
              },
              chartOptions: {
                legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
                }
              }
            }]
          }

        });
    </script>