<script type="text/javascript">
	// Radialize the colors
	Highcharts.setOptions({
	  colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
	    return {
	      radialGradient: {
	        cx: 0.5,
	        cy: 0.3,
	        r: 0.7
	      },
	      stops: [
	        [0, color],
	        [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
	      ]
	    };
	  })
	});

	// Build the chart
	Highcharts.chart('grafik3', {
	  chart: {
	    plotBackgroundColor: null,
	    plotBorderWidth: null,
	    plotShadow: false,
	    type: 'pie'
	  },
	  title: {
	    text: 'Kasus Aktif Terkonfirmasi Positif (Pasien Lokal VS Pasien Luar)'
	  },
	  tooltip: {
	    pointFormat: '{series.name}: <b>{point.y} Orang</b>'
	  },
	  accessibility: {
	    point: {
	      valueSuffix: '%'
	    }
	  },
	  plotOptions: {
	    pie: {
	      allowPointSelect: true,
	      cursor: 'pointer',
	      dataLabels: {
	        enabled: true,
	        format: '<b>{point.name}</b>: {point.y} Pasien',
	        connectorColor: 'silver'
	      }
	    }
	  },
	  series: [{
	    name: 'Total Pasien',
	    data: [
	      { name: 'PASIEN LOKAL (DIRAWAT)', color:'#F5DEB3', y: <?php echo $pos_pasienLokal_dirawat['total']; ?> },
	      { name: 'PASIEN LOKAL (ISOLASI MANDIRI)', color:'#D2B48C', y: <?php echo $pos_pasienLokal_isman['total']; ?> },
	      { name: 'PASIEN LOKAL (DIPANTAU)', color:'#F4A460', y: <?php echo $pos_pasienLokal_dipantau['total']; ?> },
	      { name: 'PASIEN LUAR KOTA (DIRAWAT', color:'#8B0000', y: <?php echo $pos_pasienLuar_dirawat['total']; ?> },
	      { name: 'PASIEN LUAR KOTA (ISOLASI MANDIRI)', color:'#B22222', y: <?php echo $pos_pasienLuar_isman['total']; ?> },
	      { name: 'PASIEN LUAR KOTA (DIPANTAU)', color:'#CD5C5C', y: <?php echo $pos_pasienLuar_dipantau['total']; ?> }
	    ]
	  }]
	});
</script>