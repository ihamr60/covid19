<script type="text/javascript">
        Highcharts.chart('grafik1', {
          chart: {
            type: 'variablepie'
          },
          title: {
            text: 'Visualisasi Kasus Terkonfirmasi Positif Covid-19 di Kota Langsa'
          },
          tooltip: {
            headerFormat: '',
            pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
              'Total Pasien {point.name}: <b>{point.y} Orang</b><br/>'/* +
              'Population density (people per square km): <b>{point.z}</b><br/>' */
          },
          series: [{
            minPointSize: 10,
            innerSize: '20%',
            zMin: 0,
            name: 'countries',
            data: [{
              name: 'POSITIF (MENINGGAL) = <?php echo $tot_positif_meninggal['positif_meninggal']; ?> Orang',
              color:'#696969',
              y: <?php echo $tot_positif_meninggal['positif_meninggal']; ?>,
              z: 92.9
            }, {
              name: 'POSITIF (DIRAWAT) = <?php echo $tot_positif_dirawat['positif_dirawat']; ?> Orang',
              color:'#8B0000',
              y: <?php echo $tot_positif_dirawat['positif_dirawat']; ?>,
              z: 118.7
            },{
              name: 'POSITIF (DIPANTAU) = <?php echo $tot_positif_dipantau['positif_dipantau']; ?> Orang',
              color:'#FA8072',
              y: <?php echo $tot_positif_dipantau['positif_dipantau']; ?>,
              z: 118.7
            }, {
              name: 'POSITIF (ISOLASI MANDIRI) = <?php echo $tot_positif_isman['positif_isman']; ?> Orang',
              color:'#CD5C5C',
              y: <?php echo $tot_positif_isman['positif_isman']; ?>,
              z: 218.7
            }, {
              name: 'POSITIF (SEMBUH) = <?php echo $tot_positif_sembuh['positif_sembuh']; ?> Orang',
              color:'#8FBC8F',
              y: <?php echo $tot_positif_sembuh['positif_sembuh']; ?>,
              z: 358.7
            }]
          }]
        });
    </script>