
<div id="modalTambahSurveilans" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambahSurveilans" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM TAMBAH DATA AKUN SURVEILANS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>USERNAME:</label>
                    <p>
                        <input
                            type="text"
                            name="surveyor_username"
                            class="form-control"
                            placeholder="Ex: ilham234"
                            required>
                        <font color="red" size="0">Note: Max 16 Char. Gunakan username sebagai password default saat pertama kali login.</font>
                    </p>

                </div>
                <div class="col-md-12">
                    <label>NAMA USER:</label>
                    <p>
                        <input
                            type="text"
                            name="surveyor_nama"
                            class="form-control"
                            placeholder="Ex: Ilham Ramadhan"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>KECAMATAN:</label>
                    <p>
                         <select
                            name="surveyor_kd_kec"
                            class="form-control"
                            required>
                            <option value="">Pilih Kecamatan</option>
                             <?php 
                                foreach($data_kec->result_array() as $e)
                                {
                                    
                                    echo "<option value='".$e['kec_no']."'>".$e['kec_nama']."</option>";
                                    
                                }
                             ?>
                         </select>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>NAUNGAN PKM:</label>
                    <p>
                         <select
                            name="surveyor_kd_pkm"
                            class="form-control"
                            required>
                            <option value="">Pilih PKM</option>
                             <?php 
                                foreach($data_naungan->result_array() as $e)
                                {
                                    
                                    echo "<option value='".$e['rsud_no']."'>".$e['rsud_nama']."</option>";
                                    
                                }
                             ?>
                         </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
