<?php
    foreach($data_tracert->result_array() as $d)
    {
?>

<div id="modalEditTracert<?php echo $d['tracert_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editTracert" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT DATA AKUN TRACERT</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>USERNAME:</label>
                    <p>
                        <input name="tracert_no" type="hidden" value="<?php echo $d['tracert_no'] ?>">
                        <input
                            type="text"
                            name="tracert_username"
                            class="form-control"
                            value="<?php echo $d['tracert_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>NAMA TRACERT:</label>
                    <p>
                        <input
                            type="text"
                            name="tracert_nama"
                            class="form-control"
                            value="<?php echo $d['tracert_nama'] ?>"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
