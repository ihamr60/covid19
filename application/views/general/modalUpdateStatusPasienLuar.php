<?php
    foreach($pasien_pending_luar_verified->result_array() as $d)
    {
?>
<div id="modalUpdateStatus<?php echo $d['pasien_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/updateStatus" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">UPDATE PERKEMBANGAN STATUS PASIEN</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>PERKEMBANGAN STATUS PASIEN:</label>
                    <p>
                        <input type="hidden" name="pasien_nik" value="<?php echo $d['pasien_nik'] ?>">
                        <input type="hidden" name="status_pasien" value="<?php echo $this->uri->segment(3) ?>">
                        <input type="hidden" name="pasien_stts_tindakan_before" value="<?php echo $d['pasien_stts_tindakan'] ?>">
                        <select
                            name="pasien_stts_tindakan"
                            class="form-control"
                            required>
                            <option value="<?php echo $d['tindakan_no'] ?>"><?php echo $d['tindakan_nama'] ?></option>
                            <?php
                            foreach($status_tindakan->result_array() as $e)
                                if($e['tindakan_no'] != $d['tindakan_no'])
                                {   
                                    echo "<option value='".$e['tindakan_no']."'>".$e['tindakan_nama']."</option> ";
                                }
                                
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
<?php } ?>
