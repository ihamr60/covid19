<div class="col-sm-5 col-md-4">
    <div class="user-left">
        <div class="center">
            <h4>Photo Profile</h4>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="user-image">
                    <div id="kv-avatar-errors" class="center-block" style="display:none"></div>
                    <div class="kv-avatar ">
                        <img src="<?php 
                          if(empty($data_pasien['pasien_foto']) && $data_pasien['pasien_kelamin'] == 'Laki-laki')
                          {
                            echo base_url().'vendor/assets/images/img_avatar3.png';
                          }
                          else if(empty($data_pasien['pasien_foto']) && $data_pasien['pasien_kelamin'] == 'Perempuan')
                          {
                            echo base_url().'vendor/assets/images/img_avatar2.png';
                          }
                          else if(!empty($data_pasien))
                          {
                            echo base_url().'upload/pasien_foto/'.$data_pasien['pasien_foto'].'';
                          }
                          
                          ?>" style=" border-radius: 10px; width:160px; height:200px;">
                    </div>
                </div>
            </div>
            <h4>- <u><b><?php echo $data_pasien['pasien_nama'] ?></b></u> -</h4>
            <h5>NO.RM. <?php echo $data_pasien['pasien_no_rm'] ?> <br> KTP. <?php echo $data_pasien['pasien_nik'] ?></h5>
             <table >
                <tbody>
                    <tr>
                        <form role="form" action="<?php echo base_url();?>index.php/crud_pegawai/updateFoto" enctype="multipart/form-data" method="post">
                            <td>
                                <input required class="form-control" type="file" name="foto">
                                <input type="hidden" value="<?php //echo $detail_pegawai['nik_ktp'] ?>" name="nik_ktp">
                                <input type="hidden" value="<?php //echo $detail_pegawai['nama_pegawai'] ?>" name="nama_pegawai">
                            </td>
                            <td>
                                <button href="#" class="btn btn-sm btn-teal">Update Foto</button>
                            </td>
                        </form>
                    </tr>
                </tbody>
            </table>
            <hr>
            <p>
                <a class="btn btn-social-icon btn-twitter">
                    <i class="fa fa-twitter"></i>
                </a>
                <a class="btn btn-social-icon btn-linkedin">
                    <i class="fa fa-linkedin"></i>
                </a>
                <a class="btn btn-social-icon btn-google">
                    <i class="fa fa-google-plus"></i>
                </a>
                <a class="btn btn-social-icon btn-github">
                    <i class="fa fa-github"></i>
                </a>
            </p>
            <hr>
        </div>
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="3">
                        <img src="<?php echo base_url() ?>vendor/assets/images/web/police.png" height=20px width=px> |
                        GENERAL INFORMATION
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Nama</td>
                    <td>
                        <a href="#">
                            <b><?php echo $data_pasien['pasien_nama'] ?></b>
                        </a>
                    </td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>No RM</td>
                    <td>
                        <a href="#">
                            <b><?php echo $data_pasien['pasien_no_rm'] ?></b>
                        </a>
                    </td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>No. KTP</td>

                    <td><b><?php echo $data_pasien['pasien_nik'] ?></b></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Pekerjaan</td>
                    <td>
                        <a href="">
                            <span class="label label-purple">
                                <?php echo $data_pasien['pasien_pekerjaan'] ?>
                            </span>
                        </a>
                    </td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Kewarganegaraan</td>
                    <td>
                        <a href="#">
                            <span class="label label-info">
                                <font color="black"><?php echo $data_pasien['pasien_kewarganegaraan'] ?></font>
                            </span>
                        </a>
                    </td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Tanggal Melapor</td>
                    <td>
                            <?php echo date('d-m-Y', strtotime($data_pasien['pasien_tgl_lapor'])) ?>
                    </td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="3">
                        <img src="<?php echo base_url() ?>vendor/assets/images/web/info.png" height=15px width=px> |
                        CONTACT INFORMATION
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b>Contact Person / HP</b></td>
                    <td><b><?php echo $data_pasien['pasien_tlp']; ?></b></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td><?php echo date('d-m-Y', strtotime($data_pasien['pasien_tgl_lhr'])) ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td><?php echo $data_pasien['pasien_kelamin'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Nama Wali</td>
                    <td><?php echo $data_pasien['pasien_nama_ortu'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
        </table>
        <!--<table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="3">
                        <img src="<?php echo base_url() ?>vendor/assets/images/web/graduation.png" height=20px width=px> |
                        EDUCATION INFORMATION
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Pendidikan</td>
                    <td><b><?php //echo $detail_pegawai['pendidikan'] ?></b></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>PTN / PTS / Sekolah</td>
                    <td><b><?php //echo $detail_pegawai['ptn'] ?></b></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
            </tbody>
        </table>-->
        <!--<table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="10">
                        <img src="<?php echo base_url() ?>vendor/assets/images/web/physics.png" height=20px width=px> |
                        PHYSICAL INFORMATION
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Tinggi Badan</td>
                    <td><?php //echo $detail_pegawai['tinggi_badan'] ?> CM</td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Berat Badan</td>
                    <td><?php //echo $detail_pegawai['berat_badan'] ?> KG</td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Rambut</td>
                    <td><?php //echo $detail_pegawai['rambut'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Bentuk Wajah</td>
                    <td><?php //echo $detail_pegawai['bentuk_wajah'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Warna Kulit</td>
                    <td><?php //echo $detail_pegawai['warna_kulit'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Ciri Khas</td>
                    <td><?php //echo $detail_pegawai['ciri_khas'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Cacat</td>
                    <td><?php //echo $detail_pegawai['cacat'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
            </tbody>
        </table>-->
        <!--<table class="table table-condensed table-hover">
            <thead>
                <tr>
                    <th colspan="3">
                        <img src="<?php echo base_url() ?>vendor/assets/images/web/address.png" height=20px width=px> |
                        ADDRESS INFORMATION
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Alamat</td>
                    <td><b><?php echo $data_pasien['pasien_alamat'] ?></b></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Kelurahan</td>
                    <td><b><?php echo $data_pasien['pasien_desa'] ?></b></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Kecamatan</td>
                    <td><?php echo $data_pasien['pasien_kec'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td><?php echo $data_pasien['pasien_kabkota'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
                <tr>
                    <td>Provinsi</td>
                    <td><?php echo $data_pasien['pasien_provinsi'] ?></td>
                    <td><a href="<?php echo base_url() ?>index.php/admin/bg_editPegawai/<?php //echo $detail_pegawai['nik_ktp'] ?>"><i class="fa fa-pencil edit-user-info"></i></a></td>
                </tr>
            </tbody>
        </table> -->
        <hr>
        <h4>
            <b>
                <img src="<?php echo base_url() ?>vendor/assets/images/web/reward.png" height=25px width=25px> | REWARD 
            </b>
            <a data-toggle="modal" data-target="#modalTambahRiwPengPeg" class="btn btn-xs btn-teal">
                <b><i class="fa fa-plus"></i> ADD REWARD </b>
            </a>
        </h4>
        <div class="tab-pane">
            <table class="table table-striped table-bordered table-hover" id="projects">
                <thead>
                    <tr>
                        <th class="center">No</th>
                        <th>Nama Penghargaan</th>
                        <th class="center">Tahun Perolehan</th>
                        <th class="center">Instansi Pemberi</th>
                        <th width="70px" class="center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                      /*  $no=1;
                        foreach($data_riwpeng_peg->result_array() as $d)
                        { */
                    ?>
                    <tr>
                        <td class="center"><?php //echo $no ?></td>
                        <td><?php //echo $d['riwpeng_penghargaan'] ?></td>
                        <td class="center"><?php //echo $d['riwpeng_tahun'] ?></td>
                        <td class="center"><?php //echo $d['riwpeng_instansi'] ?></td>
                        <td class="center">
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <a data-toggle="modal" data-target="#modalEditRiwPengPeg<?php //echo $d['riwpeng_no'] ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url();?>index.php/crud_pegawai/hapusRiwPengPegawai/<?php //echo $d['riwpeng_no'] ?>/<?php //echo $d['riwpeng_nik_ktp'] ?>" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data Riwayat Penghargaan ini?')"><i class="fa fa-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php //$no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>