<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['config_nama_header'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/logo/<?php echo $data_config['config_logo'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <?php $this->load->view('general/link_css') ?>
    
    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';
    </style>

</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
            <?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="clip-home-3"></i>
                                <a href="">
                                    Dashboard
                                </a>
                            </li>
                            <li class="active">
                                    Form Input Data <font color="red">*</font>Pasien Luar Kota
                            </li>
                           
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php //echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <!--<marquee>SELAMAT DATANG DI SISTEM INFORMASI COVID-19 KOTA LANGSA</marquee>-->
                <?php echo $this->session->flashdata('info'); ?>
                <div class="row">
                    
                    <!-- BATAS -->
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><b style="text-transform: uppercase;">FORM INPUTAN DATA PASIEN COVID-19 TERPADU <span class="badge badge-warning animate__animated animate__infinite animate__headShake"><font color="">( KHUSUS PASIEN LUAR KOTA LANGSA )</font></span></b>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <form role="form" action="<?php echo base_url();?>index.php/<?php echo $kontroller ?>/editPasienLuar" enctype="multipart/form-data" method="post">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title"><b>FORM TAMBAH DATA PASIEN COVID-19 TERPADU ( <font color="red">*</font>KHUSUS PASIEN DARI LUAR KOTA LANGSA<font color="red">*</font> )</b></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5><b>A. DATA PASIEN</b></h5> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>NIK KTP: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="number"
                                                    name="pasien_nik"
                                                    class="form-control"
                                                    readonly
                                                    value="<?php echo $data_pasien['pasien_nik'] ?>"
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <label>No Rekam Medis: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_no_rm"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_no_rm'] ?>"
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Nama Lengkap Pasien: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_nama"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_nama'] ?>"
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <label>No HP / Tlp: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="number"
                                                    name="pasien_tlp"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_tlp'] ?>"
                                                    required>
                                            </p>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Tanggal Lahir: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="date"
                                                    name="pasien_tgl_lhr"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_tgl_lhr'] ?>"
                                                    required>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Jenis Kelamin: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_kelamin" 
                                                    class="form-control"
                                                    required>
                                                    <?php 
                                                        if($data_pasien['pasien_kelamin'] == "Laki-laki")
                                                        {
                                                            echo '<option selected value="Laki-laki">Laki-laki</option>
                                                                  <option value="Perempuan">Perempuan</option>'; 
                                                        }
                                                        else if($data_pasien['pasien_kelamin'] == "Perempuan")
                                                        {
                                                            echo '<option value="Laki-laki">Laki-laki</option>
                                                                  <option selected value="Perempuan">Perempuan</option>'; 
                                                        }
                                                    ?>
                                                    
                                                </select>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5><b>B. DATA ALAMAT DOMISILI <span class="badge badge-danger animate__animated animate__infinite animate__headShake">(KHUSUS PASIEN LUAR KOTA LANGSA)</span></b></h5> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!--<div class="col-md-3">
                                            <label>Provinsi: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_provinsi" 
                                                    class="form-control"
                                                    required>
                                                    <option value="">Pilih Provinsi</option>
                                                    <?php 
                                                        foreach($data_prov->result_array() as $d)
                                                        {
                                                    ?>   
                                                    <option value="<?php echo $d['prov_nama'] ?>"><?php echo $d['prov_nama'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div>-->
                                        <div class="col-md-3">
                                            <label>Provinsi: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_provinsi"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_provinsi'] ?>"
                                                    required>
                                            </p>
                                        </div>

                                        <!--<div class="col-md-3">
                                            <label>Kota: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_kabkota" 
                                                    class="form-control"
                                                    required>
                                                    <option value="">Pilih Kota</option>
                                                    <?php 
                                                        foreach($data_kota->result_array() as $d)
                                                        {
                                                    ?>   
                                                    <option value="<?php echo $d['kabkota_nama'] ?>"><?php echo $d['kabkota_nama'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div>-->
                                        <div class="col-md-3">
                                            <label>Kab / Kota: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_kabkota"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_kabkota'] ?>"
                                                    required>
                                            </p>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Kecamatan: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_kec"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_kec'] ?>"
                                                    required>
                                            </p>
                                        </div>

                                        <!--<div class="col-md-3">
                                            <label>Kelurahan / Desa: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_desa" 
                                                    class="form-control"
                                                    required>
                                                    <option value="">Pilih Desa</option>
                                                    <?php 
                                                        foreach($data_desa->result_array() as $d)
                                                        {
                                                    ?>   
                                                    <option value="<?php echo $d['desa_no'] ?>"><?php echo $d['desa_nama'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div>-->
                                        <div class="col-md-3">
                                            <label>Kelurahan / Desa: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_desa"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_desa'] ?>"
                                                    required>
                                            </p>
                                        </div>

                                        <!--<div class="col-md-9">
                                            <label>Alamat Detail (Jln/Lorong): <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_alamat"
                                                    class="form-control"
                                                    placeholder="Ex: Dusun Rukun, Lorong A. No.45"
                                                    required>
                                            </p>
                                        </div>-->

                                        <div class="col-md-12">
                                            <label>Alamat Detail (Jln/Lorong/Dusun): <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_alamat"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_alamat'] ?>"
                                                    required>
                                            </p>
                                        </div>
                                        
                                        
                                        <!--<div class="col-md-3">
                                            <label>Kecamatan: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_kec" 
                                                    class="form-control"
                                                    required>
                                                    <option value="">Pilih Kecamatan</option>
                                                    <?php 
                                                        foreach($data_kec->result_array() as $d)
                                                        {
                                                    ?>   
                                                    <option value="<?php echo $d['kec_nama'] ?>"><?php echo $d['kec_nama'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div>-->
                                        
                                        <!--<div class="col-md-3">
                                            <label>No Rumah: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_provinsi" 
                                                    class="form-control"
                                                    required>
                                                    <option value="">Pilih Rumah</option>
                                                    <?php 
                                                        foreach($data_prov->result_array() as $d)
                                                        {
                                                    ?>   
                                                    <option value="<?php echo $d['prov_nama'] ?>"><?php echo $d['prov_nama'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div> -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5><b>C. INFORMASI MEDIS PASIEN</b></h5> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>Tanggal Masuk RS: <font color="green" size="0">(Optional)</font></label>
                                            <p>
                                                <input
                                                    type="date"
                                                    name="pasien_tgl_masuk"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_tgl_masuk'] ?>"
                                                    >
                                            </p>
                                        </div>

                                        <div class="col-md-3">
                                            <label>Tanggal Keluar RS: <font color="purple" size="0">(Diisi saat keluar nanti)</font></label>
                                            <p>
                                                <input
                                                    disabled
                                                    type="date"
                                                    name="pasien_tgl_keluar"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_tgl_keluar'] ?>"
                                                    >
                                            </p>
                                        </div>
                                        <!--<div class="col-md-3">
                                            <label>Status Pasien: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_status" 
                                                    class="form-control"
                                                    required>
                                                    <option value="">Pilih Status</option>
                                                    <?php 
                                                        foreach($data_stts_pasien->result_array() as $d)
                                                        {
                                                    ?>   
                                                    <option value="<?php echo $d['sp_status'] ?>"><?php echo $d['sp_status'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div> -->
                                        <div class="col-md-3">
                                            <label>Tanggal Pengambilan SWAB: <font color="green" size="0">(Optional)</font></label>
                                            <p>
                                                <input
                                                    type="date"
                                                    name="pasien_tgl_pengambilan_swab"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_tgl_pengambilan_swab'] ?>"
                                                    >
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Tanggal Hasil LAB: <font color="green" size="0">(Optional)</font></label>
                                            <p>
                                                <input
                                                    type="date"
                                                    name="pasien_tgl_hasil_lab"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_tgl_hasil_lab'] ?>"
                                                    >
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Ruang Rawat Pasien RS: <font color="green" size="0">(Optional)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_ruang_rawat" 
                                                    class="form-control"
                                                    >
                                                    <option value="<?php echo $data_pasien['pasien_ruang_rawat'] ?>"><?php echo $data_pasien['pasien_ruang_rawat'] ?></option>
                                                    <?php 
                                                        foreach($data_ruangan->result_array() as $d)
                                                        {
                                                            if($data_pasien['pasien_ruang_rawat'] != $d['ruang_nama'])
                                                            {
                                                    ?>   
                                                    <option value="<?php echo $d['ruang_nama'] ?>"><?php echo $d['ruang_nama'] ?></option>
                                                    <?php   }
                                                        } ?>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Status Pasien: <font color="red" size="0">(Wajib diisi)</font></label>
                                            <p>
                                                <select 
                                                    name="pasien_stts_tindakan" 
                                                    class="form-control"
                                                    required 
                                                    >
                                                    <option value="<?php echo $data_pasien['tindakan_no'] ?>"><?php echo $data_pasien['tindakan_nama'] ?></option>
                                                    <?php 
                                                        foreach($data_tindakan->result_array() as $d)
                                                        {
                                                            if($data_pasien['pasien_stts_tindakan'] != $d['tindakan_no'])
                                                            {
                                                    ?>   
                                                    <option value="<?php echo $d['tindakan_no'] ?>"><?php echo $d['tindakan_nama'] ?></option>
                                                    <?php   }
                                                        } ?>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-12">
                                            <label>Keterangan Tambahan: <font color="green" size="0">(Optional)</font></label>
                                            <p>
                                                <input
                                                    type="text"
                                                    name="pasien_ket"
                                                    class="form-control"
                                                    value="<?php echo $data_pasien['pasien_ket'] ?>"
                                                    >
                                            </p>
                                        </div>
                                    </div>
                                    <!--
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5><b>D. LAMPIRAN PENDUKUNG</b></h5> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <label>Lampiran Berkas Pendukung:</label>
                                            <p>
                                                <input
                                                    type="file"
                                                    name="lampiran_pasien"
                                                    class="form-control"
                                                    >
                                            </p>
                                        </div>
                                    </div> -->
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-blue">
                                        TAMBAH PASIEN
                                    </button>
                                </div>
                            </form>

                        </div>
                        <?php //echo $modalTambahRs; ?>
                    </div>
                    <!-- BATAS -->
                    
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['config_nama_footer'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
        <!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="https://code.highcharts.com/highcharts.src.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>


    <!-- DATA GRAFIK -->
    <?php //echo $js_stts_kateg_pegawai; ?>
    <?php //echo $js_golongan; ?>
    <?php //echo $js_jabatan; ?>
    <?php //echo $js_bagian; ?>

    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
            UIModals.init();
        });
    </script>

    <script type="text/javascript">
      $('select').select2();
    </script>


    <?php //echo $report1, $report2, $report3 ?>


</body>

</html>