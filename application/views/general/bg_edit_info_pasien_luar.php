<?php
    // Tanggal Lahir
    function umur($tgl_lahir){
        
    // ubah ke format Ke Date Time
    $lahir = new DateTime($tgl_lahir);
    $hari_ini = new DateTime();
        
    $diff = $hari_ini->diff($lahir);
        
    // Display
   // echo "Tanggal Lahir: ". date('d M Y', strtotime($tgl_lahir)) .'<br />';
    echo $diff->y; // tahun
    //echo " ". $diff->m ." Bulan";
    //echo " ". $diff->d ." Hari";
    }
?>

<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['config_nama_header'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/logo/<?php echo $data_config['config_logo'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <?php $this->load->view('general/link_css') ?>
    
    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';
    </style>

</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="clip-home-3"></i>
                                <a href="">
                                    Dashboard
                                </a>
                            </li>
                            <li class="active">
                                    Edit Form Informasi Tambahan 
                            </li>
                           
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <?php echo $this->session->flashdata('info'); ?>
                <div class="row">
                    
                    <!-- BATAS -->
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><b style="text-transform: uppercase;">FORM EDIT INFORMASI TAMBAHAN PASIEN</b>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="w3-card-4 center">

                                    <header class="w3-container w3-light-grey">
                                      <h2 style="text-shadow: 2px 1px 1px grey; text-transform: uppercase;"><b><u>( <?php echo $data_pasien['pasien_nama'] ?> )</u></b></h2>
                                    </header>

                                    <div class="w3-container">
                                      <p>Gampong <?php echo $data_pasien['pasien_desa'].', Kecamatan '.$data_pasien['pasien_kec'].', '.$data_pasien['pasien_kabkota'].', '.$data_pasien['pasien_provinsi'] ?><br><b>KTP : <?php echo $data_pasien['pasien_nik'] ?> / Hp: <?php echo $data_pasien['pasien_tlp'] ?></b></p>
                                      <hr>
                                      <img src="<?php 
                                      if(empty($data_pasien['pasien_foto']) && $data_pasien['pasien_kelamin'] == 'Laki-laki')
                                      {
                                        echo base_url().'vendor/assets/images/img_avatar3.png';
                                      }
                                      else if(empty($data_pasien['pasien_foto']) && $data_pasien['pasien_kelamin'] == 'Perempuan')
                                      {
                                        echo base_url().'vendor/assets/images/img_avatar2.png';
                                      }
                                      else if(!empty($data_pasien))
                                      {
                                        echo base_url().'upload/pasien_foto/'.$data_pasien['pasien_foto'].'';
                                      }
                                      
                                      ?>" width="130px" 
                                      style="border-radius: 50%; box-shadow: 9px 8px 7px grey;">
                                      <br><br>
                                      <p><font color="red">**</font>Status pasien terindikasi <b><font color="red"><?php echo $data_pasien['tindakan_nama'] ?></font></b></p>
                                    </div>
                                    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/updateFoto" method="post" enctype="multipart/form-data">
                                        <p align="center">
                                            <div class="form-group">
                                                <div class="">
                                                    <input type="hidden" name="pasien_nik" value="<?php echo $this->uri->segment(3) ?>">
                                                    <p align="center"><input type="file" name="pasien_foto"></p>
                                                </div>
                                                <label class=" control-label">
                                                    <button type="submit"> Update Photo</button>
                                                </label>
                                            </div>
                                        </p>
                                    </form>

                                </div>

                               <form action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambah_info_pasien_luar/<?php echo $this->uri->segment(3) ?>" method="post" enctype="multipart/form-data" role="form" class="smart-wizard form-horizontal" id="form">
                                    <div id="wizard" class="swMain">
                                        <ul>
                                            <li>
                                                <a href="#step-1">
                                                    <div class="stepNumber">
                                                        1
                                                    </div>
                                                    <span class="stepDesc">
                                                        Step 1
                                                        <br />
                                                        <small>Step 1 Individual</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-2">
                                                    <div class="stepNumber">
                                                        2
                                                    </div>
                                                    <span class="stepDesc">
                                                        Step 2
                                                        <br />
                                                        <small>Step 2 Rujukan</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-3">
                                                    <div class="stepNumber">
                                                        3
                                                    </div>
                                                    <span class="stepDesc">
                                                        Step 3
                                                        <br />
                                                        <small>Step 3 description</small>
                                                    </span>
                                                </a>
                                            </li>
                        
                                        </ul>
                                        <div class="progress progress-striped active progress-sm">
                                            <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
                                                <span class="sr-only"> 0% Complete (success)</span>
                                            </div>
                                        </div>
                                        <div id="step-1">
                                            <h2 class="StepTitle"><b>Step 1 - Data Individu</b></h2>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    NAMA ORANG TUA 
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="" name="pasien_nama_ortu" value="<?php echo $data_pasien['pasien_nama_ortu'] ?>">
                                                    <font color="green" size="0">Optional</font>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    PROFESI PASIEN <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <select 
                                                        name="pasien_pekerjaan" 
                                                        class="form-control"
                                                        required>
                                                        <option value="<?php echo $data_pasien['pasien_pekerjaan'] ?>"><?php echo $data_pasien['pasien_pekerjaan'] ?></option>
                                                        <?php 
                                                            foreach($data_pekerjaan->result_array() as $d)
                                                            {
                                                                if($data_pasien['pasien_pekerjaan'] != $d['pekerjaan_nama'])
                                                                {
                                                        ?>   
                                                        <option value="<?php echo $d['pekerjaan_nama'] ?>"><?php echo $d['pekerjaan_nama'] ?></option>
                                                        <?php   }
                                                            } ?>
                                                    </select>
                                                    <font color="red" size="0">Wajib diisi</font>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    USIA PASIEN <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-1">
                                                    <input type="number" class="form-control" required id="" name="pasien_usia" value="<?php umur($data_pasien['pasien_tgl_lhr']); ?>">
                                                </div>
                                                <label class="control-label">
                                                    TAHUN
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    KEWARGANEGARAAN <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <select 
                                                        name="pasien_kewarganegaraan" 
                                                        class="form-control"
                                                        required>
                                                        <option value="<?php echo $data_pasien['pasien_kewarganegaraan'] ?>"><?php echo $data_pasien['pasien_kewarganegaraan'] ?></option>
                                                        <?php 
                                                            foreach($data_negara->result_array() as $d)
                                                            {
                                                                if($data_pasien['pasien_kewarganegaraan'] != $d['negara_nama'])
                                                                {
                                                        ?>   
                                                        <option value="<?php echo $d['negara_nama'] ?>"><?php echo $d['negara_nama'] ?></option>
                                                        <?php   }
                                                            } ?>
                                                    </select>
                                                    <font color="red" size="0">Wajib diisi</font>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-8">
                                                    <button class="btn btn-blue next-step btn-block">
                                                        SELANJUTNYA <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step-2">
                                            <h2 class="StepTitle"><b>Step 2 - Data Rujukan Pasien</b></h2>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    TANGGAL MELAPOR <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="date" required class="form-control" name="pasien_tgl_lapor" value="<?php echo $data_pasien['pasien_tgl_lapor']; //$data_pasien['pasien_tgl_masuk'] ?>" >
                                                    <font color="red" size="0">Wajib diisi</font>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    NAMA FASKES ASAL
                                                </label>
                                                <div class="col-sm-7">
                                                    <select 
                                                        name="pasien_faskes_asal" 
                                                        class="form-control"
                                                        >
                                                        <option value="<?php echo $data_pasien['pasien_faskes_asal'] ?>"><?php echo $data_pasien['pasien_faskes_asal'] ?></option>
                                                        <?php 
                                                            foreach($data_faskes->result_array() as $d)
                                                            {
                                                                if($data_pasien['pasien_faskes_asal'] != $d['faskes_nama'])
                                                                {
                                                        ?>   
                                                        <option value="<?php echo $d['faskes_nama'] ?>"><?php echo $d['faskes_nama'] ?></option>
                                                        <?php   }
                                                            } ?>
                                                    </select>
                                                    <font color="green" size="0">Optional</font>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    KAB/KOTA FASKES ASAL
                                                </label>
                                                <div class="col-sm-7">
                                                    <select 
                                                        name="pasien_kabkota_faskes_asal" 
                                                        class="form-control"
                                                        >
                                                        <option value="<?php echo $data_pasien['pasien_kabkota_faskes_asal'] ?>"><?php echo $data_pasien['pasien_kabkota_faskes_asal'] ?></option>
                                                        <?php 
                                                            foreach($data_kabkota->result_array() as $d)
                                                            {
                                                                if($data_pasien['pasien_kabkota_faskes_asal'] != $d['kabkota_nama'])
                                                                {
                                                        ?>   
                                                        <option value="<?php echo $d['kabkota_nama'] ?>"><?php echo $d['kabkota_nama'] ?></option>
                                                        <?php   }
                                                            } ?>
                                                    </select>
                                                    <font color="green" size="0">Optional</font>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    PROV FASKES ASAL
                                                </label>
                                                <div class="col-sm-7">
                                                    <select 
                                                        name="pasien_prov_faskes_asal" 
                                                        class="form-control"
                                                        >
                                                        <option value="<?php echo $data_pasien['pasien_prov_faskes_asal'] ?>"><?php echo $data_pasien['pasien_prov_faskes_asal'] ?></option>
                                                        <?php 
                                                            foreach($data_prov->result_array() as $d)
                                                            {
                                                                if($data_pasien['pasien_prov_faskes_asal'] != $d['prov_nama'])
                                                                {
                                                        ?>   
                                                        <option value="<?php echo $d['prov_nama'] ?>"><?php echo $d['prov_nama'] ?></option>
                                                        <?php   }
                                                            } ?>
                                                    </select>
                                                    <font color="green" size="0">Optional</font>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <button class="btn btn-light-grey back-step btn-block">
                                                        <i class="fa fa-circle-arrow-left"></i> KEMBALI
                                                    </button>
                                                </div>
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <button class="btn btn-blue next-step btn-block">
                                                        SELANJUTNYA <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="step-3">
                                            <h2 class="StepTitle"><b>Step 3 - Lampiran Pendukung</b></h2><hr>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    Berkas Lampiran (PDF)
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="file" class="form-control" name="pasien_lampiran">
                                                    <font color="green" size="0">Optional *Ex: Hasil Swab, atau dokumen penting lainnya</font>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <button class="btn btn-light-grey back-step btn-block">
                                                        <i class="fa fa-circle-arrow-left"></i> KEMBALI
                                                    </button>
                                                </div>
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <button type="submit" class="btn btn-blue btn-block">
                                                        KIRIM DATA <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php //echo $modalTambahLab; ?>
                        <?php //echo $modalEditLab; ?>
                    </div>
                    <!-- BATAS -->
                    
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['config_nama_footer'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="https://code.highcharts.com/highcharts.src.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/form-wizard.min.js"></script>


    <!-- DATA GRAFIK -->
    <?php //echo $js_stts_kateg_pegawai; ?>
    <?php //echo $js_golongan; ?>
    <?php //echo $js_jabatan; ?>
    <?php //echo $js_bagian; ?>

    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
            UIModals.init();
            FormWizard.init();
        });
    </script>

    <script type="text/javascript">
      $('select').select2();
    </script>

    <?php //echo $report1, $report2, $report3 ?>


</body>

</html>