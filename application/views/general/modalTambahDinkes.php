
<div id="modalTambahDinkes" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambahDinkes" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM TAMBAH DATA AKUN DINKES</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>USERNAME:</label>
                    <p>
                        <input
                            type="text"
                            name="dinkes_username"
                            class="form-control"
                            placeholder="Ex: ilham234"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>NAMA LENGKAP:</label>
                    <p>
                        <input
                            type="text"
                            name="dinkes_nama"
                            class="form-control"
                            placeholder="Ex: Ilham Ramadhan"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
