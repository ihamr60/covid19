<?php
    foreach($data_spesimen->result_array() as $d)
    {
?>

<div id="modalEditSpesimen<?php echo $d['jenis_spesi_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editSpesimen" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT JENIS SPESIMEN</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA JENIS SPESIMEN:</label>
                    <p>
                        <input name="jenis_spesi_no" type="hidden" value="<?php echo $d['jenis_spesi_no'] ?>">
                        <input
                            type="text"
                            name="jenis_spesi_nama"
                            class="form-control"
                            value="<?php echo $d['jenis_spesi_nama'] ?>"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
