<?php
    foreach($data_prov->result_array() as $d)
    {
?>

<div id="modalEditProv<?php echo $d['prov_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editProv" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT DATA PROVINSI</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA PROVINSI:</label>
                    <p>
                        <input name="prov_no" type="hidden" value="<?php echo $d['prov_no'] ?>">
                        <input
                            type="text"
                            name="prov_nama"
                            class="form-control"
                            value="<?php echo $d['prov_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>NEGARA:</label>
                    <p>
                         <select
                            name="prov_negara"
                            class="form-control"
                            required>
                            <option value="<?php echo $d['prov_negara'] ?>"><?php echo $d['negara_nama'] ?></option>
                             <?php 
                                foreach($data_negara->result_array() as $e)
                                {
                                    if($d['prov_negara'] != $e['negara_no'])
                                    {
                                        echo "<option value='".$e['negara_no']."'>".$e['negara_nama']."</option>";
                                    }
                                }
                             ?>
                         </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
