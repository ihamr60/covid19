
<div id="modalTambahPengguna" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambahPengguna" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM TAMBAH DATA AKUN PENGGUNA</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>USERNAME:</label>
                    <p>
                        <input
                            type="text"
                            name="rsud_username"
                            class="form-control"
                            placeholder="Ex: ilham234"
                            required>
                        <font color="red" size="0">Note: Max 16 Char. Gunakan username sebagai password default saat pertama kali login.</font>
                    </p>

                </div>
                <div class="col-md-12">
                    <label>NAMA USER + INSTANSI:</label>
                    <p>
                        <input
                            type="text"
                            name="rsud_nama"
                            class="form-control"
                            placeholder="Ex: Ilham Ramadhan (PKM Langsa Barat)"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
