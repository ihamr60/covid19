<?php
    foreach($data_hubKontak->result_array() as $d)
    {
?>

<div id="modalEditKategHubKontak<?php echo $d['tkh_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editKategHubKontak" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT DATA KATEGORI HUBUNGAN KONTAK</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA NEGARA:</label>
                    <p>
                        <input name="tkh_no" type="hidden" value="<?php echo $d['tkh_no'] ?>">
                        <input
                            type="text"
                            name="tkh_hubungan"
                            class="form-control"
                            value="<?php echo $d['tkh_hubungan'] ?>"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
