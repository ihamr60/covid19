
<div id="modalTambahTindakan" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambahTindakan" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM TAMBAH DATA JENIS TINDAKAN PASIEN (STATUS CHILD)</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA STATUS CHILD:</label>
                    <p>
                        <input
                            type="text"
                            name="tindakan_nama"
                            class="form-control"
                            placeholder="Ex: Isolasi Mandiri"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>PILIH STATUS PARENT:</label>
                    <p>
                        <select
                            name="status_pasien"
                            class="form-control"
                            required>
                            <option value="">Pilih Status Parent</option>

                            <?php 
                                foreach($status_pasien->result_array() as $d)
                                {
                                    echo "<option value='".$d['sp_no']."'>".$d['sp_status']."</option>";
                                }
                            ?>
                            
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
