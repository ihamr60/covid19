
<div id="modalTambahKec" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambahKec" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM TAMBAH DATA KECAMATAN</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA KECAMATAN:</label>
                    <p>
                        <input
                            type="text"
                            name="kec_nama"
                            class="form-control"
                            placeholder="Ex: Langsa Baro"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>KABUPATEN / KOTA:</label>
                    <p>
                        <select
                            name="kec_kabkota"
                            class="form-control"
                            required>
                            <option value="">Pilih Kab/Kota</option>
                            <?php
                                foreach($data_kabkota->result_array() as $d)
                                {
                                    echo "<option value='".$d['kabkota_no']."'>".$d['kabkota_nama']."</option>";
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
