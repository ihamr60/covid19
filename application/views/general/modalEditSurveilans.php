<?php
    foreach($data_surveyor->result_array() as $d)
    {
?>

<div id="modalEditSurveilans<?php echo $d['surveyor_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editSurveilans" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT DATA AKUN SURVEILANS</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>USERNAME:</label>
                    <p>
                        <input name="surveyor_no" type="hidden" value="<?php echo $d['surveyor_no'] ?>">
                        <input
                            type="text"
                            name="surveyor_username"
                            class="form-control"
                            value="<?php echo $d['surveyor_username'] ?>"
                            required
                            readonly>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>NAMA SURVEILANS:</label>
                    <p>
                        <input
                            type="text"
                            name="surveyor_nama"
                            class="form-control"
                            value="<?php echo $d['surveyor_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>KECAMATAN:</label>
                    <p>
                        <select
                            name="surveyor_kd_kec"
                            class="form-control"
                            required>
                            <option value="<?php echo $d['surveyor_kd_kec'] ?>"><?php echo $d['kec_nama'] ?></option>
                            <?php 
                                foreach($data_kec->result_array() as $e)
                                {
                                    if($d['surveyor_kd_kec'] != $e['kec_no'])
                                    {
                                        echo "<option value='".$e['kec_no']."'>".$e['kec_nama']."</option>";
                                    }
                                    
                                }
                            ?>
                        </select>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>PUSKESMAS:</label>
                    <p>
                        <select
                            name="surveyor_kd_pkm"
                            class="form-control"
                            required>
                            <option value="<?php echo $d['surveyor_kd_pkm'] ?>"><?php echo $d['rsud_nama'] ?></option>
                            <?php 
                                foreach($data_naungan->result_array() as $e)
                                {
                                    if($d['surveyor_kd_pkm'] != $e['rsud_no'])
                                    {
                                        echo "<option value='".$e['rsud_no']."'>".$e['rsud_nama']."</option>";
                                    }
                                    
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
