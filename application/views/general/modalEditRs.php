<?php
    foreach($data_rs->result_array() as $d)
    {
?>

<div id="modalEditRs<?php echo $d['rs_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editRs" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT DATA RUMAH SAKIT</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA RUMAH SAKIT:</label>
                    <p>
                        <input name="rs_no" type="hidden" value="<?php echo $d['rs_no'] ?>">
                        <input
                            type="text"
                            name="rs_nama"
                            class="form-control"
                            value="<?php echo $d['rs_nama'] ?>"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
