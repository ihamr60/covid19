
<div id="modalTambahDesa" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambahDesa" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM TAMBAH DATA DESA / GAMPONG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA DESA / GAMPONG:</label>
                    <p>
                        <input
                            type="text"
                            name="desa_nama"
                            class="form-control"
                            placeholder="Ex: Paya Bujok Tunong"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>KECAMATAN:</label>
                    <p>
                        <select
                            name="desa_kec"
                            class="form-control"
                            required>
                            <option value="">Pilih Kecamatan</option>
                            <?php
                                foreach($data_kec->result_array() as $d)
                                {
                                    echo "<option value='".$d['kec_no']."'>".$d['kec_nama']."</option>";
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
