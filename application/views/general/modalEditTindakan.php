<?php
    foreach($data_tindakan->result_array() as $d)
    {
?>

<div id="modalEditTindakan<?php echo $d['tindakan_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editTindakan" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT JENIS TINDAKAN PASIEN (STATUS CHILD)</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA STATUS CHILD:</label>
                    <p>
                        <input name="tindakan_no" type="hidden" value="<?php echo $d['tindakan_no'] ?>">
                        <input
                            type="text"
                            name="tindakan_nama"
                            class="form-control"
                            value="<?php echo $d['tindakan_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>PILIH STATUS PARENT:</label>
                    <p>
                        <select
                            name="status_pasien"
                            class="form-control"
                            required>
                            <option value="<?php echo $d['sp_no'] ?>"><?php echo $d['sp_status'] ?></option>
                            <?php 
                                foreach($status_pasien->result_array() as $e)
                                {
                                    if($d['sp_no'] != $e['sp_no'])
                                    {
                                        echo "<option value='".$e['sp_no']."'>".$e['sp_status']."</option>";
                                    }
                                    
                                }
                            ?>
                        </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
