<div class="row">
    <?php echo $biodata_pasien; ?>
    <div class="col-sm-7 col-md-8">
        <!--<p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas convallis porta purus, pulvinar mattis nulla tempus ut. Curabitur quis dui orci. Ut nisi dolor, dignissim a aliquet quis, vulputate id dui. Proin ultrices ultrices ligula, dictum varius
            turpis faucibus non. Curabitur faucibus ultrices nunc, nec aliquet leo tempor cursus.
        </p> -->
        <div class="row">
            <?php //echo $sub_menuProfile ?>
        </div>
        <h4>
            <b>
                <img src="<?php echo base_url() ?>vendor/assets/images/web/pdf.png" height=25px width=25px> | DOCUMENT PASIEN (E-FILLING)
            </b>
        </h4>
        <h5><b>1. ARSIP PERSONAL PASIEN</b></h5>
        <div class="row">
            <div class="col-md-12 space20">
                <a data-toggle="modal" data-target="#modalTambahArsipPeg" class="btn btn-xs btn-teal">
                    <b><i class="fa fa-plus"></i> ADD ARSIP ELEKTRONIK </b>
                </a>
            </div>
        </div>
        <div class="alert alert-warning">
            <button data-dismiss="alert" class="close">
                &times;
            </button>
            <b>NOTICE! - </b>               
           <?php
                date_default_timezone_set("Asia/Jakarta");

                $b = time();
                $jam = date("G",$b);

                if ($jam>=0 && $jam<=11)
                {
                    echo "Selamat Pagi ";
                }
                else if ($jam >=12 && $jam<=14)
                {
                    echo "Selamat Siang";
                }
                else if ($jam >=15 && $jam<=17)
                {
                    echo "Selamat Sore";
                }
                else if ($jam >=17 && $jam<=18)
                {
                    echo "Selamat Petang";
                }
                else if ($jam >=19 && $jam<=23)
                {
                    echo "Selamat Malam";
                }

                ?> 
                <?php echo $nama; ?>. Tekan Kombinasi <b>CTRL+SHIFT+ALT</b> saat hendak melihat E-Filing.
                <br>
                <b>WARNING! - <font size="4" color="red">**</font> Please Upload All Required Files!</b>
        </div> 



        <div class="tab-pane">
            <table class="table table-striped table-bordered table-hover" id="sample-table-2">
                <thead>
                    <tr>
                        <th class="center" width="10px">#</th>
                        <th class="hidden-xs">FILE ARSIP ELEKTRONIK</th>
                        <th class="center" width="160px">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    
                <?php 
                 /*   $no=1;
                    foreach($data_filing_umum->result_array() as $d)
                    {*/
                ?>
                    <tr>
                        <td class="center"><?php //echo $no ?></td>
                        <td class="hidden-xs" style='text-transform:uppercase'><b><?php //echo $d['filing_name'] ?></b> <span class="label label-warning"><font color="black"><?php //echo $d['kategori_file'] ?></font></span></td>
                        <td class="center">
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <a data-toggle="modal" data-target="#modalEditArsipPeg<?php //echo $d['filing_uuid_umum'] ?>" class="btn btn-teal btn-xs tooltips" data-placement="top" data-original-title="Edit / Change Document"><i class="fa fa-edit"></i> Edit</a>
                                <a target="_blank" href="<?php echo base_url() ?>upload/arsip_pegawai/<?php //echo $d['filing_lampiran'] ?>" class="btn btn-green btn-xs tooltips" data-placement="top" data-original-title="Lihat / Download E-Filing"><i class="fa fa-file"></i> Lihat Arsip</a>
                                <a href="<?php echo base_url();?>index.php/crud_pegawai/hapusArsipPeg/<?php //echo $d['filing_uuid_umum'] ?>/<?php //echo $d['filing_nik_ktp'] ?>/<?php //echo $d['filing_lampiran'] ?>" class="btn btn-xs btn-bricky tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data E-Filing ini?')"><i class="fa fa-trash"></i></a>
                            </div>
                            <div class="visible-xs visible-sm hidden-md hidden-lg">
                                <div class="btn-group">
                                    <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                    </a>
                                    <ul role="menu" class="dropdown-menu pull-right">
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">
                                                <i class="fa fa-share"></i> Share
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php //$no++; } ?>
                <?php 
                     /*   
                        foreach($data_filing_wajib->result_array() as $d)
                        {*/
                    ?>
                    <tr>
                        <td class="center"><?php //echo $no ?></td>
                        <td class="hidden-xs" style='text-transform:uppercase'><b><?php //echo $d['filename_req'] ?> <font size="3" color="red">**</font></b> <span class="label label-warning"><font color="black"><?php //echo $d['kategori_file'] ?></font></span> 
                            <?php 

                              /*  foreach($data_filing_wajib->result_array() as $a)
                                { 
                                    $cek   = $this->web_app_model->get2WhereOneItemOrder($this->uri->segment(3),'filing_wajib_nik_ktp',$d["uuid_file_req"],'filing_wajib_uuid_file_req','tbl_filing_wajib','no_urut_filing_wajib','DESC');

                                    if (count($cek) > 0) 
                                    {
                                        echo '<img class="tooltips" data-placement="top" data-original-title="Sudah di Unggah" src="'.base_url().'/vendor/assets/images/web/checklist.png"';
                                        break;
                                    }
                                    else
                                    {
                                        echo '<img class="tooltips" data-placement="top" data-original-title="Belum di Unggah" src="'.base_url().'/vendor/assets/images/web/false.png"';
                                        break;
                                    }
                                } */
                            ?>
                            
                        </td>
                        <td class="center">
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <?php 
                                   /* foreach($data_filing_wajib->result_array() as $a)
                                                            { 
                                        $cek   = $this->web_app_model->get2WhereOneItemOrder($this->uri->segment(3),'filing_wajib_nik_ktp',$d["uuid_file_req"],'filing_wajib_uuid_file_req','tbl_filing_wajib','no_urut_filing_wajib','DESC');

                                        if (count($cek) > 0) 
                                        {
                                            echo '
                                                <a data-toggle="modal" data-target="#modalChangeFileReq'.$cek['filing_wajib_uuid'].'" class="btn btn-teal btn-xs tooltips" data-placement="top" data-original-title="Change File"><i class="fa fa-edit"></i> Change</a>
                                                <a target="_blank" href="'.base_url().'upload/arsip_pegawai_req/'.$cek["filing_wajib_lampiran"].'" class="btn btn-green btn-xs tooltips" data-placement="top" data-original-title="Lihat E-Filing"><i class="fa fa-share"></i> Lihat Arsip</a>';
                                            break;
                                        }
                                        else
                                        {
                                            echo '
                                                <a data-toggle="modal" data-target="#modalUploadFileReq'.$d['uuid_file_req'].'" href="#" class="btn btn-purple btn-xs tooltips" data-placement="top" data-original-title="Unggah File"><i class="fa fa-upload"></i> Upload</a>
                                                <a href="#" disabled class="btn btn-danger btn-xs tooltips" data-placement="top" data-original-title="Belum Unggah"><i class="fa fa-file"></i> Not Found</a>';
                                            break;
                                        }
                                    } */
                                ?>
                               
                            </div>
                            <div class="visible-xs visible-sm hidden-md hidden-lg">
                                <div class="btn-group">
                                    <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                        <i class="fa fa-cog"></i> <span class="caret"></span>
                                    </a>
                                    <ul role="menu" class="dropdown-menu pull-right">
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">
                                                <i class="fa fa-edit"></i> Edit
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">
                                                <i class="fa fa-share"></i> Share
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a role="menuitem" tabindex="-1" href="#">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php //$no++; } ?>
                </tbody>
            </table>
        </div>
        <hr>
        <h4>
            <b>
                <img src="<?php echo base_url() ?>vendor/assets/images/web/book.png" height=25px width=25px> | RIWAYAT PENDIDIKAN 
            </b>
        </h4>
        <h5><b>1. PENDIDIKAN DALAM DAN LUAR NEGERI</b></h5>
        <div class="tab-pane">
            <div class="row">
                <div class="col-md-12 space20">
                    <a data-toggle="modal" data-target="#modalTambahPendidikanPeg" class="btn btn-xs btn-teal">
                        <b><i class="fa fa-plus"></i> ADD PENDIDIKAN </b>
                    </a>
                </div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="projects">
                <thead>
                    <tr>
                        <th class="center">No</th>
                        <th class="center">Tingkat</th>
                        <th class="">Nama Pendidikan</th>
                        <th class="center">Jurusan</th>
                        <th class="center">No. / Tahun Ijazah</th>
                        <th class="center">Tempat</th>
                        <th class="center">Kepala Sekolah/Dekan</th>
                        <th width="80px" class="center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                   /* $no=1;
                        foreach($data_pendidikan_peg->result_array() as $d){ */
                    ?>
                    <tr>
                        <td class="center"><b><?php //echo $no ?></b></td>
                        <td class="center"><?php //echo $d['pend_tingkat'] ?></td>
                        <td class=""><?php //echo $d['pend_nama_pendidikan'] ?></td>
                        <td class="center"><?php //echo $d['pend_jurusan'] ?></td>
                        <td class="center"><?php //echo $d['pend_no_ijazah']."/".$d['pend_ijazah_tahun'] ?></td>
                        <td><?php //echo $d['pend_tempat'] ?></td>
                        <td class="center"><?php //echo $d['pend_kepala_sekolah'] ?></td>
                        <td class="center">
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <a data-toggle="modal" data-target="#modalEditPendidikanPeg<?php //echo $d['pend_no'] ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url();?>index.php/crud_pegawai/hapusPendidikanPegawai/<?php //echo $d['pend_no'] ?>/<?php //echo $d['pend_nik_ktp'] ?>" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data Pendidikan ini?')"><i class="fa fa-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php //$no++; } ?>
                </tbody>
            </table>
        </div>
        <h5><b>2. KURSUS / LATIHAN DALAM DAN LUAR NEGERI</b></h5>
        <div class="row">
            <div class="col-md-12 space20">
                <a data-toggle="modal" data-target="#modalTambahKursusPeg" class="btn btn-xs btn-teal">
                    <b> <i class="fa fa-plus"></i> ADD KURSUS</b>
                </a>
            </div>
        </div>
        <div class="tab-pane">
            <table class="table table-striped table-bordered table-hover" id="projects">
                <thead>
                    <tr>
                        <th class="center">No</th>
                        <th>Nama Kursus / Pelatihan</th>
                        <th class="center">Durasi Kursus</th>
                        <th class="center">Tahun SK Lulus</th>
                        <th class="center">Tempat</th>
                        <th class="">Keterangan</th>
                        <th width="80px" class="center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                   /*     $no=1;
                        foreach($data_kursus_peg->result_array() as $d)
                        { */
                    ?>
                    <tr>
                        <td class="center"><b><?php //echo $no ?></b></td>
                        <td><?php //echo $d['kursus_nama'] ?></td>
                        <td class="center"><?php //echo $d['kursus_durasi'] ?></td>
                        <td class="center"><?php //echo $d['kursus_tahun_lulus'] ?></td>
                        <td class="center"><?php //echo $d['kursus_tempat'] ?></td>
                        <td><?php //echo $d['kursus_ket'] ?></td>
                        <td class="center">
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <a data-toggle="modal" data-target="#modalEditKursusPeg<?php //echo $d['kursus_no'] ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url();?>index.php/crud_pegawai/hapusKursusPegawai/<?php //echo $d['kursus_no'] ?>/<?php //echo $d['kursus_nik_ktp'] ?>" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data Kursus ini?')"><i class="fa fa-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php //$no++; } ?>
                </tbody>
            </table>
        </div>
        <hr>
        <h4>
            <b>
                <img src="<?php echo base_url() ?>vendor/assets/images/web/people.png" height=25px width=25px> | RIWAYAT PEKERJAAN 
            </b>
        </h4>
        <h5><b>1. RIWAYAT KEPANGKATAN GOLONGAN RUANG PENGGAJIAN</b></h5>
        <div class="row">
            <div class="col-md-12 space20">
                <a data-toggle="modal" data-target="#modalTambahPangkatPeg" class="btn btn-xs btn-teal">
                    <b> <i class="fa fa-plus"></i> ADD KEPANGKATAN </b>
                </a>
            </div>
        </div>
        <div class="tab-pane">
            <table class="table table-striped table-bordered table-hover" id="projects">
                <thead>
                    <tr>
                        <th class="center">No</th>
                        <th class="center">Pangkat</th>
                        <th class="center">Golongan</th>
                        <th class="center">Terhitung Mulai</th>
                        <th class="center">Gaji Pokok</th>
                        <th class="center">Pejabat</th>
                        <th class="center">No. SK</th>
                        <th class="center">Tanggal SK</th>
                        <th class="center">Keterangan</th>
                        <th class="center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                     /*   $no=1;
                        foreach($data_riwpang_peg->result_array() as $d)
                        { */
                    ?>
                    <tr>
                        <td class="center"><b><?php //echo $no ?></b></td>
                        <td><?php //echo $d['riwpang_pangkat'] ?></td>
                        <td class="center"><?php //echo $d['riwpang_gol'] ?></td>
                        <td class="center"><?php //echo date('d-m-Y', strtotime($d['riwpang_tmt'])) ?></td>
                        <td class="center"><?php //echo $d['riwpang_gaji'] ?></td>
                        <td class="center"><?php //echo $d['riwpang_pejabat'] ?></td>
                        <td class="center"><?php //echo $d['riwpang_no_sk'] ?></td>
                        <td class="center"><?php //echo date('d-m-Y', strtotime($d['riwpang_tgl_sk'])) ?></td>
                        <td class="center"><?php //echo $d['riwpang_ket'] ?></td>
                        <td class="center">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modalEditPangkatPeg<?php //echo $d['riwpang_no'] ?>">
                                            <i class="clip-pencil-2"></i> Edit Data
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>index.php/crud_pegawai/hapusPangkatPegawai/<?php //echo $d['riwpang_no'] ?>/<?php //echo $d['riwpang_nik_ktp'] ?>" onclick="return confirm('Anda yakin akan menghapus data Riwayat Pangkat ini?')">
                                            <i class="fa fa-remove"></i> Hapus
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php //$no++; } ?> 
                </tbody>
            </table>
        </div>
        <h5><b>2. PENGALAMAN JABATAN / PEKERJAAN</b></h5>
        <div class="row">
            <div class="col-md-12 space20">
                <a data-toggle="modal" data-target="#modalTambahRiwPekPeg" class="btn btn-xs btn-teal">
                    <b><i class="fa fa-plus"></i> ADD PENGALAMAN KERJA / JABATAN </b>
                </a>
            </div>
        </div>
        <div class="tab-pane">
            <table class="table table-striped table-bordered table-hover" id="projects">
                <thead>
                    <tr>
                        <th class="center">No</th>
                        <th class="center">Jabatan / Pekerjaan</th>
                        <th class="center">Terhitung Mulai</th>
                        <th class="center">Golongan</th>
                        <th class="center">Gaji Pokok</th>
                        <th class="center">Pejabat</th>
                        <th class="center">No. SK</th>
                        <th class="center">Tgl. SK</th>
                        <th class="center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                      /*  $no=1;
                        foreach($data_riwpek_peg->result_array() as $d)
                        { */
                    ?>
                    <tr>
                        <td class="center"><b><?php //echo $no ?></b></td>
                        <td class="center"><?php //echo $d['riwpek_jabatan'] ?></td>
                        <td class="center"><?php //echo date('d-m-Y', strtotime($d['riwpek_tmt'])) ?></td>
                        <td class="center"><?php //echo $d['riwpek_gol'] ?></td>
                        <td class="center"><?php //echo $d['riwpek_gaji'] ?></td>
                        <td class="center"><?php //echo $d['riwpek_pejabat'] ?></td>
                        <td class="center"><?php //echo $d['riwpek_no_sk'] ?></td>
                        <td class="center"><?php //echo date('d-m-Y', strtotime($d['riwpek_tgl_sk'])) ?></td>
                        <td class="center">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#modalEditRiwPekPeg<?php //echo $d['riwpek_no'] ?>">
                                            <i class="clip-pencil-2"></i> Edit Data
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a role="menuitem" tabindex="-1" href="<?php echo base_url();?>index.php/crud_pegawai/hapusRiwPekPegawai/<?php //echo $d['riwpek_no'] ?>/<?php //echo $d['riwpek_nik_ktp'] ?>" onclick="return confirm('Anda yakin akan menghapus data Riwayat Pekerjaan / Jabatan ini?')">
                                            <i class="fa fa-remove"></i> Hapus
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php //$no++; } ?>
                </tbody>
            </table>
        </div>
        <hr>
        <h4>
            <b>
                <img src="<?php echo base_url() ?>vendor/assets/images/web/airplane.png" height=25px width=25px> | PENGALAMAN KELUAR NEGERI 
            </b>
        </h4>
        <div class="row">
            <div class="col-md-12 space20">
                <a data-toggle="modal" data-target="#modalTambahRiwLuPeg" class="btn btn-xs btn-teal">
                    <b> <i class="fa fa-plus"></i> ADD PENGALAMAN LUAR NEGERI </b>
                </a>
            </div>
        </div>
        <div class="tab-pane">
            <table class="table table-striped table-bordered table-hover" id="projects">
                <thead>
                    <tr>
                        <th class="center">No</th>
                        <th class="center">Negara</th>
                        <th class="center">Tujuan Kunjungan</th>
                        <th class="center">Lamanya</th>
                        <th class="center">Yang Membiayai</th>
                        <th class="center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                      /*  $no = 1;
                        foreach($data_riwlu_peg->result_array() as $d)
                        { */
                    ?>
                    <tr>
                        <td class="center"><b><?php //echo $no ?></b></td>
                        <td class="center"><?php //echo $d['riwlu_negara'] ?></td>
                        <td class="center"><?php //echo $d['riwlu_tujuan'] ?></td>
                        <td class="center"><?php //echo $d['riwlu_durasi'] ?></td>
                        <td class="center"><?php //echo $d['riwlu_fasilitator'] ?></td>
                        <td class="center">
                            <div class="visible-md visible-lg hidden-sm hidden-xs">
                                <a data-toggle="modal" data-target="#modalEditRiwLuPeg<?php //echo $d['riwlu_no'] ?>" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url();?>index.php/crud_pegawai/hapusRiwLuPegawai/<?php //echo $d['riwlu_no'] ?>/<?php //echo $d['riwlu_nik_ktp'] ?>" class="btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" onclick="return confirm('Anda yakin akan menghapus data Riwayat Ke Luar Negeri ini?')"><i class="fa fa-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php //$no++; } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>