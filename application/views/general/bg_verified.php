<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['config_nama_header'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/logo/<?php echo $data_config['config_logo'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <?php $this->load->view('general/link_css') ?>
    
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';
    </style>

</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="clip-home-3"></i>
                                <a href="">
                                    Dashboard
                                </a>
                            </li>
                            <li class="active">
                                    Data Pasien Terverifikasi <font color="red"><?php echo $this->uri->segment(3) ?> <?php if (isset($_GET['allData']) && $_GET['allData']==1){echo 'All Data Pasien';}?></font>
                            </li>
                           
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <marquee>SELAMAT DATANG DI SISTEM INFORMASI COVID-19 KOTA LANGSA</marquee>
                <?php echo $this->session->flashdata('info'); ?>
                <div class="row">
                    
                    <!-- BATAS -->
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i><b style="text-transform: uppercase;"> <?php if (isset($_GET['allData']) && $_GET['allData']==1){echo 'All Data Pasien';}?> - DATA PASIEN COVID-19 STATUS TERVERIFIKASI <font color="red"><?php echo $this->uri->segment(3) ?></font> BY DINKES <font color="red">**</font></b>
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-link panel-collapse collapses" href="#"> </a>
                                    <a data-original-title="RESIZE FULL" data-content="Klik icon tersebut untuk tampilan full table" data-placement="top" data-trigger="hover" id="test" class="btn btn-xs btn-link panel-expand popovers" href="#"> <i class="fa fa-laptop"></i> </a>
                                    <a class="btn btn-xs btn-link panel-close" href="#"> <i class="fa fa-times"></i> </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <!--<div class="row">
                                    <div class="col-md-12 space20">
                                        <a data-toggle="modal" href="#modalTambahRs" class="btn btn-purple">
                                            <i class="fa fa-plus"></i> TAMBAH RUMAH SAKIT 
                                        </a>
                                    </div> 
                                </div>-->
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <!--<th width="1" class="center">#ID</th>-->
                                                <th width="100" class="center col-to-export">TANGGAL UPDATE</th>
                                                <th width="" class="col-to-export">NO. IDENTITAS</th>
                                                <th width="" class="col-to-export">NAMA </th>
                                                <th width="" class="col-to-export">NO TLP</th>
                                                <th width="" class="col-to-export center">KELAMIN</th>
                                                <!--<th width="" class="col-to-export">TANGGAL LAHIR</th>-->
                                                <th width="" class="col-to-export">DESA</th>
                                                <th width="" class="col-to-export">KAB/KOTA</th> 
                                                <th width="" class="col-to-export">KECAMATAN</th>
                                                <th width="" class="col-to-export">PROVINSI</th>
                                                <!--<th width="" class="col-to-export center">NO RUMAH</th>-->
                                                <!--<th width="" class="col-to-export">TANGGAL KELUAR</th>-->
                                                <!--<th width="" class="col-to-export center">STATUS</th>-->
                                                <!--<th width="" class="col-to-export">KOMORBID</th>
                                                <th width="" class="col-to-export">TANGGAL SWAB</th>
                                                <th width="" class="col-to-export">TGL HASIL LAB</th>
                                                <th width="" class="col-to-export">RUANG RAWAT</th>-->
                                                <th width="" class="col-to-export center">TINDAKAN</th>
                                                <th width="" class="col-to-export center">NO PCR/SWAB</th>
                                                <th width="" class="col-to-export center">TANGGAL PCR/SWAB</th>
                                                <th width="" class="col-to-export center">TANGGAL HASIL PCR/SWAB</th>
                                                <th width="" class="col-to-export center">STATUS</th>
                                                <!--<th width="" class="col-to-export center">LAPORAN TERAKHIR</th>-->
                                                <th width="" class="col-to-export center">PASCA LAPOR</th>
                                                <th width="" class="col-to-export center">KETERANGAN</th>
                                                <th width="1" class="center">ACT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $no=1;
                                                foreach ($data_verified->result_array() as $d)
                                                {
                                                    //$kec   = $this->web_app_model->getWhereOneItem($d['desa_kec'],'kec_no','tbl_kec');
                                            ?>
                                                
                                                <!--<td class="center"><?php echo $d['pasien_no']; ?></td>-->
                                                <td class="center"><?php echo $d['pasien_tgl_lapor']; ?></td>
                                                <td align="right">RM. <b><?php echo $d['pasien_no_rm'].' |</b><br> KTP. <b>'.$d['pasien_nik'].' |' ?></b></td>
                                                <td style="text-transform: uppercase;"><b><?php echo $d['pasien_nama'] ?></b></td>
                                                <td><?php echo $d['pasien_tlp'] ?></td>
                                                <td style="text-transform: uppercase;" class="center"><b><?php echo $d['pasien_kelamin'] ?></b></td>
                                                <!--<td><?php echo $d['pasien_tgl_lhr'] ?></td>-->
                                                <td style="text-transform: uppercase;"><?php echo $d['desa_nama'] ?></td>
                                                <td style="text-transform: uppercase;"><?php echo $d['kabkota_nama'] ?></td> 
                                                <td style="text-transform: uppercase;"><?php echo $d['kec_nama'] ?></td>
                                                <td style="text-transform: uppercase;"><?php echo $d['prov_nama'] ?></td>
                                                <!--<td class="center"><b><?php echo $d['pasien_no_rumah'] ?></b></td>-->
                                                <!--<td><?php echo $d['pasien_tgl_keluar'] ?></td>-->
                                                <!--<td style="text-transform: uppercase;" class="center"><span class="badge badge-orange"><?php echo $d['pasien_status'] ?></span></td>-->
                                                <!--<td><?php //echo $d['pasien_komorbid'] ?></td>
                                                <td><?php echo $d['pasien_tgl_pengambilan_swab'] ?></td>
                                                <td><?php echo $d['pasien_tgl_hasil_lab'] ?></td>
                                                <td><?php echo $d['pasien_ruang_rawat'] ?></td>-->
                                                <td style="text-transform: uppercase;" class="center"><b><?php echo $d['tindakan_nama'] ?></b></td>
                                                <td class="center">
                                                    <?php 
                                                        if($d['pasien_no_swabpcr'] == '')
                                                        {
                                                            echo '<span class="badge badge-danger">Tidak Diketahui</span>';
                                                        }
                                                        else
                                                        {
                                                            echo $d['pasien_no_swabpcr'];
                                                        }
                                                    ?>
                                                </td>        
                                                <td class="center">
                                                    <?php 
                                                        if($d['pasien_tgl_pengambilan_swab'] == '0000-00-00')
                                                        {
                                                            echo '<span class="badge badge-danger">Tidak Diketahui</span>';
                                                        }
                                                        else
                                                        {
                                                            echo $d['pasien_tgl_pengambilan_swab'];
                                                        }
                                                    ?>
                                                </td>
                                                <td class="center">
                                                    <?php 
                                                        if($d['pasien_tgl_hasil_lab'] == '0000-00-00')
                                                        {
                                                            echo '<span class="badge badge-danger">Tidak Diketahui</span>';
                                                        }
                                                        else
                                                        {
                                                            echo $d['pasien_tgl_hasil_lab'];
                                                        }
                                                    ?>
                                                </td>
                                                <td><span class="badge badge-teal animate__animated animate__infinite animate__headShake">PASIEN LOKAL</span></td>
                                                <!--<td><?php echo $d['pasien_tgl_lapor'] ?>-->
                                                <td style="text-transform: uppercase;" class="center"><b>
                                                    <?php 
                                                        $tgl1 = new DateTime($d['pasien_tgl_lapor']);
                                                        $tgl2 = new DateTime(date('Y-m-d'));
                                                        $e = $tgl2->diff($tgl1)->days + 1;
                                                        echo $e.' Hari';
                                                    ?>
                                                </b></td>
                                                <td class="center"><?php 
                                                        if(!empty($d['pasien_ket']))
                                                        {
                                                            echo $d['pasien_ket'];
                                                        }
                                                        else
                                                        {
                                                            echo '<span class="badge badge-danger">Tanpa Keterangan</span>';
                                                        }
                                                        
                                                    ?></td>
                                                <td align="center">
                                                    <div class="btn-group">
                                                        <a class="btn btn-purple dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                            <i class="fa fa-cog"></i> <span class="caret"></span>
                                                        </a>
                                                        <ul role="menu" class="dropdown-menu pull-right">
                                                            <?php 
                                                                if($status=="Admin Dinas Kesehatan" && $d['pasien_verified_dinkes'] != 1)
                                                                {
                                                            ?>
                                                                <li role="presentation">
                                                                    <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/dinkes/verifikasi_pasien/<?php echo $d['pasien_nik'] ?>" onclick="return confirm('Anda yakin akan memverifikasi data ini?')">
                                                                        <i class="fa fa-check"></i> Verifikasi
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                            <?php 
                                                                if($status!="Petugas Surveilans")
                                                                {
                                                            ?>
                                                                <li role="presentation">
                                                                    <a role="menuitem" data-toggle="modal" href="#modalUpdateStatus<?php echo $d['pasien_no'] ?>"  tabindex="-1">
                                                                        <i class="clip-pencil-2"></i> Update Status
                                                                    </a>
                                                                </li>
                                                                <li role="presentation">
                                                                    <a role="menuitem" tabindex="-1" href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/hapus_pasien/<?php echo $d['pasien_nik'] ?>/<?php echo $d['pasien_foto'] ?>/<?php echo $d['pasien_lampiran'] ?>" onclick="return confirm('Anda yakin akan menghapus data ini?')">
                                                                        <i class="fa fa-trash"></i> Hapus Data
                                                                    </a>
                                                                </li>
                                                                <?php } ?>
                                                                <li role="presentation">
                                                                    <a href="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/bg_detail_profile/<?php echo $d['pasien_nik'] ?>?tab1=1"  tabindex="-1">
                                                                        <i class="clip-user-2"></i> Detail Profile
                                                                    </a>
                                                                </li>
                                                                
                                                                
                                                           
                                                        </ul>
                                                    </div>   
                                                </td>
                                                
                                            </tr>
                                        <?php $no++; } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php echo $modalUpdateStatus; ?>
                        <?php //echo $modalEditRs; ?>
                    </div>
                    <!-- BATAS -->
                    
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['config_nama_footer'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="https://code.highcharts.com/highcharts.src.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>


    <!-- DATA GRAFIK -->
    <?php //echo $js_stts_kateg_pegawai; ?>
    <?php //echo $js_golongan; ?>
    <?php //echo $js_jabatan; ?>
    <?php //echo $js_bagian; ?>

    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
            UIModals.init();
        });
    </script>

    <?php //echo $report1, $report2, $report3 ?>


</body>

</html>