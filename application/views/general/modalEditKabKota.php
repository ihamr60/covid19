<?php
    foreach($data_kabkota->result_array() as $d)
    {
?>

<div id="modalEditKabKota<?php echo $d['kabkota_no'] ?>" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/editKabKota" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM EDIT DATA KABUPATEN / KOTA</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>NAMA KABUPATEN / KOTA:</label>
                    <p>
                        <input name="kabkota_no" type="hidden" value="<?php echo $d['kabkota_no'] ?>">
                        <input
                            type="text"
                            name="kabkota_nama"
                            class="form-control"
                            value="<?php echo $d['kabkota_nama'] ?>"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>PROVINSI:</label>
                    <p>
                         <select
                            name="kabkota_prov"
                            class="form-control"
                            required>
                            <option value="<?php echo $d['kabkota_prov'] ?>"><?php echo $d['prov_nama'] ?></option>
                             <?php 
                                foreach($data_prov->result_array() as $e)
                                {
                                    if($d['kabkota_prov'] != $e['prov_no'])
                                    {
                                        echo "<option value='".$e['prov_no']."'>".$e['prov_nama']."</option>";
                                    }
                                }
                             ?>
                         </select>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>

<?php } ?>
