
<div id="modalTambahStatus" class="modal fade" tabindex="-1" data-width="500" style="display: none;">
    <form role="form" action="<?php echo base_url() ?>index.php/<?php echo $kontroller ?>/tambahStatus" method="post" enctype="multipart/form-data" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">FORM TAMBAH DATA STATUS PASIEN</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>STATUS PASIEN:</label>
                    <p>
                        <input
                            type="text"
                            name="sp_status"
                            class="form-control"
                            placeholder="Ex: Positif (No Spasi)"
                            required
                            onkeypress="return event.charCode < 32 || event.charCode  >35">
                    </p>
                    <font size="0" color="red">NOTE: NO SPASI</font>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                Cancel
            </button>
            <button type="submit" class="btn btn-blue">
                Tambahkan
            </button>
        </div>
    </form>
</div>
