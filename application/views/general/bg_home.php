<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['config_nama_header'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/logo/<?php echo $data_config['config_logo'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />




    <link href="<?php echo base_url(); ?>vendor/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <?php $this->load->view('general/link_css') ?>
    
    <!-- DATA GRAFIK 
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';
    </style>-->

</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
            <?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="clip-home-3"></i>
                                <a href="">
                                    Dashboard
                                </a>
                            </li>
                           
                            <li class="search-box">
                                <form class="sidebar-search">
                                    <div class="form-group">
                                        <input type="text" placeholder="Start Searching...">
                                        <button class="submit">
                                            <i class="clip-search-3"></i>
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <marquee>SELAMAT DATANG DI SISTEM INFORMASI COVID-19 KOTA LANGSA</marquee>
                <?php echo $this->session->flashdata('info'); ?>
                <div class="row">

               
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>TABEL CAKUPAN VAKSINASI KOTA LANGSA <b></b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <br>
                            <h4 align="center">TABEL CAKUPAN VAKINASI I, II & III DI LINGKUNGAN PEMERINTAHAN KOTA LANGSA <br> Sumber data: Sistem Informasi Aceh Terpadu (SIAT) Provinsi Aceh</h4><br>
                           <table class="table table-striped table-bordered table-hover" id="projects">
                                <thead>
                                    <tr>
                                        <th class="center col-to-export">NO</th>
                                        <th>GOLONGAN PESERTA VAKSINASI</th>
                                        <th class="center col-to-export">TOTAL PESERTA VAKSINASI</th>
                                        <th class="center col-to-export" style="color: green;">SUDAH VAKSIN I</th>
                                        <th class="center col-to-export" style="color: green;">SUDAH VAKSIN II</th>
                                        <th class="center col-to-export" style="color: green;">SUDAH VAKSIN III</th>
                                        <th class="center col-to-export" style="color: blue;">TUNDA VAKSIN I</th>
                                        <th class="center col-to-export" style="color: blue;">TUNDA VAKSIN II</th>
                                        <th class="center col-to-export" style="color: blue;">TUNDA VAKSIN III</th>
                                        <th class="center col-to-export" style="color: red;">BATAL VAKSIN I</th>
                                        <th class="center col-to-export" style="color: red;">BATAL VAKSIN II</th>
                                        <th class="center col-to-export" style="color: red;">BATAL VAKSIN III</th>
                                        <th class="center col-to-export" style="color: purple;">PERSENTASE CAKUPAN VAKSIN I</th>
                                        <th class="center col-to-export" style="color: purple;">PERSENTASE CAKUPAN VAKSIN II</th>
                                        <th class="center col-to-export" style="color: purple;">PERSENTASE CAKUPAN VAKSIN III</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no=1;
                                        foreach($vaksin as $d)
                                        {
                                    ?>
                                    <tr>
                                        <td class="center col-to-export"><?php echo $no ?>.</td>
                                        <td class="col-to-export" style="text-transform: uppercase;"><b><?php echo $d->golongan ?></b></td>
                                        <td class="col-to-export" align="right"><?php echo $d->peserta_vaksinasi ?> Orang</td>
                                        <td class="col-to-export" align="right" style="color: green;"><b><?php echo $d->status_vaksin1_sudah ?></b></td>
                                        <td class="col-to-export" align="right" style="color: green;"><b><?php echo $d->status_vaksin2_sudah ?></b></td>
                                        <td class="col-to-export" align="right" style="color: green;"><b><?php echo $d->status_vaksin3_sudah ?></b></td>
                                        <td class="col-to-export" align="right" style="color: blue;"><?php echo $d->status_vaksin1_tunda ?> </td>
                                        <td class="col-to-export" align="right" style="color: blue;"><?php echo $d->status_vaksin2_tunda ?> </td>
                                        <td class="col-to-export" align="right" style="color: blue;"><?php echo $d->status_vaksin3_tunda ?> </td>
                                        <td class="col-to-export" align="right" style="color: red;"><?php echo $d->status_vaksin1_batal ?> </td>
                                        <td class="col-to-export" align="right" style="color: red;"><?php echo $d->status_vaksin2_batal ?> </td>
                                        <td class="col-to-export" align="right" style="color: red;"><?php echo $d->status_vaksin3_batal ?> </td>
                                        <td class="col-to-export" align="right" style="color: purple;"><b><?php echo $d->persentase_cakupan_vaksin1 ?> %</b></td>
                                        <td class="col-to-export" align="right" style="color: purple;"><b><?php echo $d->persentase_cakupan_vaksin2 ?> %</b></td>
                                        <td class="col-to-export" align="right" style="color: purple;"><b><?php echo $d->persentase_cakupan_vaksin3 ?> %</b></td>
                                    </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>GRAFIK TERKONFIRMASI POSITIF <b></b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <figure class="highcharts-figure">
                              <div id="grafik1"></div>
                              
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>GRAFIK PASIEN SUSPECT <b></b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="chart-wrap">
                              <label id="patterns-enabled-label">
                                <input type="checkbox" id="patterns-enabled" checked="">
                                Enable color patterns
                              </label>

                              <figure class="highcharts-figure">
                                <div id="grafik2"></div>
                                
                              </figure>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>GRAFIK KASUS AKTIF TERKONFIRMASI POSITIF <b>(<font color="orange">PASIEN LOKAL</font> VS <font color="red">PASIEN LUAR</font>)</b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <figure class="highcharts-figure">
                              <div id="grafik3"></div>
                              
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>GRAFIK PASIEN MENINGGAL DI KOTA LANGSA <b>(PASIEN LOKAL)</b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <figure class="highcharts-figure">
                              <div id="grafik4"></div>
                             
                            </figure>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>GRAFIK KASUS AKTIF POSITIF PER GAMPONG DI KOTA LANGSA</b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <figure class="highcharts-figure">
                              <div id="grafik5"></div>
                              
                            </figure>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>TABEL RESIKO ZONASI GAMPONG <b></b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <br>
                            <h4 align="center">TABEL RESIKO ZONASI GAMPONG <br> KOTA LANGSA</h4><br>
                            <div class="flot-small-container">
                               <table class="table table-striped table-bordered table-hover"  id="sample-table-2">
                                    <thead>
                                        <tr>
                                           <!-- <th class="center">NO</th> -->
                                            <th class="col-to-export">GAMPONG</th>
                                            <th class="center col-to-export">JUMLAH</th>
                                            <th class="center col-to-export">STATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=1;
                                            foreach($zonasi_desa->result_array() as $d)
                                            {
                                        ?>
                                        <tr>
                                            <!--<td class="center"><?php echo $no ?></td> -->
                                            <td class="col-to-export" style="text-transform: uppercase;"><?php echo $d['desa_nama'] ?></td>
                                            <td class="col-to-export center"><?php echo $d['total'] ?></td>
                                            <td class="col-to-export center">
                                                <?php 
                                                    if($d['total'] > 5)
                                                    {
                                                        echo "<span class='badge badge-danger'><font color='black'>RESIKO TINGGI</font></span>";
                                                    }
                                                    else if($d['total'] >= 3)
                                                    {
                                                        echo "<span class='badge badge-orange'><font color=''>RESIKO SEDANG</font></span>";
                                                    }
                                                    else if($d['total'] >= 1)
                                                    {
                                                        echo "<span class='badge badge-yellow'><font color='black'>RESIKO RENDAH</font></span>";
                                                    }
                                                    else if($d['total'] == 0)
                                                    {
                                                        echo "<span class='badge badge-success'><font color=''>TIDAK ADA KASUS</font></span>";
                                                    } 
                                                ?>
                                                
                                            </td>
                                        </tr>
                                        <?php  $no++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="clip-stats"></i>TABEL RESIKO ZONASI KECAMATAN <b></b>
                            <div class="panel-tools">
                                <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                </a>
                                <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-refresh" href="#">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <a class="btn btn-xs btn-link panel-close" href="#">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <br>
                            <h4 align="center">TABEL RESIKO ZONASI KECAMATAN <br> KOTA LANGSA</h4><br>
                            <div class="flot-small-container">
                               <table class="table table-striped table-bordered table-hover" id="projects">
                                    <thead>
                                        <tr>
                                            <th class="center col-to-export">NO</th>
                                            <th>KECAMATAN</th>
                                            <th class="center col-to-export">JUMLAH</th>
                                            <th class="center col-to-export">STATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=1;
                                            foreach($zonasi_kec->result_array() as $d)
                                            {
                                        ?>
                                        <tr>
                                            <td class="center col-to-export"><?php echo $no ?></td>
                                            <td class="col-to-export" style="text-transform: uppercase;"><?php echo $d['kec_nama'] ?></td>
                                            <td class="col-to-export center"><?php echo $d['total'] ?></td>
                                            <td class="center col-to-export">
                                                <?php 
                                                    if($d['total'] > 5)
                                                    {
                                                        echo "<span class='badge badge-danger'><font color='black'>RESIKO TINGGI</font></span>";
                                                    }
                                                    else if($d['total'] >= 3)
                                                    {
                                                        echo "<span class='badge badge-orange'><font color=''>RESIKO SEDANG</font></span>";
                                                    }
                                                    else if($d['total'] >= 1)
                                                    {
                                                        echo "<span class='badge badge-yellow'><font color='black'>RESIKO RENDAH</font></span>";
                                                    }
                                                    else if($d['total'] == 0)
                                                    {
                                                        echo "<span class='badge badge-success'><font color=''>TIDAK ADA KASUS</font></span>";
                                                    }
                                                ?>
                                                
                                            </td>
                                        </tr>
                                        <?php $no++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['config_nama_footer'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
        <!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    
    <script src="https://code.highcharts.com/highcharts.src.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/variable-pie.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/modules/pattern-fill.js"></script>
    <script src="https://code.highcharts.com/themes/high-contrast-light.js"></script>
    <script src="https://code.highcharts.com/modules/item-series.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/autosize/dist/autosize.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/summernote/dist/summernote.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/form-elements.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>


    <!-- DATA GRAFIK -->
    <?php echo $grafik5; ?>
    <?php echo $grafik4; ?>
    <?php echo $grafik3; ?>
    <?php echo $grafik2; ?>
    <?php echo $grafik1; ?>

    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
            TableExport.init();
        });
    </script>

    

    <?php //echo $report1, $report2, $report3 ?>


</body>

</html>