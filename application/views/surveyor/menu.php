<ul class="main-navigation-menu">
    
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #346da4; color: white;"';}?> 
            href="<?php echo base_url();?>index.php/surveyor?home=1">
            <i class="clip-home-3" <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>DASHBOARD</b></span>
        </a>
    </li>
   <!-- <li>
        <a <?php if (isset($_GET['dt_tmbhpasien']) && $_GET['dt_tmbhpasien']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/rsud/bg_tambah_pasien?dt_tmbhpasien=1">
            <i class="clip-pencil" <?php if (isset($_GET['dt_tmbhpasien']) && $_GET['dt_tmbhpasien']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>FORM P.E. PASIEN</b></span>
        </a>
    </li> -->
   
    <li>
        <a <?php if (isset($_GET['dt_pasien']) && $_GET['dt_pasien']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="javascript:void(0)">
            <i class="clip-checkbox" <?php if (isset($_GET['dt_pasien']) && $_GET['dt_pasien']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            
            <span class="title"><b>DATA VERIFIED</b></span>
            <?php 
                if($data_pasien_verified->num_rows() + $pasien_pending_luar_verified->num_rows() != 0)
                {
            ?>
                <span class="badge badge-info animate__animated animate__infinite animate__heartBeat"><font color="black"><?php echo $data_pasien_verified->num_rows() + $pasien_pending_luar_verified->num_rows(); ?></font></span>

            <?php } ?>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="javascript:void(0)">
                    <i class="clip-user-2"></i>
                    <span class="title">Pasien Lokal</span>
                    <?php 
                        if($data_pasien_verified->num_rows() != 0)
                        {
                    ?>

                        <span class="badge badge-info animate__animated animate__infinite animate__heartBeat"><font color="black"><?php echo $data_pasien_verified->num_rows(); ?></font></span>
                    <?php } ?>

                        <i class="icon-arrow"></i>
                </a>
                <ul class="sub-menu">
                    <?php 
                        foreach ($data_status->result_array() as $d)
                        {
                          //  $data_verified  = $this->web_app_model->getJoinAll2Where('pasien_desa','desa_no','tbl_pasien','tbl_desa','pasien_verified_dinkes','1','pasien_status',$d['sp_status']);

                            $data_verified       = $this->web_app_model->get2JoinAll2Where('pasien_desa','desa_no','tindakan_no','pasien_stts_tindakan','tbl_pasien','tbl_desa','tbl_tindakan_pasien','tbl_pasien','pasien_verified_dinkes','1','status_pasien',$d['sp_no']);
                    
                    ?>
                            <li>
                                <!--active open-->
                                <a href="<?php echo base_url();?>index.php/surveyor/bg_verified/<?php echo $d['sp_no'] ?>?dt_pasien=1">
                                    <i class="clip-checkmark-2"></i>
                                    <span class="title"> <?php echo $d['sp_status'] ?> </span>
                                    <span class="badge badge-info animate__animated animate__tada"><font color="black"><?php echo $data_verified->num_rows(); ?> Kasus</font></span>
                                </a>
                            </li>
                    <?php } ?>
                        <!--<li>
                            <a href="<?php echo base_url();?>index.php/dinkes/bg_pasien_lainnya?dt_pasien=1">
                                <i class="clip-user-2"></i>
                                <span class="title"> Lainnya</span>
                                <span class="badge badge-info animate__animated animate__tada"><font color="black">
                                    <?php 
                                    $jml    = 0;
                                    foreach ($data_pasien_verified->result_array() as $d)
                                    {
                                        $cek_stts       = $this->web_app_model->getWhereOneItem($d['pasien_stts_tindakan'],'tindakan_no','tbl_tindakan_pasien');

                                        if(empty($cek_stts))
                                        {
                                            $jml++;
                                        }
                                        
                                    }
                                    echo $jml;
                                    ?> Kasus</font></span>
                            </a>
                        </li>-->
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url();?>index.php/surveyor/bg_verified_luar?dt_tmbhpasien=1">
                    <i class="clip-user-2"></i>
                    <span class="title">Pasien Luar</span>
                    <?php 
                        if($pasien_pending_luar_verified->num_rows() != 0)
                        {
                    ?>
                        <span class="badge badge-info animate__animated animate__infinite animate__heartBeat"><font color="black"><?php echo $pasien_pending_luar_verified->num_rows(); ?></font></span>
                    <?php } ?>
                </a>
            </li>
        </ul>   
    </li>
<li>
        <a align="center"> 
        </a>
    </li>
    <li>
        <a style="background: #0fb459; color: white; text-transform: uppercase;" align="center"> <b>DATA <?php echo $kec_surveilans['kec_nama'] ?></b>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['dt_isman']) && $_GET['dt_isman']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/surveyor/bg_isman?dt_isman=1">
            <i class="clip-file-2" <?php if (isset($_GET['dt_isman']) && $_GET['dt_isman']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>DATA PASIEN ISMAN</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['dt_lap_tracert']) && $_GET['dt_lap_tracert']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/surveyor/bg_lap_tracert?dt_lap_tracert=1">
            <i class="clip-file-2" <?php if (isset($_GET['dt_lap_tracert']) && $_GET['dt_lap_tracert']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>RIWAYAT TRACING</b></span>
        </a>
    </li>
    <li>
        <a <?php if (isset($_GET['dt_tracert']) && $_GET['dt_tracert']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/tracert/bg_data_tracert?dt_tracert=1">
            <i class="clip-user-2" <?php if (isset($_GET['dt_tracert']) && $_GET['dt_tracert']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>AKUN TRACERT</b></span>
        </a>
    </li>
    <!--<li>
        <a <?php if (isset($_GET['dt_verified']) && $_GET['dt_verified']==1){echo 'style="background: #346da4; color: white;"';}?> 
        href="<?php echo base_url();?>index.php/rsud/bg_verified?dt_verified=1">
            <i class="clip-checkbox" <?php if (isset($_GET['dt_verified']) && $_GET['dt_verified']==1){echo 'style="background: #346da4; color: white;"';}?>></i>
            <span class="title"><b>VERIFIKASI PASIEN</b></span>
        </a>
    </li> -->
    