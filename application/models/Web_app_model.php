<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_App_Model extends CI_Model 
{	

	//=====================
	// Created by : Ilham Ramadhan, S.Tr.Kom
	// 05/09/3030
	//====================

	// db2 digunakan untuk mengakses database ke-2
	// private $db2;

	// public function __construct()
	// {
	//  parent::__construct();
	//         $this->db2 = $this->load->database('db2', TRUE);
	// }

	public function getLoginData($usr, $psw) 
	{
		$u = $usr; 
		$p = md5($psw);
		
		$q_cek_login = $this->db->get_where('tbl_login', array('login_username' => $u, 'login_password' => $p)); 
		if(count($q_cek_login->result())>0)
		{
			foreach ($q_cek_login->result() as $qck)
			{	
				if($qck->login_stts=='rsud')
				{
					$q_ambil_data = $this->db->get_where('tbl_rsud', array('rsud_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->rsud_username;
						$sess_data['nama'] 		 	 = $qad->rsud_nama;
						$sess_data['stts'] 			 = 'Pengguna';
						$sess_data['kontroller'] 	 = 'rsud';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/rsud?home=1');
				}
				else if($qck->login_stts=='dinkes')
				{
					$q_ambil_data = $this->db->get_where('tbl_dinkes', array('dinkes_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->dinkes_username;
						$sess_data['nama'] 		 	 = $qad->dinkes_nama;
						$sess_data['stts'] 			 = 'Admin Dinas Kesehatan';
						$sess_data['kontroller'] 	 = 'dinkes';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/dinkes?home=1');
				}
				else if($qck->login_stts=='surveyor')
				{
					$q_ambil_data = $this->db->get_where('tbl_surveyor', array('surveyor_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->surveyor_username;
						$sess_data['nama'] 		 	 = $qad->surveyor_nama;
						$sess_data['kec'] 		 	 = $qad->surveyor_kd_kec;
						$sess_data['stts'] 			 = 'Petugas Surveilans';
						$sess_data['kontroller'] 	 = 'surveyor';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/surveyor?home=1');
				}
				else if($qck->login_stts=='admin')
				{
					$q_ambil_data = $this->db->get_where('tbl_admin', array('admin_username' => $u));
					foreach($q_ambil_data -> result() as $qad)
					{
						$sess_data['logged_in']  	 = 'yes';
						$sess_data['username'] 		 = $qad->admin_username;
						$sess_data['nama'] 		 	 = $qad->admin_nama;
						$sess_data['stts'] 			 = 'Admin';
						$sess_data['kontroller'] 	 = 'admin';
					    $this->session->set_userdata($sess_data);  
					}	
					header('location:'.base_url().'index.php/admin?home=1');
				}
			}
		}
		else
			{
				//jika terdapat session bernama "auth"
	            //untuk penamaan session bebas ya, mau "auth", "cemungud", "follback_kaka" terserah :D
	            if(isset($_SESSION['auth']))
	            {
	                //jika user gagal masuk selama 3 kali atau lebih
	                if($_SESSION['auth']>3 || $_SESSION['auth']==3){
	                        //set nilai session "auth" ke 4
	                        $_SESSION['auth']=4;
	                        //jalankan function blokir_user
	                        header('location:'.base_url().'index.php/web/blokir');
	                }
	                //jika tidak
	                else{
	                        //session "auth" ditambah 1
	                        $_SESSION['auth']=$_SESSION['auth']+1;
	                        //jalankan function login()
	                        //login();
	                        
	                        header('location:'.base_url().'index.php/web');
							$this->session->set_flashdata('info','<div class="alert alert-danger">
								                                    <button data-dismiss="alert" class="close">
								                                        &times;
								                                    </button>
								                                    <i class="fa fa-times-circle"></i>
								                                    <strong>Ups, Maaf!</strong> Username atau password anda salah.
								                                </div>');

							$this->session->set_flashdata("info2","<script type='text/javascript'>
											     Swal.fire({
													  icon: 'error',
													  title: 'Oops...',
													  text: 'Something went wrong!',
													  //footer: '<a href>Why do I have this issue?</a>'
													})
											    </script>
											    ");
	                }
	            }
	            //jika tidak ada session "auth"
	            else{
	                    //daftarkan session "auth", dan beri nilai 1
	                    $_SESSION['auth']=1;
	                    //jalankan function login()
	                   // login();
	                    header('location:'.base_url().'index.php/web');
						$this->session->set_flashdata('info','<div class="alert alert-danger">
							                                    <button data-dismiss="alert" class="close">
							                                        &times;
							                                    </button>
							                                    <i class="fa fa-times-circle"></i>
							                                    <strong>Ups, Maaf!</strong> Username atau password anda salah.
							                                </div>');

						$this->session->set_flashdata("info2","<script type='text/javascript'>
													     Swal.fire({
															  icon: 'error',
															  title: 'Oops...',
															  text: 'Something went wrong!',
															  //footer: '<a href>Why do I have this issue?</a>'
															})
													    </script>
													    ");
	            }

				
			}
	}

	public function insertData($data,$table)
	{
		$this->db->insert($table,$data);
	}

	public function getWhereOneItem($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getWhereTwoItem($where,$field,$where2,$field2,$table)
	{
		$data = array();
  		$options = array($field => $where, $field2 => $where2);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getAllData($table)
	{
		return $this->db->get($table);
		//return $this->db->get($table)->result();
	}

	public function getAllData_order($table, $order, $by)
	{
		$this->db->order_by($order,$by);
		return $this->db->get($table);
		//return $this->db->get($table)->result();
	}


	public function get_traceSuratMasuk($nomor_surat,$perihal,$pengirim)
	{
		return $this->db->query("SELECT * FROM tbl_distribusi_surat WHERE dist_perihal LIKE '%".$perihal."%' OR dist_perihal LIKE '%".$perihal."' OR dist_perihal LIKE '".$perihal."%' OR dist_perihal LIKE '".$perihal."_' OR dist_perihal LIKE '_".$perihal."' AND dist_pengirim LIKE '%".$pengirim."%' OR dist_pengirim LIKE '".$pengirim."%' OR dist_pengirim LIKE '%".$pengirim."' OR dist_pengirim LIKE '_".$pengirim."' OR dist_pengirim LIKE '".$pengirim."_' AND dist_nomorsurat LIKE '%".$nomor_surat."%'");
	}

	public function get_lap_tracert($kec_surveilans)
	{
		return $this->db->query("SELECT monitor_no, monitor_foto, pasien_no_rm, pasien_nik, monitor_updated, pasien_nama, monitor_kd_kec, kec_nama, tracert_nama, surveyor_nama, monitor_ket FROM tbl_monitor_isman INNER JOIN tbl_pasien ON tbl_monitor_isman.monitor_kd_pasien = tbl_pasien.pasien_nik INNER JOIN tbl_kec ON tbl_monitor_isman.monitor_kd_kec = tbl_kec.kec_no INNER JOIN tbl_tracert ON tbl_monitor_isman.monitor_kd_tracert = tbl_tracert.tracert_username INNER JOIN tbl_surveyor ON tbl_monitor_isman.monitor_kd_surveyor = tbl_surveyor.surveyor_username WHERE monitor_kd_kec='".$kec_surveilans."'");
	}

	public function get_riwayat_tracing($nik_pasien)
	{
		return $this->db->query("
			SELECT * FROM `tbl_monitor_isman` 
			WHERE monitor_kd_pasien = '".$nik_pasien."' 
			GROUP BY monitor_updated");
	}

	public function get_count_kelompok_usian($usia)
	{
		return $this->db->query("SELECT tanggal, kelompok_usia, COUNT(kelompok_usia) as total FROM `tbl_vaksinasi` WHERE kelompok_usia='".$usia."' GROUP BY tanggal ORDER BY tanggal ASC ");
	}

	public function get_isman($kecamatan)
	{
		return $this->db->query("

			SELECT * FROM `tbl_pasien` 

			INNER JOIN tbl_desa 
			ON tbl_pasien.pasien_desa = tbl_desa.desa_no 
			INNER JOIN tbl_kec 
			ON tbl_desa.desa_kec = tbl_kec.kec_no
			INNER JOIN tbl_tindakan_pasien 
			ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no
			LEFT JOIN tbl_tracert
			ON tbl_pasien.pasien_tracert = tbl_tracert.tracert_username
			LEFT JOIN tbl_surveyor
			ON tbl_tracert.tracert_kd_surveyor = tbl_surveyor.surveyor_username

			WHERE pasien_verified_dinkes = 1
            AND desa_kec = ".$kecamatan."
			AND pasien_tracing = 1
			#AND (pasien_stts_tindakan = 31
			#OR pasien_stts_tindakan = 26
			#OR pasien_stts_tindakan = 2)

			GROUP BY pasien_nik");

			/*SELECT * FROM `tbl_pasien` 

			INNER JOIN tbl_desa 
			ON tbl_pasien.pasien_desa = tbl_desa.desa_no 
			INNER JOIN tbl_kec 
			ON tbl_desa.desa_kec = tbl_kec.kec_no
			INNER JOIN tbl_tindakan_pasien 
			ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no
			LEFT JOIN tbl_monitor_isman
			ON tbl_pasien.pasien_nik = tbl_monitor_isman.monitor_kd_pasien
			LEFT JOIN tbl_tracert
			ON tbl_monitor_isman.monitor_kd_tracert = tbl_tracert.tracert_username
			LEFT JOIN tbl_surveyor
			ON tbl_monitor_isman.monitor_kd_surveyor = tbl_surveyor.surveyor_username

			WHERE pasien_verified_dinkes = 1
			AND (pasien_stts_tindakan = 31
			OR pasien_stts_tindakan = 26
			OR pasien_stts_tindakan = 2)

			GROUP BY pasien_nik */
	}

	public function get_surveilans()
	{
		return $this->db->query("

			SELECT * FROM `tbl_surveyor` 

			INNER JOIN tbl_kec 
			ON tbl_surveyor.surveyor_kd_kec = tbl_kec.kec_no 
			INNER JOIN tbl_rsud 
			ON tbl_surveyor.surveyor_kd_pkm = tbl_rsud.rsud_no");
	}

	public function get_tracert()
	{
		return $this->db->query("

			SELECT * FROM `tbl_tracert` 

			INNER JOIN tbl_kec 
			ON tbl_tracert.tracert_kd_kec = tbl_kec.kec_no 
			INNER JOIN tbl_surveyor 
			ON tbl_tracert.tracert_kd_surveyor = tbl_surveyor.surveyor_username");
	}
	
	public function get_zonasi_kec()
	{
		//return $this->db->query("SELECT kec_nama, COUNT(IF( pasien_verified_dinkes = 1, pasien_no, NULL)) as total FROM tbl_pasien JOIN tbl_desa ON tbl_pasien.pasien_desa = tbl_desa.desa_no RIGHT JOIN tbl_kec ON tbl_desa.desa_kec = tbl_kec.kec_no GROUP BY kec_nama");

		return $this->db->query("SELECT kec_nama, COUNT(IF( pasien_verified_dinkes = 1 && status_pasien = 99 && tindakan_no != 99 && tindakan_no != 98 && pasien_lokal = 1, pasien_no, NULL)) as total FROM tbl_pasien JOIN tbl_desa ON tbl_pasien.pasien_desa = tbl_desa.desa_no RIGHT JOIN tbl_kec ON tbl_desa.desa_kec = tbl_kec.kec_no LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no GROUP BY kec_nama");
	}

	public function get_wilayah_kerja($kd_kec)
	{

		return $this->db->query("SELECT * FROM `tbl_desa` WHERE desa_kec = ".$kd_kec." AND NOT EXISTS (SELECT * FROM tbl_area_tracert WHERE tbl_area_tracert.area_kd_desa = tbl_desa.desa_no)");
	}

	public function get_zonasi_desa()
	{
		//return $this->db->query("SELECT desa_nama, COUNT(IF( pasien_verified_dinkes = 1, pasien_no, NULL)) as total FROM tbl_pasien RIGHT JOIN tbl_desa ON tbl_pasien.pasien_desa = tbl_desa.desa_no GROUP BY desa_nama");

		return $this->db->query("SELECT desa_nama, COUNT(IF( pasien_verified_dinkes = 1 && status_pasien = 99 && tindakan_no != 99 && tindakan_no != 98 && pasien_lokal = 1, pasien_no, NULL)) as total FROM tbl_pasien RIGHT JOIN tbl_desa ON tbl_pasien.pasien_desa = tbl_desa.desa_no LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no GROUP BY desa_nama");

		//return $this->db->query("SELECT desa_no,desa_nama, pasien_no_rumah, desa_kec, COUNT(IF( pasien_verified_dinkes = 1 && status_pasien = 99 && tindakan_no != 99 && tindakan_no != 98, pasien_no, NULL)) as total FROM tbl_pasien RIGHT JOIN tbl_desa ON tbl_pasien.pasien_desa = tbl_desa.desa_no LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no GROUP BY desa_nama, pasien_no_rumah");
	}

	public function tot_positif_sembuh()
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(IF( pasien_verified_dinkes = 1 && tindakan_no = 99 && pasien_lokal = 1, pasien_no, NULL)) as positif_sembuh FROM tbl_pasien LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function tot_positif_dirawat()
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(IF( pasien_verified_dinkes = 1 && tindakan_no = 30 && pasien_lokal = 1, pasien_no, NULL)) as positif_dirawat FROM tbl_pasien LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function tot_positif_meninggal()
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(IF( pasien_verified_dinkes = 1 && tindakan_no = 98 && pasien_lokal = 1, pasien_no, NULL)) as positif_meninggal FROM tbl_pasien LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function tot_positif_isman()
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(IF( pasien_verified_dinkes = 1 && tindakan_no = 31 && pasien_lokal = 1, pasien_no, NULL)) as positif_isman FROM tbl_pasien LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function tot_positif_dipantau()
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(IF( pasien_verified_dinkes = 1 && tindakan_no = 33 && pasien_lokal = 1, pasien_no, NULL)) as positif_dipantau FROM tbl_pasien LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function total_pasien_where($stts_tindakan,$stts_pasienLokal)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(IF( pasien_verified_dinkes = 1 && tindakan_no = ".$stts_tindakan." && pasien_lokal = ".$stts_pasienLokal.", pasien_no, NULL)) as total FROM tbl_pasien LEFT JOIN tbl_tindakan_pasien ON tbl_pasien.pasien_stts_tindakan = tbl_tindakan_pasien.tindakan_no");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function updateDataWhere($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	public function getWhereAllItem($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table);
	}

	public function get2WhereAllItem($where,$field,$where2,$field2,$table)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		return $this->db->get($table);
	}

	public function get3WhereAllItem($where,$field,$where2,$field2,$where3,$field3,$table)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->where($field3,$where3);
		return $this->db->get($table);
	}

	public function getWhereAllItem_between($where,$field,$table,$date_field,$tgl_awal,$tgl_akhir)
	{
		$this->db->where($field,$where);
		$this->db->where(''.$date_field.' >=',$tgl_awal);
		$this->db->where(''.$date_field.' <=',$tgl_akhir);
		return $this->db->get($table);
	}

	public function getAllData_between($table,$date_field,$tgl_awal,$tgl_akhir)
	{
		$this->db->where(''.$date_field.' >=',$tgl_awal);
		$this->db->where(''.$date_field.' <=',$tgl_akhir);
		return $this->db->get($table);
		//return $this->db->get($table)->result();
	}	

	public function getCount($table,$field,$where)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field."='".$where."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount2Where($table,$field1,$where1,$field2,$where2)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."' AND ".$field2."='".$where2."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount3Where($table,$field1,$where1,$field2,$where2,$field3,$where3)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."' AND ".$field2."='".$where2."' AND ".$field3."='".$where3."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getCount1Where($table,$field1,$where1)
	{
		$data = array();
  		$Q = $this->db->query("SELECT COUNT(*) AS TOTAL FROM ".$table." WHERE ".$field1."='".$where1."'");
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getJoinOneWhere($idTabel1,$idTabel2,$table1,$table2,$field,$where)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function get2JoinOneWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->from($table1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function get3JoinOneWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$idTabel5,$idTabel6,$table1,$table2,$table3,$table4,$table5,$table6,$field,$where)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->join($table5, ''.$table6.'.'.$idTabel6.' = '.$table5.'.'.$idTabel5.'');
		$this->db->from($table1);
		$Q = $this->db->get();
		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function get5JoinOneWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$idTabel5,$idTabel6,$idTabel7,$idTabel8,$idTabel9,$idTabel10,$table1,$table2,$table3,$table4,$table5,$table6,$table7,$table8,$table9,$table10,$field,$where)
	{
		$data = array();
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->join($table5, ''.$table6.'.'.$idTabel6.' = '.$table5.'.'.$idTabel5.'');
		$this->db->join($table7, ''.$table8.'.'.$idTabel8.' = '.$table7.'.'.$idTabel7.'');
		$this->db->join($table9, ''.$table10.'.'.$idTabel10.' = '.$table9.'.'.$idTabel9.'');
		$this->db->from($table1);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
				{
      				$data = $query->row_array();
    			}
  		$query->free_result();
		return $data;
	}

	public function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
	}

	public function get2JoinAllWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get5JoinAllWhere($idTabel1,$idTabel2,$idTabel3,$idTabel4,$idTabel5,$idTabel6,$idTabel7,$idTabel8,$idTabel9,$idTabel10,$table1,$table2,$table3,$table4,$table5,$table6,$table7,$table8,$table9,$table10,$field,$where)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->join($table5, ''.$table6.'.'.$idTabel6.' = '.$table5.'.'.$idTabel5.'');
		$this->db->join($table7, ''.$table8.'.'.$idTabel8.' = '.$table7.'.'.$idTabel7.'');
		$this->db->join($table9, ''.$table10.'.'.$idTabel10.' = '.$table9.'.'.$idTabel9.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get5JoinAll2Where($idTabel1,$idTabel2,$idTabel3,$idTabel4,$idTabel5,$idTabel6,$idTabel7,$idTabel8,$idTabel9,$idTabel10,$table1,$table2,$table3,$table4,$table5,$table6,$table7,$table8,$table9,$table10,$field,$where,$field2,$where2)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->join($table5, ''.$table6.'.'.$idTabel6.' = '.$table5.'.'.$idTabel5.'');
		$this->db->join($table7, ''.$table8.'.'.$idTabel8.' = '.$table7.'.'.$idTabel7.'');
		$this->db->join($table9, ''.$table10.'.'.$idTabel10.' = '.$table9.'.'.$idTabel9.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get5JoinAll2Where_orderby($idTabel1,$idTabel2,$idTabel3,$idTabel4,$idTabel5,$idTabel6,$idTabel7,$idTabel8,$idTabel9,$idTabel10,$table1,$table2,$table3,$table4,$table5,$table6,$table7,$table8,$table9,$table10,$field,$where,$field2,$where2,$order,$by)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->order_by($order,$by);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->join($table5, ''.$table6.'.'.$idTabel6.' = '.$table5.'.'.$idTabel5.'');
		$this->db->join($table7, ''.$table8.'.'.$idTabel8.' = '.$table7.'.'.$idTabel7.'');
		$this->db->join($table9, ''.$table10.'.'.$idTabel10.' = '.$table9.'.'.$idTabel9.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get5JoinAll2Where_ajax($idTabel1,$idTabel2,$idTabel3,$idTabel4,$idTabel5,$idTabel6,$idTabel7,$idTabel8,$idTabel9,$idTabel10,$table1,$table2,$table3,$table4,$table5,$table6,$table7,$table8,$table9,$table10,$field,$where,$field2,$where2)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->join($table5, ''.$table6.'.'.$idTabel6.' = '.$table5.'.'.$idTabel5.'');
		$this->db->join($table7, ''.$table8.'.'.$idTabel8.' = '.$table7.'.'.$idTabel7.'');
		$this->db->join($table9, ''.$table10.'.'.$idTabel10.' = '.$table9.'.'.$idTabel9.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query->result();
	}

	public function get2JoinAll2Where($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where,$field2,$where2)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get2JoinAllWhereGroup($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where,$group)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->group_by($group); 
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function get2JoinAll2WhereGroup($idTabel1,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4,$field,$where,$field2,$where2,$group)
	{
		$this->db->where($field,$where);
		$this->db->where($field2,$where2);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->join($table3, ''.$table4.'.'.$idTabel4.' = '.$table3.'.'.$idTabel3.'');
		$this->db->group_by($group); 
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getJoinAllWhere($idTabel1,$idTabel2,$table1,$table2,$field,$where)
	{
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getJoinAllWhereOrderBy($idTabel1,$idTabel2,$table1,$table2,$field,$where,$order,$by)
	{

		
		$this->db->where($field,$where);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->order_by($order,$by);
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getJoinAll2Where($idTabel1,$idTabel2,$table1,$table2,$field,$where,$field1,$where1)
	{
		$this->db->where($field,$where);
		$this->db->where($field1,$where1);
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getAll2Join($idTabel1,$idTabel2,$table1,$table2)
	{
		$this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		$this->db->from($table1);
		$query = $this->db->get();
		return $query;
	}

	public function getAll3Join($idTabel1,$idTabel12,$idTabel2,$idTabel3,$table1,$table2,$table3)
	{
		 $this->db->select('*');
		 $this->db->from($table1);
		 $this->db->join($table2,''.$table2.'.'.$idTabel2.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table3,''.$table3.'.'.$idTabel3.'='.$table1.'.'.$idTabel12.'');
		 $query = $this->db->get();
		 return $query;
	}

	public function getAll4Join($idTabel1,$idTabel12,$idTabel2,$idTabel3,$idTabel4,$table1,$table2,$table3,$table4)
	{
		 $this->db->select('*');
		 $this->db->from($table1);
		 $this->db->join($table2,''.$table2.'.'.$idTabel2.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table3,''.$table3.'.'.$idTabel3.'='.$table1.'.'.$idTabel1.'');
		 $this->db->join($table4,''.$table4.'.'.$idTabel4.'='.$table1.'.'.$idTabel12.'');
		 $query = $this->db->get();
		 return $query;
	}
}
