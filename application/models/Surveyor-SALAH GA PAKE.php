<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surveyor extends MY_Controller {

	/**
	 Created by Ilham Ramadhan S.Tr.kom
	 0853 6188 5100
	 ilhamr6000@gmail.com
	 */

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Petugas Surveilans')
		{

			$bc['username'] 		= $this->session->userdata('username');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			
			$bc['atas'] 			= $this->load->view('dinkes/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('dinkes/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('dinkes/bio',$bc,true);	

			$this->load->view('dinkes/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/web');
		}
	}
}
