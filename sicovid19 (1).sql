-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 26, 2021 at 09:46 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sicovid19`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_desa`
--

CREATE TABLE `tbl_desa` (
  `desa_no` int(11) NOT NULL,
  `desa_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_desa`
--

INSERT INTO `tbl_desa` (`desa_no`, `desa_nama`) VALUES
(2, 'Karang Anyar'),
(3, 'Paya Bujok Tunong'),
(4, 'Lengkong');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_diagnos`
--

CREATE TABLE `tbl_diagnos` (
  `diagnos_no` int(11) NOT NULL,
  `diagnos_nik_pasien` varchar(16) NOT NULL,
  `diagnos_pneumonia` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `diagnos_ards` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `diagnos_lainnya` varchar(100) NOT NULL COMMENT 'sebutkan',
  `diagnos_etiologi_pernafasan` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `diagnos_detail_etiologi_pernafasan` varchar(100) NOT NULL COMMENT 'Jika Etiologi Pernafasan Ya, Sebutkan!',
  `diagnos_pernah_rawat_di_rs` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `diagnos_nama_rs_terakhir` varchar(30) NOT NULL COMMENT 'Jika Ya pernah rawat RS, Sebutkan RS nya',
  `diagnos_tgl_masuk_rs_terakhir` date NOT NULL COMMENT 'Jika Ya pernah Rawat RS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dinkes`
--

CREATE TABLE `tbl_dinkes` (
  `dinkes_no` int(11) NOT NULL,
  `dinkes_username` varchar(15) NOT NULL,
  `dinkes_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_dinkes`
--

INSERT INTO `tbl_dinkes` (`dinkes_no`, `dinkes_username`, `dinkes_nama`) VALUES
(1, 'dinkes', 'Ilham Ramadhan (DINKES)');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faskes`
--

CREATE TABLE `tbl_faskes` (
  `faskes_no` int(11) NOT NULL,
  `faskes_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_faskes`
--

INSERT INTO `tbl_faskes` (`faskes_no`, `faskes_nama`) VALUES
(1, 'Tes'),
(3, 'Nama Fasilitas Kesehatan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fasyankes`
--

CREATE TABLE `tbl_fasyankes` (
  `fasyankes_no` int(11) NOT NULL,
  `fasyankes_username` varchar(15) NOT NULL,
  `fasyankes_nama_user` varchar(30) NOT NULL,
  `fasyankes_nama_instansi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gejala`
--

CREATE TABLE `tbl_gejala` (
  `gejala_no` int(11) NOT NULL,
  `gejala_nik_pasien` varchar(16) NOT NULL COMMENT 'FK',
  `gejala_tgl_gejala` date NOT NULL,
  `gejala_demam` varchar(10) NOT NULL COMMENT '>=38 Derjat - Ya/Tdk/Tdk Tahu',
  `gejala_riwayat_demam` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_batuk` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_pilek` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_sakit_tenggorokan` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_sesak_nafas` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_sakit_kepala` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_lemah` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_nyeri_otot` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_mual_muntah` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_nyeri_abdomen` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_diare` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `gejala_gejala_lainnya` varchar(100) NOT NULL COMMENT 'sebutkan'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_spesimen`
--

CREATE TABLE `tbl_jenis_spesimen` (
  `jenis_spesi_no` int(11) NOT NULL,
  `jenis_spesi_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_jenis_spesimen`
--

INSERT INTO `tbl_jenis_spesimen` (`jenis_spesi_no`, `jenis_spesi_nama`) VALUES
(2, 'Nasopharyngeal (NP) SWAB'),
(3, 'Oropharyngeal (NP) SWAB'),
(4, 'Sputum'),
(5, 'Serum');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kabkota`
--

CREATE TABLE `tbl_kabkota` (
  `kabkota_no` int(11) NOT NULL,
  `kabkota_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kabkota`
--

INSERT INTO `tbl_kabkota` (`kabkota_no`, `kabkota_nama`) VALUES
(1, 'Langsa'),
(2, 'Banda Aceh'),
(4, 'Medan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kateg_hasil_spesimen`
--

CREATE TABLE `tbl_kateg_hasil_spesimen` (
  `spesimen_no` int(11) NOT NULL,
  `spesimen_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kateg_hasil_spesimen`
--

INSERT INTO `tbl_kateg_hasil_spesimen` (`spesimen_no`, `spesimen_nama`) VALUES
(3, 'Positif'),
(4, 'Negatif');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kateg_hubungan_kontak`
--

CREATE TABLE `tbl_kateg_hubungan_kontak` (
  `tkh_no` int(11) NOT NULL,
  `tkh_hubungan` varchar(30) NOT NULL COMMENT 'ex; SUami\r\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kateg_hubungan_kontak`
--

INSERT INTO `tbl_kateg_hubungan_kontak` (`tkh_no`, `tkh_hubungan`) VALUES
(2, 'Suami'),
(3, 'Istri'),
(4, 'Anak Kandung'),
(5, 'Ayah'),
(6, 'Ibu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kec`
--

CREATE TABLE `tbl_kec` (
  `kec_no` int(11) NOT NULL,
  `kec_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kec`
--

INSERT INTO `tbl_kec` (`kec_no`, `kec_nama`) VALUES
(2, 'Langsa Timur'),
(3, 'Langsa Barat'),
(4, 'Langsa Baro'),
(5, 'Langsa Lama'),
(6, 'Langsa Kota');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komorbid`
--

CREATE TABLE `tbl_komorbid` (
  `komorbid_no` int(11) NOT NULL,
  `komorbid_nik_pasien` varchar(16) NOT NULL,
  `komorbid_hamil` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_diabetes` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_jantung` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_hipertensi` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_keganasan` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_imunologi` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_ginjal_kronis` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_hati_kronis` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_paru_kronis` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu',
  `komorbid_lainnya` varchar(100) NOT NULL COMMENT 'sebutkan'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kontak_pasien`
--

CREATE TABLE `tbl_kontak_pasien` (
  `tkp_no` int(11) NOT NULL,
  `tkp_nik_pasien` varchar(16) NOT NULL COMMENT 'dr tbl_pasien',
  `tkp_kontak_suspek_probable` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)',
  `tkp_kontakerat_konfirmasi_probable` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)',
  `tkp_ispa_berat` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)',
  `tkp_pelihara_hewan` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)',
  `tkp_tenaga_medis` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kuisioner_perjalanan`
--

CREATE TABLE `tbl_kuisioner_perjalanan` (
  `kp_no` int(11) NOT NULL,
  `kp_nik_pasien` varchar(16) NOT NULL,
  `kp_perjalanan_negara` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)',
  `kp_perjalanan_lokal` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)',
  `kp_tinggal_dilokal` varchar(10) NOT NULL COMMENT 'Ya/Tdk/Tdk Tahu(sebelum 14 Hari Sakit)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lab`
--

CREATE TABLE `tbl_lab` (
  `lab_no` int(11) NOT NULL,
  `lab_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_lab`
--

INSERT INTO `tbl_lab` (`lab_no`, `lab_nama`) VALUES
(2, 'lab');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_labkes`
--

CREATE TABLE `tbl_labkes` (
  `labkes_no` int(11) NOT NULL,
  `labkes_username` varchar(15) NOT NULL,
  `labkes_nama_user` varchar(30) NOT NULL,
  `labkes_nama_instansi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `login_no` int(11) NOT NULL,
  `login_username` varchar(16) NOT NULL,
  `login_password` varchar(100) NOT NULL,
  `login_stts` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`login_no`, `login_username`, `login_password`, `login_stts`) VALUES
(2, 'dinkes', 'd1c7e93048a30d60d970e18407699c25', 'dinkes'),
(1, 'ilhamr60', '7456f639ceb04d365c68ed0ef26104cd', 'rsud'),
(3, 'surveyor', '108b973b479d0fccbe63143f8904c180', 'surveyor');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_negara`
--

CREATE TABLE `tbl_negara` (
  `negara_no` int(11) NOT NULL,
  `negara_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_negara`
--

INSERT INTO `tbl_negara` (`negara_no`, `negara_nama`) VALUES
(1, 'Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pasien`
--

CREATE TABLE `tbl_pasien` (
  `pasien_no` int(11) NOT NULL,
  `pasien_nik` varchar(16) NOT NULL,
  `pasien_no_rm` varchar(10) NOT NULL COMMENT 'No Rekam Medis\r\n',
  `pasien_nama` varchar(30) NOT NULL,
  `pasien_nama_ortu` varchar(30) NOT NULL,
  `pasien_tgl_lhr` date NOT NULL,
  `pasien_kelamin` varchar(10) NOT NULL,
  `pasien_tlp` varchar(15) NOT NULL,
  `pasien_pekerjaan` varchar(30) NOT NULL,
  `pasien_usia` varchar(3) NOT NULL,
  `pasien_kewarganegaraan` varchar(20) NOT NULL,
  `pasien_alamat` varchar(100) NOT NULL,
  `pasien_provinsi` varchar(30) NOT NULL,
  `pasien_tgl_masuk` date NOT NULL,
  `pasien_tgl_keluar` date DEFAULT NULL,
  `pasien_kabkota` varchar(30) NOT NULL,
  `pasien_kec` varchar(30) NOT NULL,
  `pasien_desa` varchar(30) NOT NULL,
  `pasien_tgl_lapor` date NOT NULL,
  `pasien_prov_faskes_asal` varchar(30) NOT NULL,
  `pasien_kabkota_faskes_asal` varchar(30) NOT NULL,
  `pasien_faskes_asal` varchar(30) NOT NULL,
  `pasien_status` varchar(30) NOT NULL COMMENT 'ambil dr tbl_status_pasien',
  `pasien_penginput` varchar(30) NOT NULL COMMENT 'DInkes/RSUD\r\n',
  `pasien_verified_dinkes` varchar(1) NOT NULL COMMENT '1=ya, 0=tdk\r\n',
  `pasien_tgl_pengambilan_swab` date NOT NULL COMMENT 'Tgl SWAB',
  `pasien_tgl_hasil_lab` date NOT NULL,
  `pasien_ruang_rawat` varchar(30) NOT NULL COMMENT 'EX: PINERE DLL Relasi tbl_ruang_rawat\r\n',
  `pasien_ket` varchar(250) NOT NULL,
  `pasien_lampiran` varchar(100) NOT NULL COMMENT 'File Lampiran',
  `pasien_foto` varchar(100) NOT NULL,
  `pasien_stts_tindakan` varchar(30) NOT NULL COMMENT 'ISman, Meninggal, etc'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pasien`
--

INSERT INTO `tbl_pasien` (`pasien_no`, `pasien_nik`, `pasien_no_rm`, `pasien_nama`, `pasien_nama_ortu`, `pasien_tgl_lhr`, `pasien_kelamin`, `pasien_tlp`, `pasien_pekerjaan`, `pasien_usia`, `pasien_kewarganegaraan`, `pasien_alamat`, `pasien_provinsi`, `pasien_tgl_masuk`, `pasien_tgl_keluar`, `pasien_kabkota`, `pasien_kec`, `pasien_desa`, `pasien_tgl_lapor`, `pasien_prov_faskes_asal`, `pasien_kabkota_faskes_asal`, `pasien_faskes_asal`, `pasien_status`, `pasien_penginput`, `pasien_verified_dinkes`, `pasien_tgl_pengambilan_swab`, `pasien_tgl_hasil_lab`, `pasien_ruang_rawat`, `pasien_ket`, `pasien_lampiran`, `pasien_foto`, `pasien_stts_tindakan`) VALUES
(19, '1113304093039001', '983208', 'Jokowi', 'Ismail', '2021-05-26', 'Laki-laki', '02938230232', 'Karyawan Swasta', '0', 'Indonesia', 'Desa Karang Anyar', 'Aceh', '0000-00-00', NULL, 'Banda Aceh', 'Langsa Kota', 'Lengkong', '2021-05-26', '', '', '', 'Probable', 'Ilham Ramadhan (DINKES)', '0', '0000-00-00', '0000-00-00', '', '-', '1113304093039001', '', 'Isolasi Mandiri'),
(14, '11738683757357', '37868', 'Roby Maulana', '', '2021-05-25', 'Perempuan', '08896826892', 'Karyawan BUMD', '0', 'Indonesia', 'Dusun Sejahtera', 'Aceh', '0000-00-00', NULL, 'Medan', 'Langsa Timur', 'Karang Anyar', '2021-05-23', '', '', '', 'Probable', 'Ilham Ramadhan (RSUD)', '0', '0000-00-00', '0000-00-00', '', '', '11738683757357', '', 'Dirawat'),
(12, '117487868758578', '67674', 'Andika Sukma', '', '2021-05-23', 'Laki-laki', '085368816639', 'Software Engineer', '0', 'Indonesia', 'Desa Karang Anyar', 'Aceh', '0000-00-00', NULL, 'Banda Aceh', 'Langsa Barat', 'Karang Anyar', '2021-05-23', '', '', '', 'Probable', 'Ilham Ramadhan (RSUD)', '0', '0000-00-00', '0000-00-00', '', '', '117487868758578', '', 'Isolasi Mandiri'),
(13, '11768738358735', '232323', 'Ilham Ramadhan', '', '2021-05-18', 'Laki-laki', '086357257532', 'Software Engineer', '0', 'Indonesia', 'Dusun Damai', 'Aceh', '0000-00-00', NULL, 'Langsa', 'Langsa Timur', 'Karang Anyar', '2021-05-23', '', '', '', 'Suspect', 'Ilham Ramadhan (RSUD)', '0', '0000-00-00', '0000-00-00', '', '', '11768738358735', '', 'Isolasi Mandiri'),
(15, '1178378633832', '32323', 'Toni Setiawan', '', '2021-04-30', 'Laki-laki', '08927892323', 'Karyawan BUMN', '0', 'Indonesia', 'Dusun Damai', 'Sumatera Utara', '0000-00-00', NULL, 'Banda Aceh', 'Langsa Barat', 'Paya Bujok Tunong', '2021-05-15', '', '', '', 'Meninggal', 'Ilham Ramadhan (RSUD)', '0', '0000-00-00', '0000-00-00', '', '', '1178378633832', '', 'Isolasi Mandiri'),
(16, '1186286278587', '23232', 'Rahmat Furqan', '', '2021-05-25', 'Laki-laki', '0877537532', 'Karyawan BUMN', '0', 'Indonesia', 'Dusun Damai', 'Sumatera Utara', '0000-00-00', NULL, 'Banda Aceh', 'Langsa Baro', 'Karang Anyar', '2021-05-23', '', '', '', 'Positif', 'Ilham Ramadhan (RSUD)', '0', '0000-00-00', '0000-00-00', '', '', '1186286278587', '', 'Isolasi Mandiri');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pekerjaan`
--

CREATE TABLE `tbl_pekerjaan` (
  `pekerjaan_no` int(11) NOT NULL,
  `pekerjaan_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pekerjaan`
--

INSERT INTO `tbl_pekerjaan` (`pekerjaan_no`, `pekerjaan_nama`) VALUES
(2, 'Software Engineer'),
(3, 'Karyawan BUMD'),
(4, 'Karyawan Swasta'),
(5, 'Karyawan BUMN'),
(6, 'PNS'),
(7, 'Freelance');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pemeriksa`
--

CREATE TABLE `tbl_pemeriksa` (
  `pemeriksa_no` int(11) NOT NULL,
  `pemeriksa_lab` varchar(30) NOT NULL COMMENT 'NamaLab dari Tbl Lab',
  `pemeriksa_no_sample_lab` varchar(30) NOT NULL,
  `pemeriksa_pemeriksaan_ke` varchar(30) NOT NULL,
  `pemeriksa_jenis_spesimen` varchar(30) NOT NULL COMMENT 'NamaJenis Spesimen tbl Jenis SPesimen',
  `pemeriksa_tgl_periksa` date NOT NULL,
  `pemeriksa_hasil_spesimen` varchar(30) NOT NULL COMMENT 'Kategori hasil tbl_kateg_hasil_spesimen'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prov`
--

CREATE TABLE `tbl_prov` (
  `prov_no` int(11) NOT NULL,
  `prov_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_prov`
--

INSERT INTO `tbl_prov` (`prov_no`, `prov_nama`) VALUES
(1, 'Aceh'),
(2, 'Sumatera Utara');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_puskesmas`
--

CREATE TABLE `tbl_puskesmas` (
  `puskesmas_no` int(11) NOT NULL,
  `puskesmas_username` varchar(15) NOT NULL,
  `puskesmas_nama` varchar(30) NOT NULL,
  `puskesmas_kec` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_riwayat_kontakerat`
--

CREATE TABLE `tbl_riwayat_kontakerat` (
  `trk_no` int(11) NOT NULL,
  `trk_nik_pasien` varchar(16) NOT NULL COMMENT 'dr tbl_pasien',
  `trk_nama` varchar(30) NOT NULL,
  `trk_umur` varchar(3) NOT NULL,
  `trk_kelamin` varchar(10) NOT NULL,
  `trk_hubungan` varchar(30) NOT NULL COMMENT 'dr tbl_kateg_hubungan_kontak',
  `trk_alamat` varchar(100) NOT NULL,
  `trk_desa` varchar(30) NOT NULL COMMENT 'dr tbl_desa',
  `trk_kota` varchar(30) NOT NULL COMMENT 'dr tbl_kota',
  `trk_kecamatan` varchar(30) NOT NULL COMMENT 'dr tbl_kec',
  `trk_provinsi` varchar(30) NOT NULL COMMENT 'dr tbl_prov',
  `trk_tlp` varchar(15) NOT NULL,
  `trk_aktifitas_kontak` varchar(100) NOT NULL COMMENT 'sebutkan'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_riwayat_perjalanan_lokal`
--

CREATE TABLE `tbl_riwayat_perjalanan_lokal` (
  `rpl_no` int(11) NOT NULL,
  `rpl_nik_pasien` varchar(16) NOT NULL,
  `rpl_provinsi` varchar(30) NOT NULL COMMENT 'dr tbl_provinsi',
  `rpl_kota` varchar(30) NOT NULL COMMENT 'dr tbl_kota',
  `rpl_tgl_perjalanan` date NOT NULL,
  `rpl_tgl_tiba_indo` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_riwayat_perjalanan_negara`
--

CREATE TABLE `tbl_riwayat_perjalanan_negara` (
  `rpn_no` int(11) NOT NULL,
  `rpn_nik_pasien` varchar(16) NOT NULL COMMENT 'dr tbl_pasien',
  `rpn_negara` varchar(30) NOT NULL COMMENT 'dari tbl_negara',
  `rpn_kota` varchar(30) NOT NULL COMMENT 'Ketik',
  `rpn_tgl_perjalanan` date NOT NULL,
  `rpn_tgl_tiba_indo` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_riwayat_tinggal_dilokal`
--

CREATE TABLE `tbl_riwayat_tinggal_dilokal` (
  `rtd_no` int(11) NOT NULL,
  `rtd_nik_pasien` varchar(16) NOT NULL COMMENT 'dr tbl_pasien',
  `rtd_provinsi` varchar(30) NOT NULL COMMENT 'dr tbl_provinsi',
  `rtd_kota` varchar(30) NOT NULL COMMENT 'dr tbl_kota'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rs`
--

CREATE TABLE `tbl_rs` (
  `rs_no` int(11) NOT NULL,
  `rs_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_rs`
--

INSERT INTO `tbl_rs` (`rs_no`, `rs_nama`) VALUES
(1, 'Rumah Sakit Umum Daerah Langsa'),
(2, 'RSUD');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rsud`
--

CREATE TABLE `tbl_rsud` (
  `rsud_no` int(11) NOT NULL,
  `rsud_username` varchar(15) NOT NULL,
  `rsud_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_rsud`
--

INSERT INTO `tbl_rsud` (`rsud_no`, `rsud_username`, `rsud_nama`) VALUES
(1, 'ilhamr60', 'Ilham Ramadhan (RSUD)');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ruang_rawat`
--

CREATE TABLE `tbl_ruang_rawat` (
  `ruang_no` int(11) NOT NULL,
  `ruang_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_ruang_rawat`
--

INSERT INTO `tbl_ruang_rawat` (`ruang_no`, `ruang_nama`) VALUES
(1, 'PINERE'),
(2, 'RITN');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status_pasien`
--

CREATE TABLE `tbl_status_pasien` (
  `sp_no` int(11) NOT NULL,
  `sp_status` varchar(20) NOT NULL COMMENT 'Suspect/Positif/Probable'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_status_pasien`
--

INSERT INTO `tbl_status_pasien` (`sp_no`, `sp_status`) VALUES
(1, 'Suspect'),
(2, 'Positif'),
(4, 'Probable'),
(5, 'Meninggal');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surveyor`
--

CREATE TABLE `tbl_surveyor` (
  `surveyor_no` int(11) NOT NULL,
  `surveyor_username` varchar(15) NOT NULL,
  `surveyor_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_surveyor`
--

INSERT INTO `tbl_surveyor` (`surveyor_no`, `surveyor_username`, `surveyor_nama`) VALUES
(1, 'surveyor', 'Ilham Ramadhan (Surveyor)');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tindakan_pasien`
--

CREATE TABLE `tbl_tindakan_pasien` (
  `tindakan_no` int(11) NOT NULL,
  `tindakan_nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_tindakan_pasien`
--

INSERT INTO `tbl_tindakan_pasien` (`tindakan_no`, `tindakan_nama`) VALUES
(1, 'Dirawat'),
(2, 'Isolasi Mandiri');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_desa`
--
ALTER TABLE `tbl_desa`
  ADD PRIMARY KEY (`desa_no`);

--
-- Indexes for table `tbl_diagnos`
--
ALTER TABLE `tbl_diagnos`
  ADD PRIMARY KEY (`diagnos_no`);

--
-- Indexes for table `tbl_dinkes`
--
ALTER TABLE `tbl_dinkes`
  ADD PRIMARY KEY (`dinkes_username`),
  ADD UNIQUE KEY `dinkes_no` (`dinkes_no`);

--
-- Indexes for table `tbl_faskes`
--
ALTER TABLE `tbl_faskes`
  ADD PRIMARY KEY (`faskes_no`);

--
-- Indexes for table `tbl_fasyankes`
--
ALTER TABLE `tbl_fasyankes`
  ADD PRIMARY KEY (`fasyankes_username`),
  ADD UNIQUE KEY `fasyankes_no` (`fasyankes_no`);

--
-- Indexes for table `tbl_gejala`
--
ALTER TABLE `tbl_gejala`
  ADD UNIQUE KEY `klinis_no` (`gejala_no`);

--
-- Indexes for table `tbl_jenis_spesimen`
--
ALTER TABLE `tbl_jenis_spesimen`
  ADD PRIMARY KEY (`jenis_spesi_no`);

--
-- Indexes for table `tbl_kabkota`
--
ALTER TABLE `tbl_kabkota`
  ADD PRIMARY KEY (`kabkota_no`);

--
-- Indexes for table `tbl_kateg_hasil_spesimen`
--
ALTER TABLE `tbl_kateg_hasil_spesimen`
  ADD PRIMARY KEY (`spesimen_no`);

--
-- Indexes for table `tbl_kateg_hubungan_kontak`
--
ALTER TABLE `tbl_kateg_hubungan_kontak`
  ADD PRIMARY KEY (`tkh_no`);

--
-- Indexes for table `tbl_kec`
--
ALTER TABLE `tbl_kec`
  ADD PRIMARY KEY (`kec_no`);

--
-- Indexes for table `tbl_komorbid`
--
ALTER TABLE `tbl_komorbid`
  ADD PRIMARY KEY (`komorbid_no`);

--
-- Indexes for table `tbl_kontak_pasien`
--
ALTER TABLE `tbl_kontak_pasien`
  ADD PRIMARY KEY (`tkp_no`);

--
-- Indexes for table `tbl_kuisioner_perjalanan`
--
ALTER TABLE `tbl_kuisioner_perjalanan`
  ADD PRIMARY KEY (`kp_no`);

--
-- Indexes for table `tbl_lab`
--
ALTER TABLE `tbl_lab`
  ADD PRIMARY KEY (`lab_no`);

--
-- Indexes for table `tbl_labkes`
--
ALTER TABLE `tbl_labkes`
  ADD PRIMARY KEY (`labkes_username`),
  ADD UNIQUE KEY `labkes_no` (`labkes_no`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`login_username`),
  ADD UNIQUE KEY `login_no` (`login_no`);

--
-- Indexes for table `tbl_negara`
--
ALTER TABLE `tbl_negara`
  ADD PRIMARY KEY (`negara_no`);

--
-- Indexes for table `tbl_pasien`
--
ALTER TABLE `tbl_pasien`
  ADD PRIMARY KEY (`pasien_nik`),
  ADD UNIQUE KEY `pasien_no` (`pasien_no`);

--
-- Indexes for table `tbl_pekerjaan`
--
ALTER TABLE `tbl_pekerjaan`
  ADD PRIMARY KEY (`pekerjaan_no`);

--
-- Indexes for table `tbl_pemeriksa`
--
ALTER TABLE `tbl_pemeriksa`
  ADD PRIMARY KEY (`pemeriksa_no`);

--
-- Indexes for table `tbl_prov`
--
ALTER TABLE `tbl_prov`
  ADD PRIMARY KEY (`prov_no`);

--
-- Indexes for table `tbl_puskesmas`
--
ALTER TABLE `tbl_puskesmas`
  ADD PRIMARY KEY (`puskesmas_username`),
  ADD UNIQUE KEY `puskesmas_no` (`puskesmas_no`);

--
-- Indexes for table `tbl_riwayat_kontakerat`
--
ALTER TABLE `tbl_riwayat_kontakerat`
  ADD PRIMARY KEY (`trk_no`);

--
-- Indexes for table `tbl_riwayat_perjalanan_lokal`
--
ALTER TABLE `tbl_riwayat_perjalanan_lokal`
  ADD PRIMARY KEY (`rpl_no`);

--
-- Indexes for table `tbl_riwayat_perjalanan_negara`
--
ALTER TABLE `tbl_riwayat_perjalanan_negara`
  ADD PRIMARY KEY (`rpn_no`);

--
-- Indexes for table `tbl_riwayat_tinggal_dilokal`
--
ALTER TABLE `tbl_riwayat_tinggal_dilokal`
  ADD PRIMARY KEY (`rtd_no`);

--
-- Indexes for table `tbl_rs`
--
ALTER TABLE `tbl_rs`
  ADD PRIMARY KEY (`rs_no`);

--
-- Indexes for table `tbl_rsud`
--
ALTER TABLE `tbl_rsud`
  ADD PRIMARY KEY (`rsud_username`),
  ADD UNIQUE KEY `rsud_no` (`rsud_no`);

--
-- Indexes for table `tbl_ruang_rawat`
--
ALTER TABLE `tbl_ruang_rawat`
  ADD PRIMARY KEY (`ruang_no`);

--
-- Indexes for table `tbl_status_pasien`
--
ALTER TABLE `tbl_status_pasien`
  ADD PRIMARY KEY (`sp_no`);

--
-- Indexes for table `tbl_surveyor`
--
ALTER TABLE `tbl_surveyor`
  ADD PRIMARY KEY (`surveyor_username`),
  ADD UNIQUE KEY `surveyor_no` (`surveyor_no`);

--
-- Indexes for table `tbl_tindakan_pasien`
--
ALTER TABLE `tbl_tindakan_pasien`
  ADD PRIMARY KEY (`tindakan_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_desa`
--
ALTER TABLE `tbl_desa`
  MODIFY `desa_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_diagnos`
--
ALTER TABLE `tbl_diagnos`
  MODIFY `diagnos_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_dinkes`
--
ALTER TABLE `tbl_dinkes`
  MODIFY `dinkes_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_faskes`
--
ALTER TABLE `tbl_faskes`
  MODIFY `faskes_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_fasyankes`
--
ALTER TABLE `tbl_fasyankes`
  MODIFY `fasyankes_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gejala`
--
ALTER TABLE `tbl_gejala`
  MODIFY `gejala_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_jenis_spesimen`
--
ALTER TABLE `tbl_jenis_spesimen`
  MODIFY `jenis_spesi_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_kabkota`
--
ALTER TABLE `tbl_kabkota`
  MODIFY `kabkota_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_kateg_hasil_spesimen`
--
ALTER TABLE `tbl_kateg_hasil_spesimen`
  MODIFY `spesimen_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_kateg_hubungan_kontak`
--
ALTER TABLE `tbl_kateg_hubungan_kontak`
  MODIFY `tkh_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_kec`
--
ALTER TABLE `tbl_kec`
  MODIFY `kec_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_komorbid`
--
ALTER TABLE `tbl_komorbid`
  MODIFY `komorbid_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_kontak_pasien`
--
ALTER TABLE `tbl_kontak_pasien`
  MODIFY `tkp_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_kuisioner_perjalanan`
--
ALTER TABLE `tbl_kuisioner_perjalanan`
  MODIFY `kp_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_lab`
--
ALTER TABLE `tbl_lab`
  MODIFY `lab_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_labkes`
--
ALTER TABLE `tbl_labkes`
  MODIFY `labkes_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `login_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_negara`
--
ALTER TABLE `tbl_negara`
  MODIFY `negara_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_pasien`
--
ALTER TABLE `tbl_pasien`
  MODIFY `pasien_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_pekerjaan`
--
ALTER TABLE `tbl_pekerjaan`
  MODIFY `pekerjaan_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_pemeriksa`
--
ALTER TABLE `tbl_pemeriksa`
  MODIFY `pemeriksa_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_prov`
--
ALTER TABLE `tbl_prov`
  MODIFY `prov_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_puskesmas`
--
ALTER TABLE `tbl_puskesmas`
  MODIFY `puskesmas_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_riwayat_kontakerat`
--
ALTER TABLE `tbl_riwayat_kontakerat`
  MODIFY `trk_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_riwayat_perjalanan_lokal`
--
ALTER TABLE `tbl_riwayat_perjalanan_lokal`
  MODIFY `rpl_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_riwayat_perjalanan_negara`
--
ALTER TABLE `tbl_riwayat_perjalanan_negara`
  MODIFY `rpn_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_riwayat_tinggal_dilokal`
--
ALTER TABLE `tbl_riwayat_tinggal_dilokal`
  MODIFY `rtd_no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_rs`
--
ALTER TABLE `tbl_rs`
  MODIFY `rs_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_rsud`
--
ALTER TABLE `tbl_rsud`
  MODIFY `rsud_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_ruang_rawat`
--
ALTER TABLE `tbl_ruang_rawat`
  MODIFY `ruang_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_status_pasien`
--
ALTER TABLE `tbl_status_pasien`
  MODIFY `sp_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_surveyor`
--
ALTER TABLE `tbl_surveyor`
  MODIFY `surveyor_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_tindakan_pasien`
--
ALTER TABLE `tbl_tindakan_pasien`
  MODIFY `tindakan_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
